using System;
using System.Collections;

namespace UIR.Domain.ClickLog
{
    public class Place: IEnumerable
    {
        public Place(string url, long placeId, long userId = 0, long categoryId = 0, long tempId = 0)
        {
            Url = url;
            UserId = userId;
            PlaceId = placeId;
            DateTime = DateTime.Now;
            CategoryId = categoryId;
            TempId = tempId;
        }

        public string Url { get; }
        public long UserId { get; }
        public long PlaceId { get; }
        public DateTime DateTime { get; }
        public long CategoryId { get; }
        public long TempId { get; }
        
        public IEnumerator GetEnumerator()
        {
            yield return Url;
            yield return UserId;
            yield return PlaceId;
            yield return DateTime;
            yield return CategoryId;
            yield return TempId;
        }
    }
}