using System;

namespace UIR.Domain.Interfaces
{
    public interface IHasCreateDateTime
    {
        DateTimeOffset CreateDateTime { get; set; }
    }
}