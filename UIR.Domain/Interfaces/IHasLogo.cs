namespace UIR.Domain.Interfaces
{
    public interface IHasLogo
    {
        string Logo { get; set; }
    }
}