﻿using System;
using Force.Ddd;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Domain.PaySystem.Entities
{
    public class Payment : HasIdBase<long>
    {
        public static Spec<Payment> LastCreated => new Spec<Payment>(x => x.DateTime.AddMonths(1) > DateTime.Now);

        public static Spec<Payment> LastPaid => new Spec<Payment>(x =>
            x.PaidDateTime.AddMonths(1) > DateTime.Now && x.PaymentStatus == PaymentStatus.Paid);

        public DateTime DateTime { get; set; }
        public User User { get; set; }
        public long UserId { get; set; }
        public long PaymentPlanId { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public Payout Payout { get; set; }
        public string ExternalPaymentId { get; set; }
        public DateTime PaidDateTime { get; set; }

        public void ToInProgress()
        {
            PaymentStatus = PaymentStatus.InProgress;
        }

        public void ToPaid(string externalId, DateTime paidDateTime)
        {
            ExternalPaymentId = externalId;
            PaidDateTime = paidDateTime;
            PaymentStatus = PaymentStatus.Paid;
        }

        public static string LastSuccesPaymentNotFound = "Не найден платеж за текущий месяц";
    }
}