﻿using System;
using Force.Ddd;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Domain.PaySystem.Entities
{
    public class Payout: HasIdBase<long>
    {
        public decimal Value { get; set; }
        public DateTime DateTime { get; set; }
        public User User { get; set; }
        public long UserId { get; set; }
    }
}