﻿using Force.Ddd;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Domain.PaySystem.Entities
{
    public class PaymentPlan: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public PaymentPlanValue Value { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }
    }
}