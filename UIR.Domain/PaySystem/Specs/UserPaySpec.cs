using System;
using System.Linq;
using Force.Ddd;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Domain.PaySystem.Specs
{
    public static class UserPaySpec
    {
        public static ForceSpec<User> IsPay(string userName) => 
            new ForceSpec<User>(x => x.UserName == userName) & HasLastPaid;
        
        private static ForceSpec<User> HasLastPaid => new ForceSpec<User>(x =>
            x.Payments.Any(p => p.PaidDateTime.AddMonths(1) > DateTime.Now && p.PaymentStatus == PaymentStatus.Paid));
    }
}