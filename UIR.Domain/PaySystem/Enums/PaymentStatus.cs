﻿namespace UIR.Domain.PaySystem.Enums
{
    public enum PaymentStatus
    {
        New = 10,
        InProgress = 20,
        Paid = 30,
        Cancelled = 40
    }
}