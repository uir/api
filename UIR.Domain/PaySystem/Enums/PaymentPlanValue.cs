﻿namespace UIR.Domain.PaySystem.Enums
{
    public enum PaymentPlanValue
    {
        Free = 0,

        LowValue = 500,
        MiddleValue = 2000, 
        HeigthValue = 3000,
        
        
        MiddlePartnerValue = 4000,
        HigthPartnerValue = 7000,
    }
}