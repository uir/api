﻿namespace UIR.Domain.PaySystem.Enums
{
    public enum PaymentPlanType
    {
        Public = 10,
        Partner = 20
    }
}