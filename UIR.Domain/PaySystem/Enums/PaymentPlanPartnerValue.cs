﻿namespace UIR.Domain.PaySystem.Enums
{
    public enum PaymentPlanPartnerValue
    {
        LowValue = 4000,
        MiddleValue = 7000
    }
}