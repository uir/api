﻿namespace UIR.Domain.Entities.UserSetUp
{
    public class PublicUser : User
    {
        public string NumberPassport { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string SeriesPassport { get; set; }
        public string Birthday { get; set; }
        public string ResidenceAddress { get; set; }
        public string DatePassport { get; set; }
        public string PassportDepartment { get; set; }
        public string MassPayAccount { get; set; }


        public bool InfosFill()
        {
            return !string.IsNullOrEmpty(Name) &&
                   !string.IsNullOrEmpty(NumberPassport) &&
                   !string.IsNullOrEmpty(Surname) &&
                   !string.IsNullOrEmpty(MiddleName) &&
                   !string.IsNullOrEmpty(SeriesPassport) &&
                   !string.IsNullOrEmpty(Birthday) &&
                   !string.IsNullOrEmpty(ResidenceAddress) &&
                   !string.IsNullOrEmpty(DatePassport) &&
                   !string.IsNullOrEmpty(PassportDepartment) &&
                   !string.IsNullOrEmpty(MassPayAccount);
        }
    }
}