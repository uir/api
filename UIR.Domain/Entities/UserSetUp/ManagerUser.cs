using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities.Base.Filters;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Extensions;

namespace UIR.Domain.Entities.UserSetUp
{
    public class ManagerUser: User
    {
        public ManagerUser()
        {
        }

        public ManagerUser(string userName) : base(userName)
        {
        }
        public ManagerType ManagerType { get; set; }

        [InverseProperty("Manager")]
        public List<Organization> Organizations { get; set; }
    }

    public class ManagerPermissionFilter : AuthOnlyPermissionFilter<ManagerUser>
    {
        public ManagerPermissionFilter(DbContext dbContext, IHttpContextAccessor httpContextAccessor) : base(dbContext, httpContextAccessor)
        {
        }

        protected override IQueryable<ManagerUser> GetPermittedForAuthOnly(IQueryable<ManagerUser> queryable)
        {
            var manager = DbContext.FirstOrDefault<ManagerUser>(x => x.Id == UserId);
            if (manager == null)
                return Empty;
            switch (manager.ManagerType)
            {
                case ManagerType.Admin:
                    return queryable;
                default:
                    return Empty;
            }
        }
    }
}