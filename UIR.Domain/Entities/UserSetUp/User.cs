﻿using System.Collections.Generic;
using Force.Ddd;
using Microsoft.AspNetCore.Identity;
using UIR.Domain.PaySystem.Entities;

namespace UIR.Domain.Entities.UserSetUp
{
    public class User : IdentityUser<long>, IHasId<long>
    {
        public User()
        {
        }

        public User(string userName) : base(userName)
        {
        }

        public long? ParentId { get; set; }
        public string Name { get; set; }        

        public User Parent { get; set; }
        
        public virtual List<Bookmarks> Bookmarks { get; set; }
        public virtual List<UserSale> Sales { get; set; }
        public virtual List<Payment> Payments { get; set; }
        public virtual  List<Overview> Overview { get; set; }
        public override long Id { get; set; }
        object IHasId.Id => Id;
    }
}