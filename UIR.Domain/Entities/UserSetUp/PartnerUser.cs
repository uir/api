﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Force.Ddd;
using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities.UserSetUp
{
    public class PartnerUser : User
    {
        public PartnerUser()
        {
        }

        public PartnerUser(string userName) : base(userName)
        {
        }

        [InverseProperty("Owner")]
        public virtual List<Organization> Organizations { get; set; }
    }

    public static class PartnerUserSpec
    {
        public static ForceSpec<PartnerUser> Fake =>
            Spec.Of<PartnerUser>(x => x.Organizations.Count == 0) &
            IsDraft;

        public static ForceSpec<PartnerUser> IsDraft =>
            Spec.Of<PartnerUser>(x => !x.EmailConfirmed && x.Name == null && x.Email == null);
    }
}