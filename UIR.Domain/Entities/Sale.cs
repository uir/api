﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Force.Ddd;
using UIR.Domain.Entities.Base;
using UIR.Domain.Entities.Base.Specs;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Enums;
using UIR.Domain.Interfaces;

namespace UIR.Domain.Entities
{
    public class Sale : HasIdBase<long>, IHasCreateDateTime, IHasName
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset CreateDateTime { get; set; }
        public DiscountType DiscountType { get; set; }
        public DateTimeOffset? DiscountEndDate { get; set; }
        public bool IsExclusive { get; set; }

        public long? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public IEnumerable<SalePlace> Places { get; set; }
        public virtual List<UserSale> Users { get; set; }
    }

    public static class SaleFormatter
    {
        public static Expression<Func<Sale, string>> OwnerName =>
            x => x.OrganizationId == null
                ? x.Places.Any()
                    ? x.Places.FirstOrDefault().Place.Name
                    : null
                : x.Organization.Name;

        public static Expression<Func<Sale, string>> OrganizationName =>
            x => x.OrganizationId == null
                ? x.Places.Any()
                    ? x.Places.Select(sp => sp.Place.Organization.Name).FirstOrDefault()
                    : null
                : x.Organization.Name;

        public static Expression<Func<Sale, long>> PlaceId =>
            x => x.Places.Any()
                ? x.Places.Select(sp => sp.Place.Id).FirstOrDefault()
                : x.Organization.Places.Select(sp => sp.Id).FirstOrDefault();
    }

    public static class SaleSpec
    {
        /// <summary>
        /// Условия поиска скидки:
        /// - совпадения текста в названии скидки
        /// - совпадения текста в названии филиала, которому принаджлежит скика
        /// - совпадения текста в названии организации, которой принаджлежит скика
        /// - если скидка активна
        /// </summary>
        public static ForceSpec<Sale> Search(string text) =>
            (
                DomainSpec.SearchByName<Sale>(text) |
                DomainSpec.SearchByName<Place>(text).FromManySale() |
                DomainSpec.SearchByName<Organization>(text).From<Sale>(s => s.Organization)
            )
            & Active;

        public static ForceSpec<Sale> All(long placeId, long companyId) =>
            ByOrganizationId(companyId) | ByPlaceId(placeId);

        public static ForceSpec<Sale> ByPlaceId(long placeId) => PlaceSpec.ById(placeId).FromManySale();

        public static ForceSpec<Sale> ByOrganizationId(long orgId) =>
            new ForceSpec<Sale>(x => x.Organization.Id == orgId);

        /// <summary>
        /// Скидка активна:
        /// - если у нее проставлен флаг IsActive
        /// - если активен филиал в котором она нхиодится
        /// - если активна организация в которой она находится
        /// - если проставлена дата окончания и она не истекла
        /// - если тип "Скидка" и значение больше 0
        /// </summary>
        public static ForceSpec<Sale> Active =>
            (
                OrganizationActive |
                PlaceSpec.Active.FromManySale()
            )
            & Spec.Of<Sale>(x => x.IsActive && (x.DiscountEndDate == null || x.DiscountEndDate > DateTime.Now))
            & Spec.Of<Sale>(x =>
                x.DiscountType == DiscountType.Common && x.Amount > 0 || x.DiscountType == DiscountType.Stock);

        private static ForceSpec<Sale> OrganizationActive =>
            Spec.Of<Sale>(x => x.Organization != null) & OrganizationSpec.Active.From<Sale>(s => s.Organization);

        private static ForceSpec<Sale> FromManySale(this ForceSpec<Place> placeSpec) =>
            placeSpec.FromMany<Sale, SalePlace>(s => s.Places, s => s.Place);
    }
}