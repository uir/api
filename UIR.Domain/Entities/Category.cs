﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Domain.Entities.Base;

namespace UIR.Domain.Entities
{
    public class Category : HasIdBase<long>, IHasName
    {
        public string Name { get; set; }
        public string Icon { get; set; }

        public virtual List<CategoryOrganization> Companies { get; set; }
        public virtual List<CategoryTemplate> Templates { get; set; }
    }
}