namespace UIR.Domain.Entities
{
    public class SalePlace
    {
        public long SaleId { get; set; }
        public Sale Sale { get; set; }

        public Place Place { get; set; }
        public long PlaceId { get; set; }
    }
}