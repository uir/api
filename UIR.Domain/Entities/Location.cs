﻿using Force.Ddd;

namespace UIR.Domain.Entities
{
    public class Location : HasIdBase<long>
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}