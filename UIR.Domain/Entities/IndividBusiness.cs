﻿using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities
{
    public class IndividBusiness : Organization
    {
        public string Inn { get; set; }
        public string Ogrn { get; set; }
    }
}