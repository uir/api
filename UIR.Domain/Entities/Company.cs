﻿using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities
{
    public class Company : Organization
    {
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Ogrn { get; set; }
    }
}