﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Domain.Entities.Base;
using UIR.Domain.Enums;

namespace UIR.Domain.Entities
{
    public class Period : HasIdBase<long>
    {
        public PeriodType PeriodType { get; set; }
        public int Day { get; set; }
        public string Time { get; set; }

        public virtual List<PlacePeriod> Places { get; set; }
    }
}