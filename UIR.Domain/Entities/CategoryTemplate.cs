﻿namespace UIR.Domain.Entities
{
    public class CategoryTemplate
    {
        public long CategoryId { get; set; }
        public Category Category { get; set; }

        public long TemplateId { get; set; }
        public Template Template { get; set; }
    }
}