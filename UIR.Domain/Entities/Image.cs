﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities
{
    public class Image : HasIdBase<long>
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string File { get; set; }
        public bool IsLogo { get; set; }

        public long? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public IEnumerable<ImagePlace> Places { get; set; }
    }
}