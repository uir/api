﻿namespace UIR.Domain.Entities
{
    public class PlacePeriod
    {
        public long PlaceId { get; set; }
        public Place Place { get; set; }

        public long PeriodId { get; set; }
        public Period Period { get; set; }
    }
}