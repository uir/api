namespace UIR.Domain.Entities
{
    public class VideoPlace
    {
        public long VideoId { get; set; }
        public Video Video { get; set; }

        public Place Place { get; set; }
        public long PlaceId { get; set; }
    }
}