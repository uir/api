﻿using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities
{
    public class OrganizationTag
    {
        public long TagId { get; set; }
        public Tag Tag { get; set; }

        public Organization Organization { get; set; }
        public long OrganizationId { get; set; }
    }
}