﻿using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities
{
    public class CategoryOrganization
    {
        public long CategoryId { get; set; }
        public Category Category { get; set; }

        public long OrganizationId { get; set; }
        public Organization Organization { get; set; }
    }
}