﻿using System.Collections.Generic;
using Force.Ddd;

namespace UIR.Domain.Entities
{
    public class Video : HasIdBase<long>
    {
        public string Name { get; set; }
        public string Discription { get; set; }
        public string File { get; set; }

        public IEnumerable<VideoPlace> Places { get; set; }
    }
}