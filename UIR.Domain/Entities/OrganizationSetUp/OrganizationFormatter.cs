﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace UIR.Domain.Entities.OrganizationSetUp
{
    public static class OrganizationFormatter
    {
        public static Expression<Func<Organization, string>> Logo =>
            o => Enumerable.Any<Image>(o.Images, i => i.IsLogo) ? Enumerable.First<Image>(o.Images, i => i.IsLogo).File : null;

        public static Expression<Func<Organization, bool>> IsDraft =>
            o => OrganizationSpec.IsDraft.IsSatisfiedBy(o);
    }
}