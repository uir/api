﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities.Base.Filters;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Extensions;

namespace UIR.Domain.Entities.OrganizationSetUp
{
    public class OrganizationPermissionFilter : AuthOnlyPermissionFilter<Organization>
    {
        public OrganizationPermissionFilter(DbContext dbContext, IHttpContextAccessor httpContextAccessor) : base(
            dbContext, httpContextAccessor)
        {
        }

        protected override IQueryable<Organization> GetPermittedForAuthOnly(IQueryable<Organization> queryable)
        {
            var manager = DbContext.FirstOrDefault<ManagerUser>(x => x.Id == UserId);
            if (manager == null)
                return Empty;
            switch (manager.ManagerType)
            {
                case ManagerType.Admin:
                    return queryable;
                case ManagerType.Manager:
                    return queryable.Where(o => o.Manager.Id == UserId);
                default:
                    return Empty;
            }
        }
    }
}