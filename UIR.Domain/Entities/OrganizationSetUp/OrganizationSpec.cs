﻿using Force.Ddd;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Domain.Entities.OrganizationSetUp
{
    public static class OrganizationSpec
    {
        /// <summary>
        /// Организация активна:
        /// - если все филиалы в ней активны
        /// </summary>
        public static ForceSpec<Organization> Active => PlaceSpec.Active.FromMany<Organization>(o => o.Places);

        public static ForceSpec<Organization> IsDraft => PartnerUserSpec.IsDraft.From<Organization>(o => o.Owner);
    }
}