﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Force.Ddd;
using UIR.Domain.Entities.Base;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;

namespace UIR.Domain.Entities.OrganizationSetUp
{
    public class Organization : HasIdBase<long>, IHasName
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Fio { get; set; }

        public ActiveType ActiveType { get; set; }

        public long OwnerId { get; set; }
        [ForeignKey("OwnerId")]
        public virtual PartnerUser Owner { get; set; }

        public virtual long? ManagerId { get; set; }
        [ForeignKey("ManagerId")]
        public virtual ManagerUser Manager { get; set; }

        public virtual List<Overview> Overviews { get; set; }
        public virtual List<Image> Images { get; set; }
        public virtual List<CategoryOrganization> Categories { get; set; }
        public virtual List<Place> Places { get; set; }
        public virtual List<Sale> Sales { get; set; }
        public virtual List<OrganizationTag> Tags { get; set; }
    }
}