﻿using System;
using Force.Ddd;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;

namespace UIR.Domain.Entities
{
    public class Referal : HasIdBase<string>
    {
        public string Name { get; set; }
        public ReferalType Type { get; set; }
        public DateTime CreateDateTime { get; set; }

        public long UserId { get; set; }
        public virtual User User { get; set; }
    }
}