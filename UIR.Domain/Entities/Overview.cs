﻿using System;
using Force.Ddd;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;

namespace UIR.Domain.Entities
{
    public class Overview : HasIdBase<long>
    {
        public DateTime CreateDate { get; set; }
        public Rating Rating { get; set; }
        public bool IsConfirmed { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Plus { get; set; }
        public string Minus { get; set; }

        public long UserId { get; set; }
        public virtual User User { get; set; }

        public long PlaceId { get; set; }
        public virtual Place Place { get; set; }
    }
}