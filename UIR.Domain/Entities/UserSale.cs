﻿using System;
using Force.Ddd;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Interfaces;

namespace UIR.Domain.Entities
{
    public class UserSale : HasIdBase<long>, IHasCreateDateTime
    {
        public bool IsActive { get; set; }
        public DateTimeOffset CreateDateTime { get; set; }

        public long SaleId { get; set; }
        public virtual Sale Sale { get; set; }

        public long UserId { get; set; }
        public virtual User User { get; set; }
    }
}