﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Force.Ddd;
using UIR.Domain.Entities.Base;
using UIR.Domain.Entities.Base.Specs;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Enums;

namespace UIR.Domain.Entities
{
    public class Place : HasIdBase<long>, IHasName
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Phone { get; set; }
        public string Vicinity { get; set; }
        public string Website { get; set; }
        public double Rating { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public PriceLevel PriceLevel { get; set; }
        public RoomType? RoomType { get; set; }
        public string Room { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public bool IsActive { get; set; }

        public long LocationId { get; set; }
        public virtual Location Location { get; set; }

        public long OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public virtual List<Bookmarks> Bookmarks { get; set; }
        public virtual List<PlacePeriod> Periods { get; set; }
        public virtual List<ImagePlace> Images { get; set; }
        public virtual List<VideoPlace> Videos { get; set; }
        public virtual List<SalePlace> Sales { get; set; }
        public virtual List<Overview> Overviews { get; set; }
    }

    public static class PlaceSpec
    {
        public static ForceSpec<Place> ById(long id) => new ForceSpec<Place>(x => x.Id == id);
        
        public static ForceSpec<Place> Search(string text) =>
            DomainSpec.SearchByName<Place>(text) | 
            DomainSpec.SearchByName<Organization>(text).From<Place>(p => p.Organization);
        
        public static ForceSpec<Place> Active => new ForceSpec<Place>(p => p.IsActive);
    }

    public static class PlaceFormatter
    {
        public static Expression<Func<Place, string>> Logo => x =>
            (x.Organization.Images.OrderByDescending(i => i.Id).FirstOrDefault(i => i.IsLogo) != null
                ? x.Organization.Images.OrderByDescending(i => i.Id).FirstOrDefault(i => i.IsLogo).File
                : null);

        public static Expression<Func<Place, string>> Name => x => x.Organization.Name;

        public static Expression<Func<Place, int>> SalesCount =>
            x => x.Sales.Count(s => s.PlaceId == x.Id) + x.Organization.Sales.Count;
        
        public static Expression<Func<Place, int>> ImagesCount =>
            x => x.Images.Count(i => !i.Image.IsLogo);
    }
}