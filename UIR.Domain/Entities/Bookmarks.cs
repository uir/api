﻿using UIR.Domain.Entities.UserSetUp;

namespace UIR.Domain.Entities
{
    public class Bookmarks
    {
        public long UserId { get; set; }
        public User User { get; set; }

        public long PlaceId { get; set; }
        public Place Place { get; set; }
    }
}