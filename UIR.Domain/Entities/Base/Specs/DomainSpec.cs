using System;
using System.Linq.Expressions;
using Force.Ddd;

namespace UIR.Domain.Entities.Base.Specs
{
    public static class DomainSpec
    {
        public static ForceSpec<T> SearchByName<T>(string text) where T : class, IHasName, IHasId =>
            new ForceSpec<T>(x => x.Name.ToLower().Contains(text.ToLower()));
        
        public static ForceSpec<T> SearchById<T>(string text) where T : class, IHasName, IHasId =>
            new ForceSpec<T>(x => x.Id.ToString().ToLower() == text.ToLower());

        public static ForceSpec<T> FullSearch<T>(string text) where T : class, IHasName, IHasId =>
            SearchByName<T>(text) |
            SearchById<T>(text); 

        public static Expression<Func<T, string>> OrderByName<T>() where T : class, IHasName => x => x.Name;
    }
}