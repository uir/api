﻿namespace UIR.Domain.Entities.Base
{
    public interface IHasName
    {
        string Name { get; set; }
    }
}