using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Controller;
using UIR.Extensions.Core.Extensions;

namespace UIR.Domain.Entities.Base.Filters
{
    public abstract class AuthOnlyPermissionFilter<T> : PermissionFilter<T>
    {
        protected readonly IHttpContextAccessor _httpContextAccessor;

        public override long? UserId => _httpContextAccessor.HttpContext.User.GetId();
        public string[] Roles => _httpContextAccessor.HttpContext.User.GetRoles();

        public AuthOnlyPermissionFilter(DbContext dbContext, IHttpContextAccessor httpContextAccessor) : base(dbContext)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public override IQueryable<T> GetPermitted(IQueryable<T> queryable)
        {
            if (!UserId.HasValue)
            {
                return Empty;
            }

            return GetPermittedForAuthOnly(queryable);
        }

        protected abstract IQueryable<T> GetPermittedForAuthOnly(IQueryable<T> queryable);
    }
}