namespace UIR.Domain.Entities
{
    public class ImagePlace
    {
        public long ImageId { get; set; }
        public Image Image { get; set; }

        public Place Place { get; set; }
        public long PlaceId { get; set; }
    }
}