﻿using System.Collections.Generic;
using Force.Ddd;

namespace UIR.Domain.Entities
{
    public class Template : HasIdBase<long>
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Color { get; set; }

        public virtual List<CategoryTemplate> Categories { get; set; }
    }
}