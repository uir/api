using System.Collections.Generic;
using Force.Ddd;
using UIR.Domain.Entities.Base;

namespace UIR.Domain.Entities
{
    public class Tag : HasIdBase<long>, IHasName
    {
        public string Name { get; set; }

        public virtual List<OrganizationTag> Organizations { get; set; }
    }
}