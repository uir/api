﻿using Force.Ddd;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;

namespace UIR.Domain.Entities
{
    public class UserTwoFactorToken: HasIdBase<long>
    {
        public User Owner { get; set; }
        public long OwnerId { get; set; }
        public string Value { get; set; }
        public TwoFactorTokenStatus Status { get; set; }
    }
}