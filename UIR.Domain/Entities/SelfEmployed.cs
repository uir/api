﻿using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Domain.Entities
{
    public class SelfEmployed : Organization
    {
        public string NumberPassport { get; set; }
        public string SeriesPassport { get; set; }
        public string Birthday { get; set; }
        public string ResidenceAddress { get; set; }
        public string DatePassport { get; set; }
        public string PassportDepartment { get; set; }
    }
}