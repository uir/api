﻿namespace UIR.Domain.Enums
{
    public enum PriceLevel
    {
        Free = 0x1,
        Inexpensive = 0x2,
        Moderate = 0x3,
        Expensive = 0x4,
        VeryExpensive = 0x5,
    }
}