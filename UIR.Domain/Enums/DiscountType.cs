namespace UIR.Domain.Enums
{
    public enum DiscountType
    {
        Common = 0,
        Stock = 1
    }
}