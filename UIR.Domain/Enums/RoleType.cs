﻿namespace UIR.Domain.Enums
{
    public enum RoleType
    {
        User = 0, 
        Admin = 1, 
        Partner = 2,
        PaidLowUser = 3,
        PaidMiddleUser = 4,
        PaidHeigthUser = 5,
        Manager = 6
    }
}