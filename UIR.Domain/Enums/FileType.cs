﻿namespace UIR.Domain.Enums
{
    public enum FileType
    {
        Image, Video
    }
}