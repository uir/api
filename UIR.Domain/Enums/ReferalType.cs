﻿namespace UIR.Domain.Enums
{
    public enum ReferalType
    {
        Personal, General
    }
}