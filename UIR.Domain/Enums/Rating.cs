﻿namespace UIR.Domain.Enums
{
    public enum Rating
    {
        One, Two, Three, Four, Five
    }
}