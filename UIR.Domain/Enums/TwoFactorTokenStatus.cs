﻿namespace UIR.Domain.Enums
{
    public enum TwoFactorTokenStatus
    {
        Active = 0,
        NotUsed = 1,
        Old = 2
    }
}