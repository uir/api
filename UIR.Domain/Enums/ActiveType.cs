﻿namespace UIR.Domain.Enums
{
    public enum ActiveType
    {
        Company = 1,
        IndividBusiness = 2,
        SelfEmployed = 3
    }
}