﻿using System;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using NLog;

namespace UIR.Sms.Core.Base
{
    public abstract class SmsSenderBase<TSendSmsResponse>: ISmsProvider
    {
        private static ILogger _logger;
        protected Regex CheckPhoneRegex;

        protected SmsSenderBase()
        {
            _logger = LogManager.GetLogger("SMS");
            CheckPhoneRegex = new Regex(@"\+?[0-9]{11}");
        }

        public async Task Send(string number, string text)
        {
            try
            {
                if (!CheckPhoneNumber(number, CheckPhoneRegex))
                    throw new ArgumentException("Incorrect Phone Number");

                text = NormalizeText(text);
                var url = GenerateSendSmsUrl(number, text);
                var response = await GetUrlResponse<TSendSmsResponse>(url);
                CheckSmsSuccesfullySended(response);
                _logger.Debug("Sms succesfully sended by {0}", this.GetType());
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Sms sending faile by {0}", this.GetType());
                throw new Exception("SmsSender: something wrong", ex);
            }
        }

        protected async Task<T> GetUrlResponse<T>(string url)
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                response = await client.GetAsync(url);
            }
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content);
        }

        protected virtual bool CheckPhoneNumber(string phone, Regex regex) => regex.IsMatch(phone);
        protected virtual string NormalizePhoneNumber(string phoneNumber) => Regex.Replace(phoneNumber, @"^\+", "");
        protected virtual string NormalizeText(string text) => WebUtility.UrlEncode(text);
        protected abstract string GenerateSendSmsUrl(string number, string text);
        protected abstract void CheckSmsSuccesfullySended(TSendSmsResponse response);
    }
}