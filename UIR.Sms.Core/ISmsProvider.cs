﻿using System.Threading.Tasks;

namespace UIR.Sms.Core
{
    public interface ISmsProvider
    {
        Task Send(string number, string text);
    }
}