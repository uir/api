﻿using Newtonsoft.Json;

namespace UIR.Sms.Core.Smsc
{
    public class SmscResponce
    {
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("error_code")]
        public int Code { get; set; }
        [JsonProperty("id")]
        public string MessageId { get; set; }
    }
}