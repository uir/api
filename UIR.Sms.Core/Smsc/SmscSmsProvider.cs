﻿using UIR.Sms.Core.Base;

namespace UIR.Sms.Core.Smsc
{
    public class SmscSmsProvider: SmsSenderBase<SmscResponce>
    {
        private string _login;
        private string _password;
        
        public SmscSmsProvider()
        {
            _login = "doctorc";
            _password = "306418fan";
        }

        protected override string GenerateSendSmsUrl(string number, string text) =>
            $"https://smsc.ru/sys/send.php?login={_login}&psw={_password}&phones={number}&mes={text}&fmt=3";

        protected override void CheckSmsSuccesfullySended(SmscResponce response){}
    }
}