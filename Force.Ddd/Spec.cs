using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Force.Ddd.Extensions;

namespace Force.Ddd
{
    public static class Spec
    {
        public static ForceSpec<T> Of<T>(Expression<Func<T, bool>> expression) => new ForceSpec<T>(expression);

        public static ForceSpec<T> Of<T, TFrom>(Expression<Func<T, TFrom>> expression, ForceSpec<TFrom> spec) =>
            spec.From(expression);

        public static ForceSpec<T> Of<T, TAny, TFrom>(Expression<Func<T, IEnumerable<TAny>>> expressionAny,
            Expression<Func<TAny, TFrom>> expression, ForceSpec<TFrom> spec) =>
            spec.FromMany(expressionAny, expression);
    }

    public class ForceSpec<T>
    {
        public static bool operator false(ForceSpec<T> forceSpec) => false;

        public static bool operator true(ForceSpec<T> forceSpec) => false;

        public static ForceSpec<T> operator &(ForceSpec<T> spec1, ForceSpec<T> spec2)
            => new ForceSpec<T>(spec1._expression.And(spec2._expression));

        public static ForceSpec<T> operator |(ForceSpec<T> spec1, ForceSpec<T> spec2)
            => new ForceSpec<T>(spec1._expression.Or(spec2._expression));

        public static ForceSpec<T> operator !(ForceSpec<T> forceSpec)
            => new ForceSpec<T>(forceSpec._expression.Not());

        public static implicit operator Expression<Func<T, bool>>(ForceSpec<T> forceSpec)
            => forceSpec._expression;

        public static implicit operator ForceSpec<T>(Expression<Func<T, bool>> expression)
            => new ForceSpec<T>(expression);

        private readonly Expression<Func<T, bool>> _expression;

        public ForceSpec(Expression<Func<T, bool>> expression)
        {
            _expression = expression ?? throw new ArgumentNullException(nameof(expression));
        }

        public bool IsSatisfiedBy(T obj) => _expression.AsFunc()(obj);

        public ForceSpec<TParent> From<TParent>(Expression<Func<TParent, T>> mapFrom)
            => _expression.From(mapFrom);

        public ForceSpec<TParent2> FromMany<TParent2, TParent1>(
            Expression<Func<TParent2, IEnumerable<TParent1>>> mapFrom2, Expression<Func<TParent1, T>> mapFrom1)
            => _expression.From(mapFrom1).Any(mapFrom2);
        
        public ForceSpec<TParent> FromMany<TParent>(Expression<Func<TParent, IEnumerable<T>>> mapFrom)
            => _expression.Any(mapFrom);
    }

    public static class SpecExtenions
    {
        public static ForceSpec<T> AsSpec<T>(this Expression<Func<T, bool>> expr)
            where T : class, IHasId
            => new ForceSpec<T>(expr);

        public static bool Satisfy<T>(this T obj, Func<T, bool> spec)
        {
            return spec(obj);
        }

        public static bool SatisfyExpresion<T>(this T obj, Expression<Func<T, bool>> spec)
        {
            return spec.AsFunc()(obj);
        }

        public static bool IsSatisfiedBy<T>(this Func<T, bool> spec, T obj)
        {
            return spec(obj);
        }

        public static bool IsSatisfiedBy<T>(this Expression<Func<T, bool>> spec, T obj)
        {
            return spec.AsFunc()(obj);
        }
    }
}