﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MimeTypes.Core;
using Shresth.AspNet.Core;

namespace UIR.Cloud.Controllers
{
    public class FileController : Controller
    {
        private readonly IConfiguration _configuration;

        public FileController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        [HttpGet]
        [Route("~/api/files/{folder}/{fileName}")]
        public async Task<FileStreamResult> Get(string fileName, string folder)
        {
            var path = Path.Combine(_configuration["root_folder"], folder, fileName);
            var extension = Path.GetExtension(fileName);
            var mediaType = MimeTypeMap.GetMimeType(extension);

            var fileMemrStream = new MemoryStream();
            using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                await fileStream.CopyToAsync(fileMemrStream);
                fileMemrStream.Position = 0;
            }

            return new FileStreamResult(fileMemrStream, mediaType);
        }

        [HttpPost]
        [Route("~/api/files")]
        [RequestSizeLimit(52428800)]
        public async Task<IActionResult> Post(string fileName, string folder)
        {
            var directorypath = GetDirectory(folder);
            var stream = Request.Body;
            using (var ms = new MemoryStream())
            {
                await stream.CopyToAsync(ms);
                ms.Seek(0, SeekOrigin.Begin);
                System.IO.File.WriteAllBytes(Path.Combine(directorypath, fileName), ms.ToArray());
            }
            return Ok();
        }
        
        [HttpDelete]
        [Route("~/api/files")]
        public IActionResult Delete(string fileName, string folder)
        {
            var path = Path.Combine(_configuration["root_folder"], folder, fileName);
            var fileInfo = new FileInfo(path);
            
            if (!fileInfo.Exists)
                return NotFound();
            
            fileInfo.Delete();            
            return Ok();
        }

        private string GetDirectory(string folder)
        {
            var directorypath = Path.Combine(Directory.GetCurrentDirectory(), _configuration["root_folder"], folder);
            if (!Directory.Exists(directorypath))
            {
                Directory.CreateDirectory(directorypath);
            }

            return directorypath;
        }
    }
}