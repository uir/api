﻿using AutoMapper;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.AppSettings.Extensions;
using UIR.ClickLog.Modules.ClickLog;
using UIR.ClickLog.Services.Jobs;
using UIR.DAL.ClickHouse;
using UIR.Domain.ClickLog;

namespace UIR.ClickLog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddUirClickHouse();
            services.AddAutoMapper(typeof(Startup).Assembly);
            services.AddUirBaseServices(Configuration);
            
            services.AddHangfire(c => c.UseMemoryStorage());

            services.AddClickLogModule();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
            app.UseUirSwagger();
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate<LogJob<Place>>("uir_places_log", x => x.Start(), Cron.Minutely);
        }
    }
}