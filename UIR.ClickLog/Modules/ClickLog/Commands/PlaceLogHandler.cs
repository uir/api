using Newtonsoft.Json;
using UIR.ClickLog.Modules.ClickLog.Dto.Request;
using UIR.ClickLog.Services;
using UIR.Domain.ClickLog;

namespace UIR.ClickLog.Modules.ClickLog.Commands
{
    public class PlaceLogData
    {
        public long PlaceId { get; set; }
        public long TempId { get; set; }
        [JsonProperty("id")] 
        public long CategoryId { get; set; }
    }

    public class PlaceLogHandler : LogHandler<Place, PlaceLogData>
    {
        public PlaceLogHandler(IMemoryStorage<Place> memoryStorage) : base(memoryStorage)
        {
        }

        protected override Place GetEntry(LogDto input, PlaceLogData data)
        {
            if (data == null || data.PlaceId == 0)
                return null;

            return new Place(input.Url, data.PlaceId, 0, data.CategoryId, data.TempId);
        }
    }
}