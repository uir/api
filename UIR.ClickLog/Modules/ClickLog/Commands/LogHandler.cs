using Force.Cqrs;
using Newtonsoft.Json.Linq;
using UIR.ClickLog.Modules.ClickLog.Dto.Request;
using UIR.ClickLog.Services;

namespace UIR.ClickLog.Modules.ClickLog.Commands
{
    public abstract class LogHandler<TEntry, TData>: IHandler<LogDto> where TData: class
    {
        private readonly IMemoryStorage<TEntry> _memoryStorage;

        public LogHandler(IMemoryStorage<TEntry> memoryStorage)
        {
            _memoryStorage = memoryStorage;
        }
        
        public void Handle(LogDto input)
        {
            var data = Deserialize(input.Data);
            var entry = GetEntry(input, data);
            if (entry == null)
                return;
            
            _memoryStorage.Add(entry);
        }

        protected abstract TEntry GetEntry(LogDto input, TData data);

        private TData Deserialize(dynamic data) => (data as JObject)?.ToObject<TData>();
    }
}