using Microsoft.Extensions.DependencyInjection;
using UIR.ClickLog.Modules.ClickLog.Commands;
using UIR.ClickLog.Services;
using UIR.ClickLog.Services.Jobs;
using UIR.Domain.ClickLog;

namespace UIR.ClickLog.Modules.ClickLog
{
    public static class ClickLogModule
    {
        public static void AddClickLogModule(this IServiceCollection services)
        {
            services.AddScoped<PlaceLogHandler>();
            services.AddSingleton<IMemoryStorage<Place>, MemoryStorage<Place>>();
            services.AddScoped<LogJob<Place>>();
        }
    }
}