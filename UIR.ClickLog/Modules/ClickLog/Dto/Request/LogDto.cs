namespace UIR.ClickLog.Modules.ClickLog.Dto.Request
{
    public class LogDto
    {
        public string Url { get; set; }
        public string Handler { get; set; }
        public dynamic Data { get; set; }
    }
}