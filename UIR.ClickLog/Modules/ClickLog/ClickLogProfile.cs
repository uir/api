using AutoMapper;
using UIR.ClickLog.Modules.ClickLog.Commands;

namespace UIR.ClickLog.Modules.ClickLog
{
    public class ClickLogProfile : Profile
    {
        public ClickLogProfile()
        {
            CreateMap<dynamic, PlaceLogData>()
                .ForMember(d => d.PlaceId, o => o.ResolveUsing(s => s.placeId ?? 0))
                .ForMember(d => d.TempId, o => o.ResolveUsing(s => s.tempId ?? 0))
                .ForMember(d => d.CategoryId, o => o.ResolveUsing(s => s.id ?? 0));
        }
    }
}