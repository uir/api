using System.Collections.Generic;
using Force.Cqrs;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using UIR.ClickLog.Modules.ClickLog.Commands;
using UIR.ClickLog.Modules.ClickLog.Dto.Request;

namespace UIR.ClickLog.Modules.ClickLog.Controllers
{
    [EnableCors("CorsPolicy")]
    public class ClickLogController : Controller
    {
        private IDictionary<string, IHandler<LogDto>> _handlers;

        public ClickLogController(PlaceLogHandler placeLogHandler)
        {
            _handlers = new Dictionary<string, IHandler<LogDto>>
            {
                {"PartnerInfoComponent", placeLogHandler}
            };
        }

        [HttpPost]
        [Route("~/api/log")]
        public IActionResult Get([FromBody] LogDto dto)
        {
            if (_handlers.ContainsKey(dto.Handler))
                _handlers[dto.Handler].Handle(dto);
            return Ok();
        }
    }
}