using System.Collections;
using System.Linq;
using UIR.DAL.ClickHouse.Repositories;

namespace UIR.ClickLog.Services.Jobs
{
    public class LogJob<T> where T : IEnumerable
    {
        private readonly IMemoryStorage<T> _memoryStorage;
        private readonly IClickHouseRepository<T> _repository;

        public LogJob(IMemoryStorage<T> memoryStorage, IClickHouseRepository<T> repository)
        {
            _memoryStorage = memoryStorage;
            _repository = repository;
        }

        public void Start()
        {
            var data = _memoryStorage.GetAll().ToArray();
            if (!data.Any())
                return;
            _repository.AddRange(data);
            _memoryStorage.Clear();
        }
    }
}
