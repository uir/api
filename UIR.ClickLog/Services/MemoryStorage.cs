using System.Collections.Concurrent;
using System.Collections.Generic;

namespace UIR.ClickLog.Services
{
    public interface IMemoryStorage<TData>
    {
        void Add(TData val);
        void Clear();

        IEnumerable<TData> GetAll();
    }

    public class MemoryStorage<TData> : IMemoryStorage<TData>
    {
        private static ConcurrentQueue<TData> _queue;

        public MemoryStorage()
        {
            _queue = new ConcurrentQueue<TData>();
        }

        public void Add(TData val) => _queue.Enqueue(val);
        public void Clear() => _queue.Clear();
        public IEnumerable<TData> GetAll() => _queue;
    }
}