using Microsoft.Extensions.DependencyInjection;
using UIR.DAL.ClickHouse.Repositories;
using UIR.Domain.ClickLog;

namespace UIR.DAL.ClickHouse
{
    public static class ClickHouseModule
    {
        public static void AddUirClickHouse(this IServiceCollection services)
        {
            services.AddScoped<ClickHouseConnectionProvider>();
            services.AddScoped<ClickHouseDb>();
            services.AddScoped<IClickHouseRepository<Place>, PlacesRepository>();
        }
    }
}