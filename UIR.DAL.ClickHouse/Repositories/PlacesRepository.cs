using UIR.Domain.ClickLog;

namespace UIR.DAL.ClickHouse.Repositories
{
    public class PlacesRepository : ClickHouseRepository<Place>
    {
        public override string InsertCommandText =>
            @"insert into uir.Places(Url, UserId, PlaceId, DateTime, CategoryId, TempId) values @bulk";

        public PlacesRepository(ClickHouseDb db) : base(db)
        {
        }
    }
}