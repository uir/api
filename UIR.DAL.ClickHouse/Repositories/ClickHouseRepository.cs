using System.Collections;
using System.Collections.Generic;
using ClickHouse.Ado;

namespace UIR.DAL.ClickHouse.Repositories
{
    public abstract class ClickHouseRepository<T> : IClickHouseRepository<T> where T : IEnumerable
    {
        private readonly ClickHouseDb _db;
        public abstract string InsertCommandText { get; }

        public ClickHouseRepository(ClickHouseDb db)
        {
            _db = db;
        }

        public void AddRange(IEnumerable<T> enumerable) => _db.Insert(InsertCommandText, enumerable);
    }
}