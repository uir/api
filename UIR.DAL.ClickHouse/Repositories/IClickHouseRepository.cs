using System.Collections;
using System.Collections.Generic;

namespace UIR.DAL.ClickHouse.Repositories
{
    public interface IClickHouseRepository<T> where T : IEnumerable
    {
        void AddRange(IEnumerable<T> enumerable);
    }
}