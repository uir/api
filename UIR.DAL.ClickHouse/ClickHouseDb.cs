using System;
using System.Collections.Generic;
using ClickHouse.Ado;

namespace UIR.DAL.ClickHouse
{
    public class ClickHouseDb
    {
        private readonly ClickHouseConnectionProvider _connectionProvider;

        public ClickHouseDb(ClickHouseConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<T> Query<T>(string sql, ClickHouseParameterCollection param = null) where T : new()
        {
            var res = new List<T>();
            _connectionProvider.Connection.Open();
            using (var cmd = _connectionProvider.Connection.CreateCommand(sql))
            {
                if (param != null)
                {
                    foreach (var p in param) 
                        cmd.Parameters.Add(p); 
                }
                using (var reader = cmd.ExecuteReader())
                {
                    reader.ReadAll(dr =>
                    {
                        var obj = new T();
                        foreach (var prop in obj.GetType().GetProperties()) {
                            if (!Equals(dr[prop.Name], DBNull.Value)) {
                                prop.SetValue(obj, dr[prop.Name], null);
                            }
                        }
                        res.Add(obj);
                    });
                }
            }
            _connectionProvider.Connection.Close();
            return res;
        }

        public void Insert<T>(string sql, IEnumerable<T> enumerable)
        {
            try
            {
                _connectionProvider.Connection.Open();
                var commandText = sql;
                var command = _connectionProvider.Connection.CreateCommand();
                command.CommandText = commandText;
                command.Parameters.Add(new ClickHouseParameter
                {
                    Value = enumerable,
                    ParameterName = "bulk"
                });
                command.ExecuteNonQuery();
            }
            finally
            {
                _connectionProvider.Connection.Close();
            }
        }
    }
}