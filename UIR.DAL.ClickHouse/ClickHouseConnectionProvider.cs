using System;
using ClickHouse.Ado;
using Microsoft.Extensions.Options;
using UIR.AppSettings;

namespace UIR.DAL.ClickHouse
{
    public class ClickHouseConnectionProvider : IDisposable
    {
        public ClickHouseConnection Connection { get; }

        public ClickHouseConnectionProvider(IOptions<ClicklogSettings> options)
        {
            var settings = options.Value;
            var connectionString =
                $@"
Compress=true;
CheckCompressedHash=False;
Compressor=lz4;
Host={settings.Host};
Port={settings.Port};
Database={settings.Database};
User={settings.User};
Password={settings.Password};";
            Connection = new ClickHouseConnection(connectionString);
        }

        public void Dispose()
        {
            Connection.Dispose();
        }
    }
}