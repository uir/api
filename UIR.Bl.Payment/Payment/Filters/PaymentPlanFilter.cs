﻿using System.Linq;
using Force.Ddd;
using UIR.Domain.PaySystem.Entities;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Bl.Payment.Payment.Filters
{
    public class PaymentPlanFilter: IQueryableFilter<PaymentPlan>
    {
        private readonly PaymentPlanType _type;

        public PaymentPlanFilter(PaymentPlanType type)
        {
            _type = type;
        }

        public IQueryable<PaymentPlan> Apply(IQueryable<PaymentPlan> query) => 
            query.Where(x => x.PaymentPlanType == _type);
    }
}