﻿using UIR.Domain.PaySystem.Enums;

namespace UIR.Bl.Payment.Payment.Dto.Response
{
    public class PaymentPlanDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public PaymentPlanValue Value { get; set; }
    }
}