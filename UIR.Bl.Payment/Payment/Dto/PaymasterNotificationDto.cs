﻿using Microsoft.AspNetCore.Http;

namespace UIR.Bl.Payment.Payment.Dto
{
    public class PaymasterNotificationDto
    {
        public IFormCollection Values { get; set; }
    }
}