﻿using Microsoft.AspNetCore.Http;

namespace UIR.Bl.Payment.Payment.Dto
{
    public class PaymasterInvoiceDto
    {
        public IFormCollection Values { get; set; }
    }
}