﻿namespace UIR.Bl.Payment.Payment.Dto
{
    public class CreatePaymentCommandDto
    {
        public long PaymentPlanId { get; set; }
        public long UserId { get; set; }
    }
}