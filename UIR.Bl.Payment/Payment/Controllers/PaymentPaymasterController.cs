﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UIR.Bl.Payment.Payment.Dto;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Payment.Payment.Controllers
{
    public class PaymentPaymasterController : Controller
    {
        private readonly IHandler<PaymasterInvoiceDto, bool> _invoicePayment;
        private readonly IAsyncHandler<PaymasterNotificationDto, bool> _notificationPayment;
        private readonly ILogger<PaymentPaymasterController> _logger;

        public PaymentPaymasterController(IHandler<PaymasterInvoiceDto, bool> invoicePayment,
            IAsyncHandler<PaymasterNotificationDto, bool> notificationPayment,
            ILogger<PaymentPaymasterController> logger)
        {
            _invoicePayment = invoicePayment;
            _notificationPayment = notificationPayment;
            _logger = logger;
        }

        /// <summary>
        /// Проверка платежа
        /// </summary>
        /// <returns></returns>
        [Route("~/api/payment/paymaster/invoice"), HttpPost]
        public IActionResult PaymasterInvoice(object obj)
        {
            Request.Form.Each(x => _logger.LogWarning($"Invoice. Params: {x.Key}={x.Value}"));
            try
            {
                _invoicePayment.Handle(new PaymasterInvoiceDto
                {
                    Values = Request.Form
                });
                return Ok();
            }
            catch (OperationException e)
            {
                _logger.LogError($"Invoice. {e.Message}");
                return BadRequest();
            }
        }

        /// <summary>
        /// Обработка платежа
        /// </summary>
        /// <returns></returns>
        [Route("~/api/payment/paymaster/notification"), HttpPost]
        public async Task<IActionResult> PaymasterNotification(object obj)
        {
            Request.Form.Each(x => _logger.LogWarning($"Notification. Params: {x.Key}={x.Value}"));
            try
            {
                await _notificationPayment.Handle(new PaymasterNotificationDto
                {
                    Values = Request.Form
                });
                return Ok();
            }
            catch (OperationException e)
            {
                _logger.LogError($"Notification. {e.Message}");
                return BadRequest();
            }
        }
    }
}