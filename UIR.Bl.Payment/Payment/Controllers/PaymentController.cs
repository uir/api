﻿using Force.Cqrs;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UIR.Bl.Payment.Payment.Dto;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Payment.Payment.Controllers
{
    [Authorize(Roles = "User")]
    public class PaymentController : Controller
    {
        private readonly IHandler<CreatePaymentCommandDto, long> _createPaymentHandler;

        public PaymentController(IHandler<CreatePaymentCommandDto, long> createPaymentHandler)
        {
            _createPaymentHandler = createPaymentHandler;
        }

        /// <summary>
        /// Создание платежа
        /// </summary>
        /// <returns></returns>
        [Route("~/api/payment"), HttpPost]
        public IActionResult Create([FromBody] CreatePaymentCommandDto createPaymentCommandDto)
        {
            createPaymentCommandDto.UserId = User.GetId();
            return _createPaymentHandler
                .Handle(createPaymentCommandDto)
                .Either(
                    id => id != 0,
                    x => Ok(x),
                    id => (IActionResult) BadRequest());
        }
    }
}