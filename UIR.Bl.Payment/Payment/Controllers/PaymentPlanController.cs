﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Payment.Payment.Dto.Response;
using UIR.Bl.Payment.Payment.Filters;
using UIR.Domain.PaySystem.Entities;
using UIR.Domain.PaySystem.Enums;
using UIR.Extensions.Core.Controller;

namespace UIR.Bl.Payment.Payment.Controllers
{
    [AllowAnonymous]
    public class PaymentPlanController: CrudController
    {
        public PaymentPlanController(DbContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Возвращает список тарифных планов
        /// </summary>
        /// <returns></returns>
        [Route("~/api/plans"), HttpGet]
        public IActionResult GetPlans(PaymentPlanType type) => 
            GetList<PaymentPlan, PaymentPlanDto, PaymentPlanFilter>(new PaymentPlanFilter(type));
    }
}