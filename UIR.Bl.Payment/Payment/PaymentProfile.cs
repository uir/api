﻿using AutoMapper;
using UIR.Bl.Payment.Payment.Dto.Response;
using UIR.Domain.PaySystem.Entities;

namespace UIR.Bl.Payment.Payment
{
    public class PaymentProfile : Profile
    {
        public PaymentProfile()
        {
            CreateMap<PaymentPlan, PaymentPlanDto>();
        }
    }
}