﻿using System.Globalization;
using System.Linq;
using System.Net;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Bl.Payment.Payment.Dto;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Payment.Payment.Commands
{
    public class PaymasterInvoiceCheckPayment : IHandler<PaymasterInvoiceDto, bool>
    {
        private readonly DbContext _dbContext;
        private string _systemMerchantId;

        public PaymasterInvoiceCheckPayment(
            DbContext dbContext, 
            IOptions<PaymentSettings> options)
        {
            _dbContext = dbContext;
            _systemMerchantId = options.Value.MerchantId;
        }

        public bool Handle(PaymasterInvoiceDto dto)
        {
            var keysRes = dto.Values.Keys.Any(x =>
                x == PaymasterParam.PaymentId ||
                x == PaymasterParam.PaymentAmount ||
                x == PaymasterParam.PaymentCurrency ||
                x == PaymasterParam.MerchantId);
            if (!keysRes)
                throw new OperationException("Переданны не все обязательные поля", HttpStatusCode.InternalServerError);

            if (_systemMerchantId != dto.Values[PaymasterParam.MerchantId] && dto.Values[PaymasterParam.PaymentCurrency] != "RUB")
                throw new OperationException("Ошибка поставщика или валюты", HttpStatusCode.InternalServerError);
            
            var paymentId = long.Parse(dto.Values[PaymasterParam.PaymentId]);
            var payment = _dbContext
                .Where<UIR.Domain.PaySystem.Entities.Payment>(x => x.Id == paymentId)
                .Include(x => x.PaymentPlan)
                .FirstOrDefault();
            if (payment == null)
                throw new OperationException("Платеж не найден", HttpStatusCode.InternalServerError);

            var pamentAmount = decimal.Parse(dto.Values[PaymasterParam.PaymentAmount], CultureInfo.InvariantCulture);
            if ((int) payment.PaymentPlan.Value != pamentAmount)
                throw new OperationException("Несоответствие цены", HttpStatusCode.InternalServerError);

            payment.ToInProgress();
            _dbContext.SaveChanges();

            return true;
        }
    }
}