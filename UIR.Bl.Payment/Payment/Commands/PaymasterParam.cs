﻿namespace UIR.Bl.Payment.Payment.Commands
{
    public static class PaymasterParam
    {
        public const string PaymentId = "LMI_PAYMENT_NO";
        public const string PaymentAmount = "LMI_PAID_AMOUNT";
        public const string PaymentCurrency = "LMI_PAID_CURRENCY";
        public const string MerchantId = "LMI_MERCHANT_ID";
        public const string ExternalId = "LMI_SYS_PAYMENT_ID";
        public const string PaymentDate = "LMI_SYS_PAYMENT_DATE";
        public const string PayerIp = "LMI_PAYER_IP_ADDRESS";
    }
}