﻿using System;
using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Payment.Payment.Dto;
using UIR.Bl.Shared.Core.PaySystem.Filters;
using UIR.Domain.PaySystem.Enums;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Payment.Payment.Commands
{
    public class CreatePaymentCommand : IHandler<CreatePaymentCommandDto, long>
    {
        private readonly DbContext _dbContext;

        public CreatePaymentCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public long Handle(CreatePaymentCommandDto input)
        {
            var paymentId = _dbContext
                .Where(new LastPaymentFilter(input.UserId, PaymentStatus.New))
                .Where(x => x.PaymentPlanId == input.PaymentPlanId)
                .Select(x => x.Id)
                .FirstOrDefault();

            if (paymentId != 0)
                return paymentId;

            var payment = new UIR.Domain.PaySystem.Entities.Payment
            {
                DateTime = DateTime.Now,
                PaymentPlanId = input.PaymentPlanId,
                PaymentStatus = PaymentStatus.New,
                UserId = input.UserId
            };
            
            _dbContext.Add(payment);
            _dbContext.SaveChanges();

            return payment.Id;
        }
    }
}