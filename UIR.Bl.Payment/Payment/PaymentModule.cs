﻿using Force.Cqrs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.Bl.Payment.Payment.Commands;
using UIR.Bl.Payment.Payment.Dto;

namespace UIR.Bl.Payment.Payment
{
    public class PaymentModule
    {
        public void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IHandler<CreatePaymentCommandDto, long>, CreatePaymentCommand>();
            services.AddScoped<IHandler<PaymasterInvoiceDto, bool>, PaymasterInvoiceCheckPayment>();
            services.AddScoped<IAsyncHandler<PaymasterNotificationDto, bool>, PaymasterNotificationPayment>();

            services.Configure<PaymentSettings>(configuration.GetSection("Payment"));
        }
    }
}