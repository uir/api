﻿using System;
using Force.Ddd;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.PaySystem.Entities;

namespace UIR.DAL.Core
{
    public class UirDbContext : IdentityDbContext<User, Role, long>, IUnitOfWork
    {
        public DbSet<Place> Places { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Location> Locations{ get; set; }
        public DbSet<Period> Periods{ get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<Referal> Referals { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<UserTwoFactorToken> TwoFactorTokens { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentPlan> PaymentPlans { get; set; }
        public DbSet<Overview> Overviews { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Video> Videos { get; set; }

        public UirDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>().ToTable("Users");
            builder.Entity<Role>().ToTable("Roles");
            builder.Entity<IdentityUserClaim<long>>().ToTable("UserClaim");
            builder.Entity<IdentityUserRole<long>>().ToTable("UserRole");
            builder.Entity<IdentityUserLogin<long>>().ToTable("UserLogin");
            builder.Entity<IdentityRoleClaim<long>>().ToTable("RoleClaim");
            builder.Entity<IdentityUserToken<long>>().ToTable("UserToken");
            
            builder.Entity<CategoryOrganization>()
                .HasKey(x => new {x.CategoryId, x.OrganizationId});
            builder.Entity<CategoryOrganization>()
                .HasOne(x => x.Category)
                .WithMany(x => x.Companies)
                .HasForeignKey(x => x.CategoryId);
            builder.Entity<CategoryOrganization>()
                .HasOne(x => x.Organization)
                .WithMany(x => x.Categories)
                .HasForeignKey(x => x.OrganizationId);
            
            builder.Entity<CategoryTemplate>()
                .HasKey(x => new {x.CategoryId, x.TemplateId});
            builder.Entity<CategoryTemplate>()
                .HasOne(x => x.Category)
                .WithMany(x => x.Templates)
                .HasForeignKey(x => x.CategoryId);
            builder.Entity<CategoryTemplate>()
                .HasOne(x => x.Template)
                .WithMany(x => x.Categories)
                .HasForeignKey(x => x.TemplateId);
            
            builder.Entity<PlacePeriod>()
                .HasKey(x => new {x.PeriodId, x.PlaceId});
            builder.Entity<PlacePeriod>()
                .HasOne(x => x.Period)
                .WithMany(x => x.Places)
                .HasForeignKey(x => x.PeriodId);
            builder.Entity<PlacePeriod>()
                .HasOne(x => x.Place)
                .WithMany(x => x.Periods)
                .HasForeignKey(x => x.PlaceId);
            
            builder.Entity<Bookmarks>()
                .HasKey(x => new {x.UserId, x.PlaceId});
            builder.Entity<Bookmarks>()
                .HasOne(x => x.User)
                .WithMany(x => x.Bookmarks)
                .HasForeignKey(x => x.UserId);
            builder.Entity<Bookmarks>()
                .HasOne(x => x.Place)
                .WithMany(x => x.Bookmarks)
                .HasForeignKey(x => x.PlaceId);
            
            builder.Entity<OrganizationTag>()
                .HasKey(x => new {x.OrganizationId, x.TagId});
            builder.Entity<OrganizationTag>()
                .HasOne(x => x.Organization)
                .WithMany(x => x.Tags)
                .HasForeignKey(x => x.OrganizationId);
            builder.Entity<OrganizationTag>()
                .HasOne(x => x.Tag)
                .WithMany(x => x.Organizations)
                .HasForeignKey(x => x.TagId);
            
            builder.Entity<SalePlace>()
                .HasKey(x => new {x.PlaceId, x.SaleId});
            builder.Entity<SalePlace>()
                .HasOne(x => x.Place)
                .WithMany(x => x.Sales)
                .HasForeignKey(x => x.PlaceId);
            builder.Entity<SalePlace>()
                .HasOne(x => x.Sale)
                .WithMany(x => x.Places)
                .HasForeignKey(x => x.SaleId);
            
            builder.Entity<ImagePlace>()
                .HasKey(x => new {x.PlaceId, x.ImageId});
            builder.Entity<ImagePlace>()
                .HasOne(x => x.Place)
                .WithMany(x => x.Images)
                .HasForeignKey(x => x.PlaceId);
            builder.Entity<ImagePlace>()
                .HasOne(x => x.Image)
                .WithMany(x => x.Places)
                .HasForeignKey(x => x.ImageId);
            
            builder.Entity<VideoPlace>()
                .HasKey(x => new {x.PlaceId, x.VideoId});
            builder.Entity<VideoPlace>()
                .HasOne(x => x.Place)
                .WithMany(x => x.Videos)
                .HasForeignKey(x => x.PlaceId);
            builder.Entity<VideoPlace>()
                .HasOne(x => x.Video)
                .WithMany(x => x.Places)
                .HasForeignKey(x => x.VideoId);
            
            builder.Entity<Organization>(o =>
            {
                o.HasDiscriminator<string>("OrganizationType")
                    .HasValue<Company>("Company")
                    .HasValue<IndividBusiness>("IndividBusiness")
                    .HasValue<SelfEmployed>("SelfEmployed");
            });
            
            builder.Entity<User>(o =>
            {
                o.HasDiscriminator<string>("Discriminator")
                    .HasValue<PublicUser>("PublicUser")
                    .HasValue<PartnerUser>("PartnerUser")
                    .HasValue<ManagerUser>("ManagerUser");
            });
        }

        public new void Add<TEntity>(TEntity entity) where TEntity : class, IHasId => Set<TEntity>().Add(entity);
        
        public new void Remove<TEntity>(TEntity entity) where TEntity : class, IHasId => Set<TEntity>().Remove(entity);

        public new TEntity Find<TEntity>(params object[] id) where TEntity : class, IHasId => Set<TEntity>().Find(id);

        public new IHasId Find(Type entityType, params object[] id) => null;

        public void Commit() => SaveChanges();
    }
}