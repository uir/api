﻿using Force.Ddd;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.AppSettings;

namespace UIR.DAL.Core
{
    public static class UirEfModule
    {
        public static void AddUirEf(this IServiceCollection services, IConfiguration configuration)
        {
            var dbSettings = new MaidDbSettings();
            configuration.GetSection("MaidDbSettings").Bind(dbSettings);
            var host = dbSettings.Host + (dbSettings.Port.HasValue ? "," + dbSettings.Port : "");
            var connection = !string.IsNullOrEmpty(dbSettings.User)
                ? $"Server={host};Initial Catalog={dbSettings.Database};Integrated Security=False;User ID={dbSettings.User};Password={dbSettings.Password};Connect Timeout=60"
                : $"Server={host};Initial Catalog={dbSettings.Database};Integrated Security=True;Connect Timeout=60";
            services.AddDbContext<UirDbContext>(x =>
            {
                x.UseSqlServer(connection);
                x.UseOpenIddict();
            });
            services.AddScoped<DbContext, UirDbContext>();
            services.AddScoped<UirDbContext>();
            services.AddScoped<IUnitOfWork, UirDbContext>();
        }
    }
}