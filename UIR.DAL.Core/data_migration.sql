--user
SET IDENTITY_INSERT [uir-bd-core].dbo.Users ON
insert into [uir-bd-core].dbo.Users (
  Id, UserName, Email, PhoneNumber, EmailConfirmed, PhoneNumberConfirmed, PasswordHash, ParentId, Name, Discriminator, MassPayAccount, AccessFailedCount, LockoutEnabled, TwoFactorEnabled
)
  select
    Id,
    UserName,
    Email,
    PhoneNumber,
    EmailConfirmed,
    PhoneNumberConfirmed,
    PasswordHash,
    ParentId,
    Name,
    Discriminator,
    MassPayAccount,
    0,
    0,
    0
  from [uir-bd].dbo.Users
  order by ParentId;
SET IDENTITY_INSERT [uir-bd-core].dbo.Users OFF

insert into [uir-bd-core].dbo.Roles (ConcurrencyStamp, Name, NormalizedName)
  select
    NEWID(),
    Name,
    Name
  from [uir-bd].dbo.Roles;

insert into [uir-bd-core].dbo.UserRole (UserId, RoleId) select
                                                          User_Id,
                                                          Role_Id
                                                        from [uir-bd].dbo.RoleUsers;

--category
SET IDENTITY_INSERT [uir-bd-core].dbo.Templates ON
insert into [uir-bd-core].dbo.Templates (id, Icon, Name) select
                                                           id,
                                                           Icon,
                                                           Name
                                                         from [uir-bd].dbo.Templates;
SET IDENTITY_INSERT [uir-bd-core].dbo.Templates OFF

SET IDENTITY_INSERT [uir-bd-core].dbo.Categories ON
insert into [uir-bd-core].dbo.Categories (id, Icon, Name) select
                                                            id,
                                                            Icon,
                                                            Name
                                                          from [uir-bd].dbo.Categories;
SET IDENTITY_INSERT [uir-bd-core].dbo.Categories OFF

insert into [uir-bd-core].dbo.CategoryTemplate (CategoryId, TemplateId) select
                                                                          Category_Id,
                                                                          Template_Id
                                                                        from [uir-bd].dbo.TemplateCategories;

SET IDENTITY_INSERT [uir-bd-core].dbo.Locations ON
insert into [uir-bd-core].dbo.Locations (id, Latitude, Longitude) select
                                                                    id,
                                                                    Latitude,
                                                                    Longitude
                                                                  from [uir-bd].dbo.Locations;
SET IDENTITY_INSERT [uir-bd-core].dbo.Locations OFF

SET IDENTITY_INSERT [uir-bd-core].dbo.Tags ON
insert into [uir-bd-core].dbo.Tags (id, Name) select
                                                id,
                                                Name
                                              from [uir-bd].dbo.Tags;
SET IDENTITY_INSERT [uir-bd-core].dbo.Tags OFF

insert into [uir-bd-core].dbo.Periods (Day, PeriodType, Time) select
                                                                Day,
                                                                PeriodType,
                                                                Time
                                                              from [uir-bd].dbo.Periods;

--organizations
SET IDENTITY_INSERT [uir-bd-core].dbo.Organizations ON
insert into [uir-bd-core].dbo.Organizations (
  id,
  Inn,
  Kpp,
  Ogrn,
  IndividBusiness_Inn,
  IndividBusiness_Ogrn,
  ActiveType,
  Address,
  Fio,
  Name,
  OrganizationType,
  UserId,
  Birthday,
  DatePassport,
  NumberPassport,
  PassportDepartment,
  ResidenceAddress,
  SeriesPassport)
--company
  select
    ib.Id,
    Inn,
    Kpp,
    Ogrn,
    null       as IndividBusiness_Inn,
    null       as IndividBusiness_Ogrn,
    ActiveType,
    Address,
    Fio,
    Name,
    'Company'  as OrganizationType,
    ib.User_Id as UserId,
    null       as Birthday,
    null       as DatePassport,
    null       as NumberPassport,
    null       as PassportDepartment,
    null       as ResidenceAddress,
    null       as SeriesPassport
  from [uir-bd].dbo.Company c
    join [uir-bd].dbo.InfoBase ib on c.Id = ib.Id
  union all
  --SelfEmployed
  select
    ib.Id,
    null           as Inn,
    null           as Kpp,
    null           as Ogrn,
    null           as IndividBusiness_Inn,
    null           as IndividBusiness_Ogrn,
    ActiveType,
    Address,
    Fio,
    Name,
    'SelfEmployed' as OrganizationType,
    ib.User_Id     as UserId,
    Birthday,
    DatePassport,
    NumberPassport,
    PassportDepartment,
    ResidenceAddress,
    SeriesPassport
  from [uir-bd].dbo.SelfEmployed c
    join [uir-bd].dbo.InfoBase ib on c.Id = ib.Id
  --IndividBusiness
  union all
  select
    ib.Id,
    null              as Inn,
    null              as Kpp,
    null              as Ogrn,
    c.Inn             as IndividBusiness_Inn,
    c.Ogrn            as IndividBusiness_Ogrn,
    ActiveType,
    Address,
    Fio,
    Name,
    'IndividBusiness' as OrganizationType,
    ib.User_Id        as UserId,
    null              as Birthday,
    null              as DatePassport,
    null              as NumberPassport,
    null              as PassportDepartment,
    null              as ResidenceAddress,
    null              as SeriesPassport
  from [uir-bd].dbo.IndividBusiness c
    join [uir-bd].dbo.InfoBase ib on c.Id = ib.Id
SET IDENTITY_INSERT [uir-bd-core].dbo.Organizations OFF

SET IDENTITY_INSERT [uir-bd-core].dbo.Places ON
insert into [uir-bd-core].dbo.Places (id, Address, Description, Icon, LocationId, Name, OrganizationId, Phone, PriceLevel, Rating, Room, RoomType, Vicinity, Website)
  select
    id,
    Address,
    Description,
    Icon,
    Location_Id LocationId,
    Name,
    InfoBaseId  OrganizationId,
    Phone,
    PriceLevel,
    Rating,
    Room,
    RoomType,
    Vicinity,
    Website
  from [uir-bd].dbo.Places
  where InfoBaseId in (select id
                       from [uir-bd-core].dbo.Organizations);
SET IDENTITY_INSERT [uir-bd-core].dbo.Places OFF

insert into [uir-bd-core].dbo.CategoryOrganization (CategoryId, OrganizationId)
  select
    Category_Id,
    InfoBase_Id
  from [uir-bd].dbo.InfoBaseCategories
  where InfoBase_Id in (select id
                        from [uir-bd-core].dbo.Organizations);

insert into [uir-bd-core].dbo.Images ([File], Height, IsLogo, OrganizationId, PlaceId, Width)
  select
    [File],
    Height,
    IsLogo,
    InfoBase_Id OrganizationId,
    Place_Id    PlaceId,
    Width
  from [uir-bd].dbo.PartnerImages
  where Place_Id in (select id
                     from [uir-bd-core].dbo.Places)
        or InfoBase_Id in (select id
                           from [uir-bd-core].dbo.Organizations);

insert into [uir-bd-core].dbo.Sales (Amount, Description, IsActive, Name, OrganizationId, PlaceId)
  select
    Amount,
    Description,
    IsActive,
    Name,
    InfoBase_Id OrganizationId,
    Place_Id    PlaceId
  from [uir-bd].dbo.Sales
  where Place_Id in (select id
                     from [uir-bd-core].dbo.Places)

insert into [uir-bd-core].dbo.OrganizationTag (OrganizationId, TagId)
  select
    InfoBase_Id,
    Tag_Id
  from [uir-bd].dbo.TagInfoBases
  where InfoBase_Id in (select id
                        from [uir-bd-core].dbo.Organizations);

SET IDENTITY_INSERT [uir-bd-core].dbo.Videos ON
insert into [uir-bd-core].dbo.Videos (id, Discription, [File], Name, PlaceId)
  select
    id,
    Discription,
    [File],
    Name,
    Place_Id PlaceId
  from [uir-bd].dbo.Videos;
SET IDENTITY_INSERT [uir-bd-core].dbo.Videos OFF