﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UIR.DAL.Core.Migrations
{
    public partial class managers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ManagerType",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ManagerId",
                table: "Organizations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_ManagerId",
                table: "Organizations",
                column: "ManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_Users_ManagerId",
                table: "Organizations",
                column: "ManagerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_Users_ManagerId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_ManagerId",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "ManagerType",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ManagerId",
                table: "Organizations");
        }
    }
}
