﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace UIR.DAL.Core.Migrations
{
    public partial class Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookmarks_Partners_PlaceId",
                table: "Bookmarks");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryOrganization_GoogleCategories_CategoryId",
                table: "CategoryOrganization");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryTemplate_GoogleCategories_CategoryId",
                table: "CategoryTemplate");

            migrationBuilder.DropForeignKey(
                name: "FK_Overviews_Partners_PlaceId",
                table: "Overviews");

            migrationBuilder.DropForeignKey(
                name: "FK_Partners_Locations_LocationId",
                table: "Partners");

            migrationBuilder.DropForeignKey(
                name: "FK_Partners_Organizations_OrganizationId",
                table: "Partners");

            migrationBuilder.DropForeignKey(
                name: "FK_PlacePeriod_Partners_PlaceId",
                table: "PlacePeriod");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Partners_PlaceId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Video_Partners_PlaceId",
                table: "Video");

            migrationBuilder.DropTable(
                name: "PartnerImages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Video",
                table: "Video");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Partners",
                table: "Partners");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GoogleCategories",
                table: "GoogleCategories");

            migrationBuilder.DropColumn(
                name: "InfoBaseId",
                table: "Sales");

            migrationBuilder.RenameTable(
                name: "Video",
                newName: "Videos");

            migrationBuilder.RenameTable(
                name: "Partners",
                newName: "Places");

            migrationBuilder.RenameTable(
                name: "GoogleCategories",
                newName: "Categories");

            migrationBuilder.RenameIndex(
                name: "IX_Video_PlaceId",
                table: "Videos",
                newName: "IX_Videos_PlaceId");

            migrationBuilder.RenameIndex(
                name: "IX_Partners_OrganizationId",
                table: "Places",
                newName: "IX_Places_OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Partners_LocationId",
                table: "Places",
                newName: "IX_Places_LocationId");

            migrationBuilder.AlterColumn<long>(
                name: "PlaceId",
                table: "Videos",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "PlaceId",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Videos",
                table: "Videos",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Places",
                table: "Places",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Categories",
                table: "Categories",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    File = table.Column<string>(nullable: true),
                    Height = table.Column<int>(nullable: false),
                    IsLogo = table.Column<bool>(nullable: false),
                    OrganizationId = table.Column<long>(nullable: true),
                    PlaceId = table.Column<long>(nullable: true),
                    Width = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Images_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Images_Places_PlaceId",
                        column: x => x.PlaceId,
                        principalTable: "Places",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Images_OrganizationId",
                table: "Images",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_PlaceId",
                table: "Images",
                column: "PlaceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookmarks_Places_PlaceId",
                table: "Bookmarks",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryOrganization_Categories_CategoryId",
                table: "CategoryOrganization",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryTemplate_Categories_CategoryId",
                table: "CategoryTemplate",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Overviews_Places_PlaceId",
                table: "Overviews",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlacePeriod_Places_PlaceId",
                table: "PlacePeriod",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Locations_LocationId",
                table: "Places",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Organizations_OrganizationId",
                table: "Places",
                column: "OrganizationId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Places_PlaceId",
                table: "Sales",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_Places_PlaceId",
                table: "Videos",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookmarks_Places_PlaceId",
                table: "Bookmarks");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryOrganization_Categories_CategoryId",
                table: "CategoryOrganization");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryTemplate_Categories_CategoryId",
                table: "CategoryTemplate");

            migrationBuilder.DropForeignKey(
                name: "FK_Overviews_Places_PlaceId",
                table: "Overviews");

            migrationBuilder.DropForeignKey(
                name: "FK_PlacePeriod_Places_PlaceId",
                table: "PlacePeriod");

            migrationBuilder.DropForeignKey(
                name: "FK_Places_Locations_LocationId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Places_Organizations_OrganizationId",
                table: "Places");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Places_PlaceId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Videos_Places_PlaceId",
                table: "Videos");

            migrationBuilder.DropTable(
                name: "Images");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Videos",
                table: "Videos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Places",
                table: "Places");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Categories",
                table: "Categories");

            migrationBuilder.RenameTable(
                name: "Videos",
                newName: "Video");

            migrationBuilder.RenameTable(
                name: "Places",
                newName: "Partners");

            migrationBuilder.RenameTable(
                name: "Categories",
                newName: "GoogleCategories");

            migrationBuilder.RenameIndex(
                name: "IX_Videos_PlaceId",
                table: "Video",
                newName: "IX_Video_PlaceId");

            migrationBuilder.RenameIndex(
                name: "IX_Places_OrganizationId",
                table: "Partners",
                newName: "IX_Partners_OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Places_LocationId",
                table: "Partners",
                newName: "IX_Partners_LocationId");

            migrationBuilder.AlterColumn<long>(
                name: "PlaceId",
                table: "Video",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<long>(
                name: "PlaceId",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "InfoBaseId",
                table: "Sales",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Video",
                table: "Video",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Partners",
                table: "Partners",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GoogleCategories",
                table: "GoogleCategories",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "PartnerImages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    File = table.Column<string>(nullable: true),
                    Height = table.Column<int>(nullable: false),
                    InfoBaseId = table.Column<long>(nullable: false),
                    IsLogo = table.Column<bool>(nullable: false),
                    OrganizationId = table.Column<long>(nullable: true),
                    PlaceId = table.Column<long>(nullable: false),
                    Width = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartnerImages_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartnerImages_Partners_PlaceId",
                        column: x => x.PlaceId,
                        principalTable: "Partners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PartnerImages_OrganizationId",
                table: "PartnerImages",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_PartnerImages_PlaceId",
                table: "PartnerImages",
                column: "PlaceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookmarks_Partners_PlaceId",
                table: "Bookmarks",
                column: "PlaceId",
                principalTable: "Partners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryOrganization_GoogleCategories_CategoryId",
                table: "CategoryOrganization",
                column: "CategoryId",
                principalTable: "GoogleCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryTemplate_GoogleCategories_CategoryId",
                table: "CategoryTemplate",
                column: "CategoryId",
                principalTable: "GoogleCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Overviews_Partners_PlaceId",
                table: "Overviews",
                column: "PlaceId",
                principalTable: "Partners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Partners_Locations_LocationId",
                table: "Partners",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Partners_Organizations_OrganizationId",
                table: "Partners",
                column: "OrganizationId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlacePeriod_Partners_PlaceId",
                table: "PlacePeriod",
                column: "PlaceId",
                principalTable: "Partners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Partners_PlaceId",
                table: "Sales",
                column: "PlaceId",
                principalTable: "Partners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Video_Partners_PlaceId",
                table: "Video",
                column: "PlaceId",
                principalTable: "Partners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
