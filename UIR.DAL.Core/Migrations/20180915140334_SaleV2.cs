﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UIR.DAL.Core.Migrations
{
    public partial class SaleV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DiscountEndDate",
                table: "Sales",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<bool>(
                name: "IsExclusive",
                table: "Sales",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsExclusive",
                table: "Sales");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DiscountEndDate",
                table: "Sales",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
