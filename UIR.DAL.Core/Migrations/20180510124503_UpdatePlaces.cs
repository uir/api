﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace UIR.DAL.Core.Migrations
{
    public partial class UpdatePlaces : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Organizations_OrganizationId",
                table: "Places");

            migrationBuilder.DropColumn(
                name: "InfoBaseId",
                table: "Places");

            migrationBuilder.AlterColumn<long>(
                name: "OrganizationId",
                table: "Places",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Organizations_OrganizationId",
                table: "Places",
                column: "OrganizationId",
                principalTable: "Organizations",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Places_Organizations_OrganizationId",
                table: "Places");

            migrationBuilder.AlterColumn<long>(
                name: "OrganizationId",
                table: "Places",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "InfoBaseId",
                table: "Places",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddForeignKey(
                name: "FK_Places_Organizations_OrganizationId",
                table: "Places",
                column: "OrganizationId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
