﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UIR.DAL.Core.Migrations
{
    public partial class manager2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_Users_UserId",
                table: "Organizations");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Organizations",
                newName: "OwnerId");

            migrationBuilder.RenameIndex(
                name: "IX_Organizations_UserId",
                table: "Organizations",
                newName: "IX_Organizations_OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_Users_OwnerId",
                table: "Organizations",
                column: "OwnerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_Users_OwnerId",
                table: "Organizations");

            migrationBuilder.RenameColumn(
                name: "OwnerId",
                table: "Organizations",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Organizations_OwnerId",
                table: "Organizations",
                newName: "IX_Organizations_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_Users_UserId",
                table: "Organizations",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
