﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UIR.DAL.Core.Migrations
{
    public partial class fixstate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_PaymentPlans_PaymentPlanId",
                table: "Payments");

            migrationBuilder.AlterColumn<long>(
                name: "PaymentPlanId",
                table: "Payments",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_PaymentPlans_PaymentPlanId",
                table: "Payments",
                column: "PaymentPlanId",
                principalTable: "PaymentPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_PaymentPlans_PaymentPlanId",
                table: "Payments");

            migrationBuilder.AlterColumn<long>(
                name: "PaymentPlanId",
                table: "Payments",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_PaymentPlans_PaymentPlanId",
                table: "Payments",
                column: "PaymentPlanId",
                principalTable: "PaymentPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
