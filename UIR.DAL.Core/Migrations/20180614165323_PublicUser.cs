﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace UIR.DAL.Core.Migrations
{
    public partial class PublicUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Birthday",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DatePassport",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NumberPassport",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportDepartment",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResidenceAddress",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SeriesPassport",
                table: "Users",
                nullable: true);
            
            migrationBuilder.AddColumn<string>(
                name: "MiddleName",
                table: "Users",
                nullable: true);
            
            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        
            migrationBuilder.DropColumn(
                name: "Birthday",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DatePassport",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "NumberPassport",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PassportDepartment",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ResidenceAddress",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SeriesPassport",
                table: "Users");
        }
    }
}
