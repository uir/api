﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UIR.DAL.Core.Migrations
{
    public partial class CopyPlace : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ImagePlace",
                columns: table => new
                {
                    ImageId = table.Column<long>(nullable: false),
                    PlaceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImagePlace", x => new { x.PlaceId, x.ImageId });
                    table.ForeignKey(
                        name: "FK_ImagePlace_Images_ImageId",
                        column: x => x.ImageId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ImagePlace_Places_PlaceId",
                        column: x => x.PlaceId,
                        principalTable: "Places",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SalePlace",
                columns: table => new
                {
                    SaleId = table.Column<long>(nullable: false),
                    PlaceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalePlace", x => new { x.PlaceId, x.SaleId });
                    table.ForeignKey(
                        name: "FK_SalePlace_Places_PlaceId",
                        column: x => x.PlaceId,
                        principalTable: "Places",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SalePlace_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VideoPlace",
                columns: table => new
                {
                    VideoId = table.Column<long>(nullable: false),
                    PlaceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoPlace", x => new { x.PlaceId, x.VideoId });
                    table.ForeignKey(
                        name: "FK_VideoPlace_Places_PlaceId",
                        column: x => x.PlaceId,
                        principalTable: "Places",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_VideoPlace_Videos_VideoId",
                        column: x => x.VideoId,
                        principalTable: "Videos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ImagePlace_ImageId",
                table: "ImagePlace",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_SalePlace_SaleId",
                table: "SalePlace",
                column: "SaleId");

            migrationBuilder.CreateIndex(
                name: "IX_VideoPlace_VideoId",
                table: "VideoPlace",
                column: "VideoId");
            
            CopyData(migrationBuilder);
            
            migrationBuilder.DropForeignKey(
                name: "FK_Images_Places_PlaceId",
                table: "Images");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Places_PlaceId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Videos_Places_PlaceId",
                table: "Videos");

            migrationBuilder.DropIndex(
                name: "IX_Videos_PlaceId",
                table: "Videos");

            migrationBuilder.DropIndex(
                name: "IX_Sales_PlaceId",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Images_PlaceId",
                table: "Images");
            
            migrationBuilder.DropColumn(
                name: "PlaceId",
                table: "Videos");

            migrationBuilder.DropColumn(
                name: "PlaceId",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "PlaceId",
                table: "Images");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImagePlace");

            migrationBuilder.DropTable(
                name: "SalePlace");

            migrationBuilder.DropTable(
                name: "VideoPlace");

            migrationBuilder.AddColumn<long>(
                name: "PlaceId",
                table: "Videos",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "PlaceId",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PlaceId",
                table: "Images",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Videos_PlaceId",
                table: "Videos",
                column: "PlaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_PlaceId",
                table: "Sales",
                column: "PlaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_PlaceId",
                table: "Images",
                column: "PlaceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Places_PlaceId",
                table: "Images",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Places_PlaceId",
                table: "Sales",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Videos_Places_PlaceId",
                table: "Videos",
                column: "PlaceId",
                principalTable: "Places",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        private void CopyData(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("insert into ImagePlace(ImageId, PlaceId) select id, PlaceId from Images where PlaceId is not null;");
            migrationBuilder.Sql("insert into SalePlace(SaleId, PlaceId) select id, PlaceId from Sales where PlaceId is not null;");
            migrationBuilder.Sql("insert into VideoPlace(VideoId, PlaceId) select id, PlaceId from Videos;");
        }
    }
}
