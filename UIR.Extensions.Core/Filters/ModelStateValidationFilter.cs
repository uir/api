﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace UIR.Extensions.Core.Filters
{
    public class ModelStateValidationFilter : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context
                    .ModelState
                    .SelectMany(x => x.Value?.Errors)
                    .Where(x => x != null)
                    .Select(x => x.ErrorMessage)
                    .ToArray();
                context.Result = new JsonResult(errors)
                {
                    StatusCode = 422
                };
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}