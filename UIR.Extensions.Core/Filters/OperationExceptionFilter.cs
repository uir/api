﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using UIR.Extensions.Core.Exception;

namespace UIR.Extensions.Core.Filters
{
    public class OperationExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (!(context.Exception is OperationException exception))
            {
                return;
            }

            context.HttpContext.Response.StatusCode = (int)exception.StatusCode;
            context.Result = new JsonResult(new OperationError
            {
                Message = exception.Message
            });
        }
    }
}