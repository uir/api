﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Force.Ddd;
using Force.Ddd.Pagination;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Controller
{
    public abstract class RestControllerBase : GetControllerBase
    {
        protected RestControllerBase(DbContext dbContext) : base(dbContext)
        {
        }

        protected virtual async Task<IActionResult> AddRange<TEntity, TProject>(IEnumerable<TProject> items)
            where TEntity : class
        {
            var entities = Mapper.Map<IEnumerable<TEntity>>(items);
            await DbContext.Set<TEntity>().AddRangeAsync(entities);
            await DbContext.SaveChangesAsync();
            return Ok();
        }

        protected virtual async Task<IActionResult> Add<TEntity, TProject>(TProject item)
            where TEntity : HasIdBase<long>
        {
            var ent = Mapper.Map<TEntity>(item);
            var entityEntry = await DbContext.Set<TEntity>().AddAsync(ent);
            await DbContext.SaveChangesAsync();
            return Ok(entityEntry.Entity.Id);
        }

        protected virtual async Task<IActionResult> Delete<TEntity>(long id)
            where TEntity : class
        {
            var ent = DbContext.Set<TEntity>().Find(id);
            DbContext.Set<TEntity>().Remove(ent);
            await DbContext.SaveChangesAsync();
            return Ok();
        }

        protected virtual async Task<IActionResult> Update<TEntity, TProject>(TProject item)
            where TEntity : class
            where TProject : HasIdBase<long>
        {
            var ent = DbContext.Set<TEntity>().Find(item.Id);
            Mapper.Map(item, ent);
            await DbContext.SaveChangesAsync();
            return Ok();
        }
    }
}