﻿using System.Linq;
using Force.Ddd;
using Force.Ddd.Pagination;

namespace UIR.Extensions.Core.Controller
{
    /// <summary>
    /// Спецификация списочного запроса
    /// </summary>
    /// <typeparam name="TEnt">Тип сущности</typeparam>
    /// <typeparam name="TProject">Тип проекции</typeparam>
    public class ListSpec<TEnt, TProject> : IPaging, IQueryableFilter<TEnt>, IQueryableOrder<TProject>
        where TEnt : class
        where TProject : HasIdBase<long>
    {
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// Кол-во элементов на странице
        /// </summary>
        public int Take { get; set; } = 10;

        public virtual IQueryable<TEnt> Apply(IQueryable<TEnt> query) => query;

        public virtual IOrderedQueryable<TProject> OrderBy(IQueryable<TProject> queryable) =>
            queryable.OrderBy(x => x.Id);
    }
}