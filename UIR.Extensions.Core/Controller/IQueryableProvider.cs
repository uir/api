﻿using System;
using System.Linq;

namespace UIR.Extensions.Core.Controller
{
    public interface IQueryableProvider        
    {
        IQueryable<T> Query<T>()
            where T: class;
        
        IQueryable Query(Type type);
    }   
}