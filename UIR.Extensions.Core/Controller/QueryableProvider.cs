﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Controller
{
    public class QueryableProvider : IQueryableProvider
    {
        private static bool IsPermissionFilter(Type type) => IsPermissionFilter(type, null);

        private static bool IsPermissionFilter(Type type, Func<Type, bool> predicate)
            => type.GetInterfaces().Any(y =>
                y.IsGenericType && y.GetGenericTypeDefinition() == typeof(IPermissionFilter<>) &&
                (predicate?.Invoke(y) ?? true));

        private static Type[] Filters = AppDomain
            .CurrentDomain
            .GetAssemblies()
            .SelectMany(x => x.GetTypes())
            .Where(IsPermissionFilter)
            .ToArray();

        private readonly DbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public QueryableProvider(DbContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        private static MethodInfo QueryMethod = typeof(QueryableProvider)
            .GetMethods()
            .First(x => x.Name == "Query" && x.IsGenericMethod);

        private IQueryable<T> Filter<T>(IQueryable<T> queryable)
            => Filters
                .Where(x => IsPermissionFilter(x, y => y.GetGenericArguments().First() == typeof(T)))
                .Aggregate(
                    queryable,
                    (c, n) => ((dynamic) Activator.CreateInstance(n, _dbContext, _httpContextAccessor)).GetPermitted(queryable),
                    c => c);

        public IQueryable<T> Query<T>()
            where T : class
            => Filter(_dbContext.Set<T>());

        public IQueryable Query(Type type)
            => (IQueryable) QueryMethod.MakeGenericMethod(type).Invoke(_dbContext, new object[] { });
    }
}