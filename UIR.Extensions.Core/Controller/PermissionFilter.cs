﻿using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Controller
{
    public abstract class PermissionFilter<T>: IPermissionFilter<T>
    {
        protected static IQueryable<T> Empty = new T[] { }.AsQueryable();
        protected readonly DbContext DbContext;
        public abstract long? UserId { get; }
        protected PermissionFilter(DbContext dbContext)
        {
            DbContext = dbContext;
        }
        public abstract IQueryable<T> GetPermitted(IQueryable<T> queryable);
    }

    public interface IPermissionFilter<T>
    {
        IQueryable<T> GetPermitted(IQueryable<T> queryable);
    }
}