﻿using System;
using System.Linq;
using Force.Ddd;
using Force.Extensions;

namespace UIR.Extensions.Core.Controller
{
    public abstract class SearchListSpec<TEnt, TProject> : ListSpec<TEnt, TProject>
        where TEnt : class
        where TProject : HasIdBase<long>
    {
        public abstract Func<string, ForceSpec<TEnt>> SearchSpec { get; }
        public string SearchStr { get; set; }
        public string OrderByStr { get; set; }

        public override IQueryable<TEnt> Apply(IQueryable<TEnt> query) => base
            .Apply(query)
            .WhereIf(!string.IsNullOrEmpty(SearchStr), SearchSpec(SearchStr));

        public override IOrderedQueryable<TProject> OrderBy(IQueryable<TProject> queryable) =>
            string.IsNullOrEmpty(OrderByStr)
                ? base.OrderBy(queryable)
                : queryable.AutoSort(OrderByStr);
    }
}