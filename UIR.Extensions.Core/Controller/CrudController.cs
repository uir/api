﻿using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Ddd;
using Force.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;

namespace UIR.Extensions.Core.Controller
{
    public class CrudController: Microsoft.AspNetCore.Mvc.Controller
    {
        protected readonly DbContext DbContext;

        protected CrudController(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected IActionResult Add<TDto, TEnt>(TEnt dto) where TDto : class where TEnt : class => DbContext
            .Add(dto
                .ThrowIfNull(HttpStatusCode.InternalServerError)
                .Map<TEnt>())
            .PipeTo(Ok);

        protected IActionResult Delete<TEnt>(long id)
            where TEnt : HasIdBase<long> => DbContext
            .DeleteById<TEnt>(id.ThrowIf(x => x == 0, HttpStatusCode.InternalServerError))
            .PipeTo(x => Ok());

        protected IActionResult Get<TEnt, TDto>(long id) where TEnt : class => DbContext
            .GetById<TEnt>(id)
            .ThrowIfNull(HttpStatusCode.InternalServerError, "sale not found")
            .Map<TDto>()
            .PipeTo(x => Ok(x));

        protected IActionResult Edit<TEnt, TDto>(TDto dto, long id)
            where TEnt : HasIdBase<long>
            where TDto : HasIdBase<long>
        {
            DbContext.Edit<TEnt, TDto>(dto, id);
            return Ok();
        }

        protected IActionResult GetList<TEntity, TProject, TSpec>(TSpec spec)
            where TSpec : IQueryableFilter<TEntity>
            where TEntity : class
            => DbContext
                .FilterToArray<TEntity, TProject>(spec)
                .PipeTo(Ok);
        
        protected IActionResult GetList<TEntity, TProject>()
            where TEntity : class
            => DbContext
                .Set<TEntity>()
                .ProjectTo<TProject>()
                .PipeTo(Ok);
    }
}