﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Ddd;
using Force.Ddd.Pagination;
using Force.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Controller
{
    public abstract class GetController<TEntity, TProject, TDetailProject, TListSpec, TDetailSpec>: Microsoft.AspNetCore.Mvc.Controller
        where TEntity : class, IHasId<long>
        where TListSpec : IQueryableFilter<TEntity>, IQueryableOrder<TProject>, IPaging
        where TDetailSpec : HasIdBase<long>, IQueryableFilter<TEntity>, new()
        where TDetailProject : class
        where TProject : class
    {
        protected readonly IQueryableProvider QueryableProvider;

        protected GetController(IQueryableProvider queryableProvider)
        {
            QueryableProvider = queryableProvider;
        }
        
        /// <summary>
        /// Возвращает список записей
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(string[]), 400)]
        public virtual IActionResult Get(TListSpec spec)
        {
            return QueryableProvider
                .Query<TEntity>()
                .Where(spec)
                .ProjectTo<TProject>()
                .OrderBy(spec)
                .ToPagedResponse(spec)
                .PipeTo(Ok);
        }

        /// <summary>
        /// Возвращает одну запись
        /// </summary>
        [HttpGet("{id}")]
        [ProducesResponseType(204)]
        public virtual IActionResult Get(long id) =>
            QueryableProvider
                .Query<TEntity>()
                .Where(new TDetailSpec
                {
                    Id = id
                })
                .ProjectTo<TDetailProject>()
                .FirstOrDefault()
                .PipeTo(Ok);
    }
}