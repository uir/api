using System.Threading.Tasks;
using AutoMapper;
using Force.Ddd;
using Force.Ddd.Pagination;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Controller
{
    public abstract class RestController<TEntity, TProject, TProjectCreate, TDetailProject, TListSpec, TDetailSpec> :
        GetController<TEntity, TProject, TDetailProject, TListSpec, TDetailSpec>
        where TEntity : class, IHasId<long>
        where TListSpec : IQueryableFilter<TEntity>, IQueryableOrder<TProject>, IPaging
        where TProject : HasIdBase<long>
        where TDetailSpec : HasIdBase<long>, IQueryableFilter<TEntity>, new()
        where TDetailProject : class, IHasId<long>
    {
        protected readonly DbContext DbContext;

        protected RestController(IQueryableProvider queryableProvider, DbContext dbContext) : base(queryableProvider)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Удаляет данные
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public virtual async Task<IActionResult> Delete(long id)
        {
            var ent = await QueryableProvider.Query<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
            if (ent == null)
                return NotFound();
            DbContext.Set<TEntity>().Remove(ent);
            await DbContext.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Редактирует запись
        /// </summary>
        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(string[]), 400)]
        public virtual async Task<IActionResult> Update([FromBody] TDetailProject dto)
        {
            var ent = await QueryableProvider.Query<TEntity>().FirstOrDefaultAsync(x => x.Id == dto.Id);
            if (ent == null)
                return NotFound();
            Mapper.Map(dto, ent);
            await DbContext.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Добавляет запись
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(long), 200)]
        [ProducesResponseType(typeof(string[]), 400)]
        public virtual async Task<IActionResult> Add([FromBody] TProjectCreate dto)
        {
            var ent = Mapper.Map<TEntity>(dto);
            var entityEntry = await DbContext.Set<TEntity>().AddAsync(ent);
            await DbContext.SaveChangesAsync();
            return Ok(entityEntry.Entity.Id);
        }
    }
}