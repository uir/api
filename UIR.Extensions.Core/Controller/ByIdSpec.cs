﻿using System.Linq;
using Force.Ddd;

namespace UIR.Extensions.Core.Controller
{
    /// <summary>
    /// Спецификация запроса по id
    /// </summary>
    /// <typeparam name="TEnt">Тип сущности</typeparam>
    public class ByIdSpec<TEnt> : HasIdBase<long>, IQueryableFilter<TEnt>
        where TEnt : class, IHasId<long>
    {
        public virtual IQueryable<TEnt> Apply(IQueryable<TEnt> query) => query.Where(ent => ent.Id == Id);
    }
}