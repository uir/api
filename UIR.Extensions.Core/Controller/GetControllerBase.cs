﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using Force.Ddd;
using Force.Ddd.Pagination;
using Force.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Controller
{
    public abstract class GetControllerBase : Microsoft.AspNetCore.Mvc.Controller
    {
        protected readonly DbContext DbContext;

        protected GetControllerBase(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected virtual IActionResult Get<TEntity, TProject, TSpec>(long id,
            Expression<Func<TEntity, TProject>> projection)
            where TSpec : HasIdBase<long>, IQueryableFilter<TEntity>, new()
            where TProject : class
            where TEntity : class =>
            DbContext
                .Set<TEntity>()
                .Where(new TSpec
                {
                    Id = id
                })
                .Select(projection)
                .FirstOrDefault()
                .Do(AfterMap)
                .PipeTo(Ok);

        protected virtual IActionResult Get<TEntity, TProject, TSpec>(long id)
            where TSpec : HasIdBase<long>, IQueryableFilter<TEntity>, new()
            where TProject : class
            where TEntity : class, IHasId<long> =>
            DbContext
                .Set<TEntity>()
                .Where(new TSpec
                {
                    Id = id
                })
                .ProjectTo<TProject>()
                .FirstOrDefault()
                .Do(AfterMap)
                .PipeTo(Ok);

        protected virtual IActionResult GetList<TEntity, TProject, TSpec>(TSpec spec)
            where TSpec : IQueryableFilter<TEntity>, IQueryableOrder<TProject>, IPaging
            where TProject : class
            where TEntity : class =>
            DbContext
                .Set<TEntity>()
                .Where(spec)
                .ProjectTo<TProject>()
                .OrderBy(spec)
                .ToPagedResponse(spec)
                .Do(AfterMap)
                .PipeTo(Ok);

        protected virtual IActionResult GetList<TEntity, TProject, TSpec>(TSpec spec,
            Expression<Func<TEntity, TProject>> projection)
            where TSpec : IQueryableFilter<TEntity>, IQueryableOrder<TProject>, IPaging
            where TProject : class
            where TEntity : class =>
            DbContext
                .Set<TEntity>()
                .Where(spec)
                .Select(projection)
                .OrderBy(spec)
                .ToPagedResponse(spec)
                .Do(AfterMap)
                .PipeTo(Ok);

        protected virtual void AfterMap<TProject>(TProject item)
            where TProject : class
            => Expression.Empty();
    }
}