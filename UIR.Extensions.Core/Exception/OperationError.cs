﻿namespace UIR.Extensions.Core.Exception
{
    public class OperationError
    {
        public string Message { get; set; }
    }
}