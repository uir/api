﻿using System.Net;

namespace UIR.Extensions.Core.Exception
{
    public class OperationException : System.Exception
    {
        public HttpStatusCode StatusCode { get; }

        public OperationException(string message, HttpStatusCode code = HttpStatusCode.BadRequest) : base(message)
        {
            StatusCode = code;
        }
    }

    public class HttpOperationException : System.Exception
    {
        public HttpStatusCode Code { get; set; }

        public HttpOperationException(HttpStatusCode code, string message) : base(message)
        {
            Code = code;
        }
    }
}