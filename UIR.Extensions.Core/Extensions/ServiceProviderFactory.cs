using System;

namespace UIR.Extensions.Core.Extensions
{
    public static class ServiceProviderFactory
    {
        public static IServiceProvider ServiceProvider { get; set; }
    }
}