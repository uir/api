﻿using System;
using System.Net;
using Force.Ddd;
using UIR.Extensions.Core.Exception;

namespace UIR.Extensions.Core.Extensions
{
    public static class ObjectExtension
    {
        public static T ThrowIf<T>(this T src, Func<T, bool> condition, string message)
        {
            if (condition(src)) throw new NullReferenceException(message);
            else return src;
        }

        public static T ThrowIf<T>(this T src, Func<T, bool> condition, HttpStatusCode code = HttpStatusCode.BadRequest, string message = "")
        {
            if (condition(src)) throw new OperationException(message, code);
            else return src;
        }

        public static T ThrowIfDefault<T>(this T src, HttpStatusCode code = HttpStatusCode.BadRequest,
            string message = "")
        {
            if (src.Equals(default(T))) throw new OperationException(message, code);
            else return src;
        }

        public static T ThrowIfNull<T>(this T? src, string message) where T: struct
        {
            if (src != null) return src.Value;
            else throw new NullReferenceException(message);
        }
        
        public static T ThrowIfNull<T>(this T src, string message)
        {
            if (src != null) return src;
            else throw new NullReferenceException(message);
        }
        
        public static T ThrowIfNotNull<T>(this T src, string message) where T: class 
        {
            if (src == null) 
                return src;
            else 
                throw new NullReferenceException(message);
        }
        
        public static T ThrowIfNull<T>(this T? src, HttpStatusCode code, string message = "") where T: struct
        {
            if (src != null) return src.Value;
            else throw new OperationException(message, code);
        }

        public static T ThrowIfNull<T>(this T src, HttpStatusCode code, string message = "")
        {
            if (src != null) return src;
            else throw new OperationException(message, code);
        }

        public static T SetId<T>(this T src, long id) where T: HasIdBase<long>
        {
            src.Id = id;
            return src;
        }
    }
}