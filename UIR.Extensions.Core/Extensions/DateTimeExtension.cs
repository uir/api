﻿using System;

namespace UIR.Extensions.Extensions
{
    public static class DateTimeExtension
    {
        public static DateTime GetThisMonthStartDate(this DateTime dateTime) => 
            DateTime.Today.AddDays(DateTime.Today.Day - 1).AddDays(1);
    }
}