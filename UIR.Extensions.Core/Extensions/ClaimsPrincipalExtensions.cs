﻿using System.Security.Claims;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;

namespace UIR.Extensions.Core.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        /// <summary>
        /// User ID
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static long GetId(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return default(long);

            var currentUser = user;
            return long.Parse(currentUser.GetClaim(OpenIdConnectConstants.Claims.Subject));
        }
        
        public static string[] GetRoles(this ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return new string[]{};

            var currentUser = user;
            return currentUser.GetClaim(OpenIdConnectConstants.Claims.Role)?.Split(",");
        }
    }
}