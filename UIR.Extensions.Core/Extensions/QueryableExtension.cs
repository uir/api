﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Ddd;
using Force.Extensions;
using Microsoft.EntityFrameworkCore;

namespace UIR.Extensions.Core.Extensions
{
    public static class QueryableExtension
    {
        public static TE Add<TE>(this DbContext src, TE e) where TE : class
        {
            src.Set<TE>().Add(e);
            return e;
        }

        public static DbContext DeleteById<TE>(this DbContext src, long id) where TE : class
        {
            var ent = src.Set<TE>().Find(id);
            if (ent == null)
                throw new ArgumentException("not found");
            src.Set<TE>().Remove(ent);
            src.SaveChanges();
            return src;
        }
        
        public static DbContext RemoveById<TE>(this DbContext src, long id) where TE : class
        {
            var ent = src.Set<TE>().Find(id);
            if (ent == null)
                throw new ArgumentException("not found");
            src.Set<TE>().Remove(ent);
            return src;
        }

        public static void Edit<TEnt, TDto>(this DbContext src, TDto dto, long id) where TEnt : class
        {
            src
                .Set<TEnt>()
                .Find(id)
                .PipeTo(x => Mapper.Map(dto, x));
            src.SaveChanges();
        }

        public static TE GetById<TE>(this DbContext src, object id) where TE : class
            => src.Set<TE>().Find(id);
        
        public static TR FirstById<TE, TR>(this DbContext src, long id) where TE : HasIdBase<long> => src
            .Set<TE>()
            .Where(x => x.Id == id)
            .ProjectTo<TR>()
            .FirstOrDefault();

        public static TR FirstBySpec<TE, TR>(this DbContext src, IQueryableFilter<TE> spec) where TE : HasIdBase<long>
            => spec.Apply(src.Set<TE>()).MaybeOrderBy(spec).ProjectTo<TR>().FirstOrDefault();
        
        public static TE FirstBySpec<TE>(this DbContext src, IQueryableFilter<TE> spec) where TE : HasIdBase<long>
            => spec.Apply(src.Set<TE>()).MaybeOrderBy(spec).FirstOrDefault();

        public static TProject[] FilterToArray<TE, TProject>(this DbContext src, IQueryableFilter<TE> filter) where TE : class
            => filter.Apply(src.Set<TE>()).ProjectTo<TProject>().ToArray();

        public static TProject[] FilterToArray<TE, TProject>(this DbContext src, Expression<Func<TE, bool>> filter) where TE : class
            => src.Set<TE>().Where(filter).ProjectTo<TProject>().ToArray();
        
        public static TRes[] FilterToArray<TE, TRes>(this DbContext src, Expression<Func<TE, bool>> filter, Expression<Func<TE, TRes>> selector) where TE : class
            => src.Set<TE>().Where(filter).Select(selector).ToArray();

        public static IQueryable<TE> Where<TE>(this DbContext src, Expression<Func<TE, bool>> where) where TE : class
            => src.Set<TE>().Where(where);
        
        public static IQueryable<TE> Where<TE>(this DbContext src, IQueryableFilter<TE> filter) where TE : class
            => filter.Apply(src.Set<TE>());

        public static TE FirstOrDefault<TE>(this DbContext src, Expression<Func<TE, bool>> where) where TE : class
            => src.Set<TE>().FirstOrDefault(where);

        public static IQueryable<TProject> Filter<TE, TProject>(this DbContext src, IQueryableFilter<TE> filter) where TE : class
            => filter.Apply(src.Set<TE>()).ProjectTo<TProject>();
        
        public static IQueryable<TProject> Filter<TE, TProject>(this DbContext src, Expression<Func<TE, bool>> where) where TE : class
            => src.Set<TE>().Where(where).ProjectTo<TProject>();

        public static IQueryable<TProject> Filter<TE, TProject>(this IQueryable<TE> query, IQueryableFilter<TE> filter) where TE : class
            => filter.Apply(query).ProjectTo<TProject>();
    }
}