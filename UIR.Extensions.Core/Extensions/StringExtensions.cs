﻿using System;
using Microsoft.Extensions.Options;

namespace UIR.Extensions.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ToPath(this string file, Uri uri, string cloudHost) => "//" + cloudHost + "/" + file;

        public static string ToPathImg(this string file, Uri uri, string cloudHost)
        {
            if (string.IsNullOrEmpty(file))
                return null;
            file = "//" + cloudHost + "/img/" + file;
            return file;
        }

        public static string ToPathImg(this string file)
        {
            var settings =
                (IOptions<AppSettings.AppSettings>) ServiceProviderFactory.ServiceProvider.GetService(
                    typeof(IOptions<AppSettings.AppSettings>));
            var cloudHost = settings.Value.CloudHost;
            if (string.IsNullOrEmpty(file))
                return null;
            file = "//" + cloudHost + "/img/" + file;
            return file;
        }
        
        public static string ToPathVideo(this string file)
        {
            var settings =
                (IOptions<AppSettings.AppSettings>) ServiceProviderFactory.ServiceProvider.GetService(
                    typeof(IOptions<AppSettings.AppSettings>));
            var cloudHost = settings.Value.CloudHost;
            return $"//{cloudHost}/video/{file}";
        }

        public static string ToPathImg(this string file, string cloudHost)
        {
            if (string.IsNullOrEmpty(file))
                return null;
            file = "//" + cloudHost + "/img/" + file;
            return file;
        }

        public static string ToPathVideo(this string file, Uri uri, string cloudHost) => $"//{cloudHost}/video/{file}";
    }
}