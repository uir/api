﻿using System.Collections.Generic;
using AutoMapper;

namespace UIR.Extensions.Core.Extensions
{
    public static class MapperExtension
    {
        public static TDest Map<TDest>(this object obj) => Mapper.Map<TDest>(obj);
        public static IEnumerable<TDest> Map<TDest>(this IEnumerable<object> obj) => Mapper.Map<IEnumerable<TDest>>(obj);
    }
}