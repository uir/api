﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using UIR.Extensions.Core.Controller;
using UIR.Service.Core.FileService;
using UIR.Service.Core.FileService.FileUploader;
using UIR.Service.Core.Payment.CountCheckers;
using UIR.Service.Core.TokenService;
using UIR.Sms.Core;
using UIR.Sms.Core.Smsc;

namespace UIR.Service.Core
{
    public static class ServicesModule
    {
        public static void AddUirServices(this IServiceCollection services, IConfiguration configuration,
            IHostingEnvironment env)
        {
            services.AddScoped<ImageFileService>();
            services.AddScoped<ImageFromUrlFileService>();
            services.AddScoped<VideoFileService>();
            services.AddScoped<IFileStorage, UirRemoteFileStorage>();

            services.AddSingleton<EmailService>();
            services.AddSingleton<ISmsProvider, SmscSmsProvider>();

            if (env.IsDevelopment())
            {
                services.AddScoped<SmsTokenService, SmsDebugTokenService>();
                services.AddScoped<EmailTokenService, EmailDebugTokenService>();
            }
            if (env.IsProduction() || env.IsStaging())
            {
                services.AddScoped<SmsTokenService>();
                services.AddScoped<EmailTokenService>();
            }

            services.AddScoped<ImagesPaymentPlanPartnerCountChecker>();
            services.AddScoped<VideosPaymentPlanPartnerCountChecker>();

            services.AddScoped<IQueryableProvider, QueryableProvider>();
        }
    }
}