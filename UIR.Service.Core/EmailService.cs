﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NLog;

namespace UIR.Service.Core
{
    public class EmailService
    {
        private readonly AppSettings.AppSettings _settings;

        public EmailService(IOptions<AppSettings.AppSettings> options)
        {
            _settings = options.Value;
        }

        public async Task Send(string email, string title, string body)
        {
            try
            {
                using (var client = new SmtpClient(_settings.EmailSettings.Host, _settings.EmailSettings.Port))
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_settings.EmailSettings.Login, _settings.EmailSettings.Password);
                    client.EnableSsl = true;
                    var msg = new MailMessage
                    {
                        From = new MailAddress(_settings.EmailSettings.Login),
                        Subject = title,
                        Body = body,
                        BodyEncoding = Encoding.GetEncoding("utf-8"),
                        SubjectEncoding = Encoding.UTF8
                    };
                    msg.To.Add(new MailAddress(email));
                    await client.SendMailAsync(msg);
                }
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Fatal($"{email} " + e.Message);
                throw e;
            }
        }
    }
}