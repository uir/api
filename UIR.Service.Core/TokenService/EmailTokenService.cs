﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Service.Core.TokenService
{
    public class EmailTokenService: BaseTokenService
    {
        private readonly EmailService _emailService;

        public EmailTokenService(ILogger<EmailTokenService> logger, UserManager<User> userManager, EmailService emailService) 
            : base(logger, userManager, TokenOptions.DefaultEmailProvider)
        {
            _emailService = emailService;
        }

        protected override async Task Notify(User user, string token)
        {
            NotifyLog(user, token);
            var text = $"UIR.ONE verification code: {token}";
            var title = "UIR.ONE verification";
            await _emailService.Send(user.Email, title, text);
        }

        public virtual async Task SendChangePasswordToken(User user)
        {
            var token = await UserManager.GenerateTwoFactorTokenAsync(user, TokenProviderName);
            var text = $"UIR.ONE change password code: {token}";
            var title = "UIR.ONE change password";
            await _emailService.Send(user.Email, title, text);
        }
    }
}