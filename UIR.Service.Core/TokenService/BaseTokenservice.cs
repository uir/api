﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Exception;

namespace UIR.Service.Core.TokenService
{
    public abstract class BaseTokenService : ITokenService
    {
        private readonly ILogger _logger;
        protected readonly UserManager<User> UserManager;
        public string TokenProviderName { get; }

        protected BaseTokenService(ILogger logger, UserManager<User> userManager, string tokenProviderName)
        {
            _logger = logger;
            UserManager = userManager;
            TokenProviderName = tokenProviderName;
        }

        protected abstract Task Notify(User user, string token);

        protected void NotifyLog(User user, string token) =>
            _logger.Log(LogLevel.Information, $"User: {user.UserName} - token {token}");

        public virtual async Task SendToken(User user)
        {
            var token = await UserManager.GenerateTwoFactorTokenAsync(user, TokenProviderName);
            if (token == null)
                throw new OperationException("you exceed the possible number of SMS requests");
            await Notify(user, token);
        }

        public virtual async Task ValidateToken(User user, string token)
        {
            if (!await UserManager.VerifyTwoFactorTokenAsync(user, TokenProviderName, token))
                throw new OperationException("Неправильный код");
        }
    }
}