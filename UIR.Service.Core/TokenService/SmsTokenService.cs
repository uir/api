﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Sms.Core;

namespace UIR.Service.Core.TokenService
{
    public class SmsTokenService : BaseTokenService
    {
        private readonly ISmsProvider _smsProvider;

        public SmsTokenService(ILogger<SmsTokenService> logger, UserManager<User> userManager, ISmsProvider smsProvider) :
            base(logger, userManager, TokenOptions.DefaultPhoneProvider)
        {
            _smsProvider = smsProvider;
        }

        protected override async Task Notify(User user, string token)
        {
            NotifyLog(user, token);
            var text = $"UIR.ONE verification code: {token}";
            await _smsProvider.Send(user.PhoneNumber, text);
        }
    }
}