﻿using System.Threading.Tasks;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Service.Core.TokenService
{
    public interface ITokenService
    {
        Task SendToken(User user);

        Task ValidateToken(User user, string token);
    }
}