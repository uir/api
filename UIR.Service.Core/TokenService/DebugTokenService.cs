using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Service.Core.TokenService
{
    public class SmsDebugTokenService : SmsTokenService
    {
        public SmsDebugTokenService(ILogger<SmsDebugTokenService> logger, UserManager<User> userManager) : base(logger,
            userManager, null)
        {
        }

        protected override Task Notify(User user, string token) => Task.Run(() => NotifyLog(user, token));
    }

    public class EmailDebugTokenService : EmailTokenService
    {
        public EmailDebugTokenService(ILogger<EmailDebugTokenService> logger, UserManager<User> userManager) : base(
            logger, userManager, null)
        {
        }

        protected override Task Notify(User user, string token) => Task.Run(() => NotifyLog(user, token));

        public override async Task SendChangePasswordToken(User user)
        {
            var token = await UserManager.GenerateTwoFactorTokenAsync(user, TokenProviderName);
            NotifyLog(user, token);
        }
    }
}