﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Shared.Core.PaySystem.Filters;
using UIR.Domain.PaySystem.Enums;
using UIR.Extensions.Core.Extensions;

namespace UIR.Service.Core.Payment.CountCheckers
{
    public abstract class PaymentPlanPartnerCountCheckerBase : IPaymentPlanPartnerCountChecker
    {
        public abstract IDictionary<PaymentPlanValue, int> AllowCount { get; }
        public abstract int AllowNotPayCount { get; }
        protected readonly DbContext DbContext;
        protected abstract string Error { get; }

        protected PaymentPlanPartnerCountCheckerBase(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        public string[] CheckCount(long userId, int count, long placeId)
        {
            var value = GetPaymentPlanValue(userId);
            var currentCount = GetCurrentCount(placeId);
            var allowCount = value == null ? AllowNotPayCount : AllowCount[value.Value];
            return GetErrors(count, allowCount, placeId, Error, currentCount);
        }

        protected abstract int GetCurrentCount(long placeId);

        private PaymentPlanValue? GetPaymentPlanValue(long userId)
        {
            var lastPayment = DbContext.FirstBySpec(new LastSuccessPaymentFilter(userId));
            return lastPayment?.PaymentPlan?.Value;
        }

        private string[] GetErrors(int count, int allowCount, long placeId, string error, int currentCount)
        {
            count += currentCount;
            if (allowCount == 0)
                return new List<string>().ToArray();
            return count > allowCount
                ? new[] {string.Format(error, allowCount)}
                : new List<string>().ToArray();
        }
    }
}