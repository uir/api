﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Service.Core.Payment.CountCheckers
{
    public class ImagesPaymentPlanPartnerCountChecker : PaymentPlanPartnerCountCheckerBase
    {
        public ImagesPaymentPlanPartnerCountChecker(DbContext dbContext) : base(dbContext)
        {
        }

        public override IDictionary<PaymentPlanValue, int> AllowCount => new Dictionary<PaymentPlanValue, int>
        {
            {PaymentPlanValue.Free, 10},
            {PaymentPlanValue.MiddlePartnerValue, 20},
            {PaymentPlanValue.HigthPartnerValue, 100},
        };

        public override int AllowNotPayCount => 5;

        protected override string Error => "Для данного тарифного плана максимальное число изображений {0}";

        protected override int GetCurrentCount(long placeId) =>
            DbContext.Set<ImagePlace>().Count(x => x.PlaceId == placeId);
    }
}