﻿namespace UIR.Service.Core.Payment.CountCheckers
{
    public interface IPaymentPlanPartnerCountChecker
    {
        string[] CheckCount(long userId, int count, long placeId);
    }
}