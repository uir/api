﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Service.Core.Payment.CountCheckers
{
    public class VideosPaymentPlanPartnerCountChecker : PaymentPlanPartnerCountCheckerBase
    {
        public VideosPaymentPlanPartnerCountChecker(DbContext dbContext) : base(dbContext)
        {
        }

        public override IDictionary<PaymentPlanValue, int> AllowCount => new Dictionary<PaymentPlanValue, int>
        {
            {PaymentPlanValue.Free, 2},
            {PaymentPlanValue.MiddlePartnerValue, 5},
            {PaymentPlanValue.HigthPartnerValue, 20},
        };

        public override int AllowNotPayCount => 1;

        protected override string Error => "Для данного тарифного плана максимальное число видеозаписей {0}";

        protected override int GetCurrentCount(long placeId) =>
            DbContext.Set<VideoPlace>().Count(x => x.PlaceId == placeId);
    }
}