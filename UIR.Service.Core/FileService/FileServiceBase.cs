﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MimeTypes.Core;
using UIR.Service.Core.FileService.FileUploader;
using UIR.Service.Core.FileService.Model;

namespace UIR.Service.Core.FileService
{
    public abstract class FileServiceBase<T> : IFileService<T> where T : FileModel, new()
    {
        private readonly IFileStorage _fileStorage;
        private readonly string _folderName;

        protected FileServiceBase(IFileStorage fileStorage, string folderName)
        {
            _fileStorage = fileStorage;
            _folderName = folderName;
        }

        public async Task<T[]> SaveAsync(IEnumerable<FileContent> files)
        {
            var createdFiles = new List<T>();
            foreach (var file in files)
            {
                var fileModel = CreateFileModel(file);
                createdFiles.Add(fileModel);
                await UploadFile(file.Bytes, fileModel.Name);
            }

            return createdFiles.ToArray();
        }

        public virtual async Task Remove(string fileName) => await _fileStorage.RemoveAsync(_folderName, fileName);

        protected virtual T CreateFileModel(FileContent file)
        {
            var headerFileName = file.FileName;
            var extension = headerFileName != null
                ? Path.GetExtension(headerFileName)
                : MimeTypeMap.GetExtension(file.MediaType);
            var fileName = Guid.NewGuid() + extension;
            return new T
            {
                Name = fileName
            };
        }

        protected virtual async Task UploadFile(byte[] bytes, string fileName) =>
            await _fileStorage.UploadAsync(bytes, _folderName, fileName);
    }
}