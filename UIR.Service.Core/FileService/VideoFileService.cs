﻿using Microsoft.Extensions.Options;
using UIR.Service.Core.FileService.FileUploader;
using UIR.Service.Core.FileService.Model;

namespace UIR.Service.Core.FileService
{
    public class VideoFileService : FileServiceBase<FileModel>
    {
        public string FolderName { get; set; }
        
        public VideoFileService(IFileStorage fileStorage, IOptions<AppSettings.AppSettings> options) : 
            base(fileStorage, options.Value.FileStorageSettings.VideoFolder)
        {
            FolderName = options.Value.FileStorageSettings.VideoFolder;
        }
    }
}