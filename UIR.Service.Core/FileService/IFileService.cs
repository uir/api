﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UIR.Service.Core.FileService.Model;

namespace UIR.Service.Core.FileService
{
    public interface IFileService<T> where T : FileModel
    {
        Task<T[]> SaveAsync(IEnumerable<FileContent> files);
        Task Remove(string name);
    }
}