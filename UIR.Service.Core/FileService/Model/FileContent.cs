﻿namespace UIR.Service.Core.FileService.Model
{
    public class FileContent
    {
        public byte[] Bytes { get; }
        public string FileName { get; }
        public string MediaType { get; set; }

        public FileContent(byte[] bytes, string fileName)
        {
            Bytes = bytes;
            FileName = fileName;
        }
    }
}