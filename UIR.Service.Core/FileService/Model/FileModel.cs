﻿namespace UIR.Service.Core.FileService.Model
{
    public class FileModel
    {
        public string Name { get; set; }
    }

    public class ImageFileModel : FileModel
    {
        
        public int Width { get; set; }
        public int Heigth { get; set; }
    }
}