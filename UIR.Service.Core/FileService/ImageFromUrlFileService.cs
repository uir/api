﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MimeTypes.Core;
using UIR.Service.Core.FileService.FileUploader;
using UIR.Service.Core.FileService.Model;

namespace UIR.Service.Core.FileService
{
    public class ImageFromUrlFileService : ImageFileService
    {
        protected static HttpClient _client = new HttpClient();

        public ImageFromUrlFileService(IFileStorage fileStorage, IOptions<AppSettings.AppSettings> options) : 
            base(fileStorage, options)
        {
        }

        public virtual async Task<ImageFileModel[]> MultipleUploadAsync(Uri[] urls)
        {
            var files = new List<ImageFileModel>();
            foreach (var url in urls)
            {
                using (var ms = new MemoryStream())
                {
                    var responce = await _client.GetAsync(url);
                    var fileStream = await responce.Content.ReadAsStreamAsync();
                    var headerFileName = responce.Content.Headers?.ContentDisposition?.FileName?.Trim('\"');
                    var extension = headerFileName != null
                        ? Path.GetExtension(headerFileName)
                        : MimeTypeMap.GetExtension(responce.Content.Headers?.ContentType.MediaType);
                    var fileName = Guid.NewGuid() + extension;
                    await fileStream.CopyToAsync(ms);
                    ms.Position = 0;
                    var fileModel = await SaveAsync(new[] {new FileContent(ms.ToArray(), fileName)});
                    files.AddRange(fileModel);
                }
            }

            return files.ToArray();
        }
    }
}