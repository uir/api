﻿using System.Threading.Tasks;

namespace UIR.Service.Core.FileService.FileUploader
{
    public interface IFileStorage
    {
        Task UploadAsync(byte[] bytes, string folder, string filename);
        Task RemoveAsync(string folder, string filename);
    }
}