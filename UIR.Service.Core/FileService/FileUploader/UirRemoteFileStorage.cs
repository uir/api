﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace UIR.Service.Core.FileService.FileUploader
{
    public class UirRemoteFileStorage : IFileStorage
    {
        private HttpClient _httpClient;
        private string _cloudHost;
        
        public UirRemoteFileStorage(IOptions<AppSettings.AppSettings> options)
        {
            _httpClient = new HttpClient();
            _cloudHost = options.Value.CloudHost;
        }

        public async Task UploadAsync(byte[] bytes, string folder, string filename)
        {
            var url = $@"http://{_cloudHost}/api/files?fileName={filename}&folder={folder}";
            await _httpClient.PostAsync(url, new ByteArrayContent(bytes));
        }

        public async Task RemoveAsync(string folder, string filename)
        {
            var url = $@"http://{_cloudHost}/api/files?fileName={filename}&folder={folder}";
            await _httpClient.DeleteAsync(url);
        }
    }
}