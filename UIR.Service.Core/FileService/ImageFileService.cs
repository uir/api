﻿using Microsoft.Extensions.Options;
using SixLabors.ImageSharp;
using UIR.Service.Core.FileService.FileUploader;
using UIR.Service.Core.FileService.Model;

namespace UIR.Service.Core.FileService
{
    public class ImageFileService : FileServiceBase<ImageFileModel>
    {
        public ImageFileService(IFileStorage fileStorage, IOptions<AppSettings.AppSettings> options) : 
            base(fileStorage, options.Value.FileStorageSettings.ImageFolder)
        {
        }
        
        protected override ImageFileModel CreateFileModel(FileContent file)
        {
            var filemodel = base.CreateFileModel(file);
            var image = Image.Load(file.Bytes);
            filemodel.Heigth = image.Height;
            filemodel.Width = image.Width;
            return filemodel;
        }
    }
}