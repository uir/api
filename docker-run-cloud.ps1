docker build -f ./UIR.Cloud/Dockerfile -t uir.cloud.prod .
docker container stop uir.cloud.prod
docker container rm uir.cloud.prod
docker run -p 5002:80 -d --name uir.cloud.prod -v C:/uir.data:c:/app/wwwroot uir.cloud.prod