﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.Partner.Api.Core.Modules.Admin;
using UIR.Partner.Api.Core.Modules.Company;
using UIR.Partner.Api.Core.Modules.Place;
using UIR.Partner.Api.Core.Modules.Statistic;
using UIR.Partner.Api.Core.Modules.User;

namespace UIR.Partner.Api.Core
{
    public static class BlPartnerModule
    {
        public static void AddUirPartner(this IServiceCollection services, IConfiguration configuration)
        {
            new CompanyModule().Register(services);
            new PlaceModule().Register(services);
            new UserModule().Register(services);
            new AdminModule().Register(services);
            new StatisticModule().Register(services);
        }
    }
}