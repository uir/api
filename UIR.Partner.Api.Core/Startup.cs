﻿using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.AppSettings.Extensions;
using UIR.Auth.Core;
using UIR.DAL.Core;
using UIR.Extensions.Core.Filters;
using UIR.Service.Core;

namespace UIR.Partner.Api.Core
{
    public class Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddUirEf(Configuration);
            services.AddUirServices(Configuration, Environment);
            services.AddUirAuth(new AuthSettings
            {
                RequireConfirmedEmail = true,
                RequireConfirmedPhoneNumber = false
            });
            services.AddMvc(options =>
            {
                options.Filters.Add<OperationExceptionFilter>();
                options.Filters.Add<ModelStateValidationFilter>();
            });
            services.AddUirBaseServices(Configuration);
            services.AddAutoMapper(
                typeof(Startup).Assembly,
                Assembly.Load("UIR.Bl.Shared.Core"),
                Assembly.Load("UIR.Service.Core"));
            services.AddHttpContextAccessor();
            
            services.AddUirPartner(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
            app.UseUirSwagger();
        }
    }
}