﻿using System.Linq;
using Force.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Controller;

namespace UIR.Partner.Api.Core.Modules.Category

{
    [Route("api/category")]
    public class CategoryController : CrudController
    {
        public CategoryController(DbContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Возвращает список категорий
        /// </summary>
        /// <returns></returns>
        [Route("all/short")]
        [HttpGet]
        public IActionResult GetAllCategries() => DbContext
            .Set<Template>()
            .Include(x => x.Categories)
            .ThenInclude(x => x.Category)
            .Select(x =>
                new
                {
                    x.Id,
                    x.Name,
                    Categories = x.Categories.Select(c => new {Id = c.CategoryId, c.Category.Name}).OrderBy(z => z.Name)
                })
            .ToArray()
            .OrderBy(x => x.Name)
            .PipeTo(Ok);
    }
}