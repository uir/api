using System.Collections.Generic;
using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Response.Organizations
{
    public class OrganizationDetailDto: HasIdBase<long>

    {
    public string Name { get; set; }
    public string Address { get; set; }
    public string Fio { get; set; }
    public bool IsDraft { get; set; }
    public string Login { get; set; }
    public IEnumerable<OrganizationDetailPlaceDto> Places { get; set; }
    }
}