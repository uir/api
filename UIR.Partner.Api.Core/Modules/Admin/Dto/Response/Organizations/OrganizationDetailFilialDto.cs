namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Response.Organizations
{
    public class OrganizationDetailPlaceDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
    }
}