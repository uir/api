using System;
using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Response.Organizations
{
    public class OrganizationDto : HasIdBase<long>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Fio { get; set; }
        public string Manager { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public bool IsDraft { get; set; }
    }
}