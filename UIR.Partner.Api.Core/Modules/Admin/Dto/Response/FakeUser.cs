namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Response
{
    public class FakeUser
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}