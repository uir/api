namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Request
{
    public class Base64FileDto
    {
        public string File { get; set; }
        public string Name { get; set; }
    }
}