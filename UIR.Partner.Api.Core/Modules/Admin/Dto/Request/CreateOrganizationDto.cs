﻿using AutoMapper;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Request
{
    public class CreateOrganizationDto
    {
        public Base64FileDto Logo { get; set; }
        public string Name { get; set; }
        public string Fio { get; set; }
        public string Address { get; set; }
        public ActiveType ActiveType { get; set; }
        public string Inn { get; set; }
        public string Ogrn { get; set; }
        public string Kpp { get; set; }
        public string NumberPassport { get; set; }
        public string SeriesPassport { get; set; }
        public string ResidenceAddress { get; set; }
        public string Birthday { get; set; }
        public string PassportDepartment { get; set; }
        public string DatePassport { get; set; }
        public long OwnerId { get; set; }
        
        [IgnoreMap] public long ManagerId { get; set; }

        [IgnoreMap] public long[] CategoriesIds { get; set; } = new long[0];
    }
}