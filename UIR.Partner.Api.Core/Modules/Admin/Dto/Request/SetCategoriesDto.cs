using System.ComponentModel.DataAnnotations;

namespace UIR.Partner.Api.Core.Modules.Admin.Dto.Request
{
    public class SetCategoriesDto
    {
        [Required]
        public long OrganizationId { get; set; }
        
        [Required]
        public long[] CategoriesIds { get; set; }
    }
}