﻿using UIR.Service.Core.FileService.Model;

namespace UIR.Partner.Api.Core.Modules.Admin.Dto
{
    public class UploadLogoDto
    {
        public FileContent Content { get; }
        public long OrganizationId { get; }

        public UploadLogoDto(long organizationId, FileContent content)
        {
            Content = content;
            OrganizationId = organizationId;
        }
    }
}