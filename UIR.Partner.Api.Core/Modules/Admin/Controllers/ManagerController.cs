using System;
using System.Threading.Tasks;
using AutoMapper;
using Force.Ddd;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Controller;

namespace UIR.Partner.Api.Core.Modules.Admin.Controllers
{
    public class ManagerDto : HasIdBase<long>
    {
        public ManagerType ManagerType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Login { get; set; }
        public int OrgCount { get; set; }
    }

    public class ManagerCreateDto
    {
        public ManagerType ManagerType { get; set; }
        public string Fio { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public class ManagerProfile : Profile
    {
        public ManagerProfile()
        {
            CreateMap<ManagerUser, ManagerDto>()
                .ForMember(d => d.Login, o => o.MapFrom(s => s.UserName))
                .ForMember(d => d.OrgCount, o => o.MapFrom(s => s.Organizations.Count));
            CreateMap<ManagerCreateDto, ManagerUser>()
                .ForMember(d => d.Name, o => o.ResolveUsing(s => s.Fio))
                .ForMember(d => d.UserName, o => o.ResolveUsing(s => s.Login))
                .ForMember(d => d.SecurityStamp, o => o.ResolveUsing(s => Guid.NewGuid().ToString("N")))
                .ForMember(d => d.PhoneNumber, o => o.ResolveUsing(s => s.Phone));
        }
    }

    [Route("api/admin/managers")]
    [Authorize(Roles = "Admin")]
    public class ManagerController : RestController<
        ManagerUser,
        ManagerDto,
        ManagerCreateDto,
        ManagerDto,
        ListSpec<ManagerUser, ManagerDto>,
        ByIdSpec<ManagerUser>>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;

        public ManagerController(
            UserManager<Domain.Entities.UserSetUp.User> userManager,
            IQueryableProvider queryableProvider,
            DbContext dbContext) : base(queryableProvider,
            dbContext)
        {
            _userManager = userManager;
        }

        public override async Task<IActionResult> Add([FromBody] ManagerCreateDto dto)
        {
            var ent = Mapper.Map<ManagerCreateDto, ManagerUser>(dto);
            var result = await _userManager.CreateAsync(ent, dto.Password);
            await DbContext.SaveChangesAsync();
            await _userManager.AddToRoleAsync(ent, RoleType.Admin.ToString());
            await DbContext.SaveChangesAsync();
            return Ok(ent.Id);
        }
    }
}