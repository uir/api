using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using AutoMapper;
using Force.Cqrs;
using Force.Ddd;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenIddict.Mvc.Internal;
using UIR.Auth.Core.Services;
using UIR.Domain.Entities;
using UIR.Domain.Entities.Base.Specs;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Controller;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Admin.Commands;
using UIR.Partner.Api.Core.Modules.Admin.Dto;
using UIR.Partner.Api.Core.Modules.Admin.Dto.Request;
using UIR.Partner.Api.Core.Modules.Admin.Dto.Response.Organizations;
using UIR.Service.Core.FileService.Model;

namespace UIR.Partner.Api.Core.Modules.Admin.Controllers
{
    public class OrganizationProfile : Profile
    {
        public OrganizationProfile()
        {
            CreateMap<CreateOrganizationDto, Organization>();
            CreateMap<CreateOrganizationDto, IndividBusiness>();
            CreateMap<CreateOrganizationDto, Domain.Entities.Company>();
            CreateMap<CreateOrganizationDto, SelfEmployed>();
            CreateMap<Organization, CreateOrganizationDto>();

            CreateMap<Organization, OrganizationDto>()
                .ForMember(d => d.Manager, o => o.MapFrom(s => s.Manager.Name))
                .ForMember(d => d.IsDraft,
                    o => o.MapFrom((Expression<Func<Organization, bool>>) OrganizationSpec.IsDraft));

            CreateMap<Domain.Entities.Place, OrganizationDetailPlaceDto>();

            CreateMap<Organization, OrganizationDetailDto>()
                .ForMember(d => d.IsDraft,
                    o => o.MapFrom((Expression<Func<Organization, bool>>) OrganizationSpec.IsDraft))
                .ForMember(d => d.Places, o => o.MapFrom(s => s.Places))
                .ForMember(d => d.Login, o => o.MapFrom(s => s.Owner.UserName));
        }
    }

    public class OrganizationListSpec : SearchListSpec<Organization, OrganizationDto>
    {
        public override Func<string, ForceSpec<Organization>> SearchSpec => (text) =>
            DomainSpec.FullSearch<Organization>(text) |
            Spec.Of<Organization>(x =>
                x.Fio.ToLower().Contains(text.ToLower()) ||
                x.Manager.UserName.ToLower().Contains(text.ToLower()) || 
                x.Address.ToLower().Contains(text.ToLower())) |
            DomainSpec.FullSearch<Domain.Entities.Place>(text).FromMany<Organization>(o => o.Places) |
            Spec.Of<Organization>(x => x.ManagerId.ToString() == text);

        public override IOrderedQueryable<OrganizationDto> OrderBy(IQueryable<OrganizationDto> queryable)
        {
            var res = queryable.OrderBy(x => x.CreateDate);
            if (!string.IsNullOrEmpty(OrderByStr))
                res = queryable.AutoSort(OrderByStr);
            return res.ThenBy(x => x.Id);
        }
    }

    [Authorize(Roles = "Admin")]
    [Route("api/admin/organizations")]
    public class OrganizationsController : RestController<
        Organization,
        OrganizationDto,
        CreateOrganizationDto,
        OrganizationDetailDto,
        OrganizationListSpec,
        ByIdSpec<Organization>>
    {
        private readonly IHandler<CreateTagsForCompanyDto> _createTagsForCompanyCommand;
        private readonly IHandler<CreateOrganizationDto, long> _createCompanyCommand;
        private readonly IAsyncHandler<UploadLogoDto> _uploadLogo;
        private readonly IAuthorizeService _authorizeService;

        public OrganizationsController(
            DbContext dbContext,
            IHandler<CreateTagsForCompanyDto> createTagsForCompanyCommand,
            IHandler<CreateOrganizationDto, long> createCompanyCommand,
            IAsyncHandler<UploadLogoDto> uploadLogo,
            IQueryableProvider queryableProvider,
            IAuthorizeService authorizeService) : base(queryableProvider, dbContext)
        {
            _createTagsForCompanyCommand = createTagsForCompanyCommand;
            _createCompanyCommand = createCompanyCommand;
            _uploadLogo = uploadLogo;
            _authorizeService = authorizeService;
        }

        [Route("~/api/admin/organizations/login"), HttpPost]
        public async Task<IActionResult> Login([ModelBinder(typeof(OpenIddictMvcBinder))]
            OpenIdConnectRequest data)
        {
            var user = await QueryableProvider
                .Query<PartnerUser>()
                .Where(x => x.Organizations.Select(o => o.Id).Contains(long.Parse(data.Username)))
                .FirstOrDefaultAsync();
            if (user == null)
                return NotFound();

            data.Username = user.UserName;

            var ticket = await _authorizeService.CreateTicketAsync(data, user);
            return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
        }

        [Route("~/api/companies"), HttpPost]
        public override async Task<IActionResult> Add([FromBody] CreateOrganizationDto dto)
        {
            dto.ManagerId = User.GetId();
            var companyId = _createCompanyCommand.Handle(dto);
            _createTagsForCompanyCommand.Handle(new CreateTagsForCompanyDto(companyId));
            if (dto.Logo != null)
            {
                var data = Convert.FromBase64String(dto.Logo.File);
                await _uploadLogo.Handle(new UploadLogoDto(companyId, new FileContent(data, dto.Logo.Name)));
            }

            return Ok(companyId);
        }
    }
}