using System;
using AutoMapper;
using Force.Ddd;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Controller;

namespace UIR.Partner.Api.Core.Modules.Admin.Controllers
{
    public class OverviewProfile : Profile
    {
        public OverviewProfile()
        {
            CreateMap<Overview, OverviewDto>();
            CreateMap<Overview, OverviewDetailDto>();
            CreateMap<OverviewDetailDto, Overview>();
        }
    }

    public class OverviewDto : HasIdBase<long>
    {
        public DateTime CreateDate { get; set; }
        public bool IsConfirmed { get; set; }
        public string PlaceName { get; set; }
    }
    
    public class OverviewDetailDto : HasIdBase<long>
    {
        public DateTime CreateDate { get; set; }
        public bool IsConfirmed { get; set; }
        public string PlaceName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Plus { get; set; }
        public string Minus { get; set; }
        public long PlaceId { get; set; }
    }

    [Authorize(Roles = "Admin")]
    [Route("api/admin/overviews")]
    public class OverviewsController :
        RestController<Overview, OverviewDto, Overview, OverviewDetailDto, ListSpec<Overview, OverviewDto>, ByIdSpec<Overview>>
    {
        public OverviewsController(IQueryableProvider queryableProvider, DbContext dbContext)
            : base(queryableProvider, dbContext)
        {
        }
    }
}