using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UIR.Partner.Api.Core.Modules.Admin.Commands;

namespace UIR.Partner.Api.Core.Modules.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly CreateFakeUserCommand _fakeUserCommand;

        public AdminController(CreateFakeUserCommand fakeUserCommand)
        {
            _fakeUserCommand = fakeUserCommand;
        }

        [HttpPost]
        [Route("~/api/admin/user/fake")]
        public async Task<IActionResult> CreateFakeUser()
        {
            var user = await _fakeUserCommand.Handle();
            return Ok(user);
        }

        [HttpPost]
        [Route("~/api/admin/user/info")]
        public async Task<IActionResult> UserInfo()
        {
            var user = await _fakeUserCommand.Handle();
            return Ok(user);
        }
    }
}