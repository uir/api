using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Domain.Enums;
using UIR.Partner.Api.Core.Modules.Admin.Dto.Response;

namespace UIR.Partner.Api.Core.Modules.Admin.Commands
{
    public class CreateFakeUserCommand
    {
        private readonly IUserPasswordStore<Domain.Entities.UserSetUp.User> _passwordStore;
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly DbContext _dbContext;

        public CreateFakeUserCommand(IUserStore<Domain.Entities.UserSetUp.User> passwordStore,
            UserManager<Domain.Entities.UserSetUp.User> userManager,
            DbContext dbContext)
        {
            _passwordStore = passwordStore as IUserPasswordStore<Domain.Entities.UserSetUp.User>;
            _userManager = userManager;
            _dbContext = dbContext;
        }

        public async Task<FakeUser> Handle()
        {
            var user = _dbContext.Set<PartnerUser>().FirstOrDefault(PartnerUserSpec.Fake);
            var password = Guid.NewGuid().ToString("N").Substring(0, 6);
            if (user == null)
            {
                var name = Guid.NewGuid().ToString("N");
                user = new PartnerUser(name);
                await _userManager.CreateAsync(user, password);
            }
            else
            {
                await _passwordStore.SetPasswordHashAsync(user,
                    _userManager.PasswordHasher.HashPassword(user, password), CancellationToken.None);
                await _dbContext.SaveChangesAsync();
            }

            await _userManager.AddToRoleAsync(user, RoleType.Partner.ToString());
            await _dbContext.SaveChangesAsync();

            return new FakeUser
            {
                Id = user.Id,
                Login = user.UserName,
                Password = password
            };
        }
    }
}