﻿using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;

namespace UIR.Partner.Api.Core.Modules.Admin.Commands
{
    public class CreateTagsForCompanyDto
    {
        public CreateTagsForCompanyDto(long companyId)
        {
            CompanyId = companyId;
        }

        public long CompanyId { get; set; }
    }

    public class CreateTagsForCompanyCommand : IHandler<CreateTagsForCompanyDto>
    {
        private readonly DbContext _dbContext;
        private readonly IHandler<BindTagDto> _bindTag;

        public CreateTagsForCompanyCommand(DbContext dbContext, IHandler<BindTagDto> bindTag)
        {
            _dbContext = dbContext;
            _bindTag = bindTag;
        }

        public void Handle(CreateTagsForCompanyDto input)
        {
            var categories = _dbContext
                .Set<CategoryOrganization>()
                .Where(x => x.OrganizationId == input.CompanyId)
                .Include(x => x.Organization)
                .Include(x => x.Category)
                .ThenInclude(x => x.Templates)
                .ThenInclude(x => x.Template)
                .Select(x => new
                {
                    Org = x.Organization.Name,
                    CategoryName = x.Category.Name,
                    Temps = x.Category.Templates.Select(y => y.Template.Name)
                })
                .ToArray();

            var tagsNames = categories.Select(x => x.Org).Distinct()
                .Union(categories.Select(x => x.CategoryName).Distinct())
                .Union(categories.SelectMany(x => x.Temps).Distinct());

            tagsNames.Each(x => _bindTag.Handle(new BindTagDto
            {
                CompanyId = input.CompanyId,
                Value = x
            }));
        }
    }
}