﻿using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using UIR.Domain.Entities;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Admin.Dto.Request;
using UIR.Partner.Api.Core.Modules.Company.Commands;

namespace UIR.Partner.Api.Core.Modules.Admin.Commands
{
    public class CreateCompanyCommand : IHandler<CreateOrganizationDto, long>
    {
        private readonly DbContext _dbContext;
        private readonly SetOrganizationCategories _setOrganizationCategories;

        public CreateCompanyCommand(DbContext dbContext, SetOrganizationCategories setOrganizationCategories)
        {
            _dbContext = dbContext;
            _setOrganizationCategories = setOrganizationCategories;
        }

        public long Handle(CreateOrganizationDto dto)
        {
            switch (dto.ActiveType)
            {
                case ActiveType.Company:
                    return Create<Domain.Entities.Company>(dto);
                case ActiveType.IndividBusiness:
                    return Create<IndividBusiness>(dto);
                case ActiveType.SelfEmployed:
                    return Create<SelfEmployed>(dto);
                default:
                    throw new OperationException("Error from create");
            }
        }

        private long Create<T>(CreateOrganizationDto dto)
            where T : Organization
        {
            var company = dto.Map<T>();
            company = _dbContext.Set<T>().Add(company).Entity;
            _setOrganizationCategories.Handle(company.Id, dto.CategoriesIds);
            return company.Id;
        }
    }
}