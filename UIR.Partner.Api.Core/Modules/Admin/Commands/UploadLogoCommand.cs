﻿using System.Linq;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Partner.Api.Core.Modules.Admin.Dto;
using UIR.Service.Core.FileService;

namespace UIR.Partner.Api.Core.Modules.Admin.Commands
{
    public class UploadLogoCommand : IAsyncHandler<UploadLogoDto>
    {
        private readonly DbContext _dbContext;
        private readonly ImageFileService _fileService;

        public UploadLogoCommand(DbContext dbContext, ImageFileService fileService)
        {
            _dbContext = dbContext;
            _fileService = fileService;
        }

        public async Task Handle(UploadLogoDto dto)
        {
            var oldLogo = _dbContext
                .Set<Image>()
                .FirstOrDefault(x => x.OrganizationId == dto.OrganizationId && x.IsLogo);
            if (oldLogo != null)
            {
                await _fileService.Remove(oldLogo.File);
                _dbContext.Set<Image>().Remove(oldLogo);
            }

            var files = await _fileService.SaveAsync(new[] {dto.Content});
            foreach (var file in files)
            {
                _dbContext
                    .Set<Image>()
                    .Add(new Image
                    {
                        File = file.Name,
                        OrganizationId = dto.OrganizationId,
                        IsLogo = true,
                        Height = file.Heigth,
                        Width = file.Width
                    });
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}