using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Partner.Api.Core.Modules.Admin.Commands;
using UIR.Partner.Api.Core.Modules.Admin.Dto;
using UIR.Partner.Api.Core.Modules.Admin.Dto.Request;

namespace UIR.Partner.Api.Core.Modules.Admin
{
    public class AdminModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IHandler<CreateOrganizationDto, long>, CreateCompanyCommand>();
            services.AddScoped<IAsyncHandler<UploadLogoDto>, UploadLogoCommand>();
            services.AddScoped<IHandler<CreateTagsForCompanyDto>, CreateTagsForCompanyCommand>();
            
            services.AddScoped<CreateFakeUserCommand>();
        }
    }
}