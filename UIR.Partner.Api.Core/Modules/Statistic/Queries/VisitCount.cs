using System;
using System.Linq;
using Force.Cqrs;

namespace UIR.Partner.Api.Core.Modules.Statistic.Queries
{
    public class VisitCountResponseDto
    {
        public int Monthly { get; set; }
        public int Weekly { get; set; }
        public int Daily { get; set; }
    }

    public class VisitCountQuery : IQuery<long, VisitCountResponseDto>
    {
        private readonly VisitChartQuery _chartQuery;

        public VisitCountQuery(VisitChartQuery chartQuery)
        {
            _chartQuery = chartQuery;
        }


        public VisitCountResponseDto Ask(long placeId)
        {
            var monthly = _chartQuery
                .Ask(new VisitChartRequestDto
                {
                    PlaceId = placeId,
                    StartDate = DateTime.Now.AddMonths(-1),
                    EndDate = DateTime.Now
                })
                .ToArray();

            return new VisitCountResponseDto()
            {
                Monthly = monthly.Length,
                Daily = monthly.Count(x => x.DateTime > DateTime.Now.AddDays(-1) && x.DateTime < DateTime.Now),
                Weekly = monthly.Count(x => x.DateTime > DateTime.Now.AddDays(-7) && x.DateTime < DateTime.Now)
            };
        }
    }
}