using System;
using System.Collections.Generic;
using System.Data;
using ClickHouse.Ado;
using UIR.DAL.ClickHouse;

namespace UIR.Partner.Api.Core.Modules.Statistic.Queries
{
    public class VisitChartRequestDto
    {
        public long PlaceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    
    public class VisitChartResponseDto
    {
        public DateTime DateTime { get; set; }
    }

    public class VisitChartQuery : StatisticQuery<VisitChartResponseDto, VisitChartRequestDto, VisitChartResponseDto>
    {
        public VisitChartQuery(ClickHouseDb db) : base(db)
        {
        }

        public override string Sql => @"
        select DateTime
        from Places
        where PlaceId = @placeId
        and CategoryId <> 0
        and DateTime between @startDate and @endDate;";

        protected override ClickHouseParameterCollection GetParams(VisitChartRequestDto spec)
        {
            var param = new ClickHouseParameterCollection();
            param.Add("startDate", DbType.DateTime, spec.StartDate);
            param.Add("endDate", DbType.DateTime, spec.EndDate);
            param.Add("placeId", DbType.Int32, spec.PlaceId);
            return param;
        }
    }
}