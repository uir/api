using System.Collections.Generic;
using System.Linq;
using ClickHouse.Ado;
using Force.Cqrs;
using UIR.DAL.ClickHouse;

namespace UIR.Partner.Api.Core.Modules.Statistic.Queries
{
    public abstract class StatisticQuery<TRes, TIn, TQuery> : IQuery<TIn, IEnumerable<TRes>>
        where TRes : new() where TQuery : new()
    {
        private readonly ClickHouseDb _db;

        public abstract string Sql { get; }

        public StatisticQuery(ClickHouseDb db)
        {
            _db = db;
        }

        public IEnumerable<TRes> Ask(TIn spec)
        {
            var p = GetParams(spec);
            var data = _db.Query<TQuery>(Sql, p);
            return BeforeQuery(data);
        }

        protected abstract ClickHouseParameterCollection GetParams(TIn spec = default);

        protected virtual IEnumerable<TRes> BeforeQuery(IEnumerable<TQuery> data) => typeof(TRes) == typeof(TQuery)
            ? data.Cast<TRes>()
            : new TRes[] { };
    }
}