using Microsoft.AspNetCore.Mvc;
using UIR.Partner.Api.Core.Modules.Statistic.Queries;

namespace UIR.Partner.Api.Core.Modules.Statistic
{
    public class StatisticController : Controller
    {
        private readonly VisitCountQuery _visitCountQuery;
        private readonly VisitChartQuery _visitChartQuery;

        public StatisticController(VisitCountQuery visitCountQuery, VisitChartQuery visitChartQuery)
        {
            _visitCountQuery = visitCountQuery;
            _visitChartQuery = visitChartQuery;
        }

        [Route("~/api/statistic/count"), HttpGet]
        public IActionResult VisitCount([FromQuery] long placeId) => Ok(_visitCountQuery.Ask(placeId));

        [Route("~/api/statistic/chart"), HttpGet]
        public IActionResult VisitChart([FromQuery] VisitChartRequestDto dto) => Ok(_visitChartQuery.Ask(dto));
    }
}