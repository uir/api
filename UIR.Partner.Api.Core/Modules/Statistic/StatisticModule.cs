using Microsoft.Extensions.DependencyInjection;
using UIR.DAL.ClickHouse;
using UIR.Partner.Api.Core.Modules.Statistic.Queries;

namespace UIR.Partner.Api.Core.Modules.Statistic
{
    public class StatisticModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddUirClickHouse();
            services.AddScoped<VisitCountQuery>();
            services.AddScoped<VisitChartQuery>();
        }
    }
}