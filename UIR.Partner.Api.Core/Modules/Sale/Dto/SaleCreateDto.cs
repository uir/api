using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Sale.Dto
{
    public class SaleCreateDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
    }
}