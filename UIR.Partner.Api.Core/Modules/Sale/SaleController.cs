using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Extensions.Core.Controller;
using UIR.Partner.Api.Core.Modules.Sale.Dto;

namespace UIR.Partner.Api.Core.Modules.Sale
{
    [Route("api/sale")]
    public class SaleController: CrudController
    {
        public SaleController(DbContext dbContext) : base(dbContext)
        {
        }
        
        /// <summary>
        /// Создание скидки
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Partner"), Route(""), HttpPost]
        public IActionResult CreatePartner([FromBody] SaleCreateDto dto) => Add<Domain.Entities.Sale, SaleCreateDto>(dto);

        /// <summary>
        /// Удаление скидки
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin,Partner"), Route("{id}"), HttpDelete]
        public IActionResult DeletePartner(long id) => Delete<Domain.Entities.Sale>(id);
        
        /// <summary>
        /// Возвращает скидку
        /// </summary>
        /// <returns></returns>
        [Route("{id}"), HttpGet]
        public IActionResult Get(long id) => Get<Domain.Entities.Sale, SaleDto>(id);
    }
}