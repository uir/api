using AutoMapper;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Place.Dto.Request;
using UIR.Partner.Api.Core.Modules.Sale.Dto;

namespace UIR.Partner.Api.Core.Modules.Sale
{
    public class SaleProfile: Profile
    {
        public SaleProfile()
        {
            CreateMap<Domain.Entities.Sale, SaleCreateDto>();
            CreateMap<Domain.Entities.Sale, CreateCompanySaleDto>();
            CreateMap<CreateCompanySaleDto, Domain.Entities.Sale>();
        }
    }
}