﻿using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Partner.Api.Core.Modules.Place.Dto.Request;
using UIR.Partner.Api.Core.Modules.Place.Filters;

namespace UIR.Partner.Api.Core.Modules.Place.Queries
{
    public class PartnerUsersSearchQuery : IQuery<UserSearchDto, PlaceUserDto[]>
    {
        private readonly DbContext _dbContext;

        public PartnerUsersSearchQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public PlaceUserDto[] Ask(UserSearchDto spec)
        {
            var salesIds = _dbContext
                .Where<SalePlace>(x => x.PlaceId == spec.PlaceId)
                .Select(x => x.SaleId)
                .ToArray();

            var users = _dbContext
                .Where(new PlaceUsersSearchSpec(spec.Search, salesIds))
                .ToArray()
                .Map<PlaceUserDto>()
                .ToArray();

            return users;
        }
    }
}