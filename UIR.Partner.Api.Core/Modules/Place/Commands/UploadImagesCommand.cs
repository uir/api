﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Service.Core.FileService;
using UIR.Service.Core.Payment.CountCheckers;

namespace UIR.Partner.Api.Core.Modules.Place.Commands
{
    public class UploadImagesCommand
    {
        private readonly DbContext _dbContext;
        private readonly ImageFileService _fileService;
        private readonly ImagesPaymentPlanPartnerCountChecker _checker;

        public UploadImagesCommand(DbContext dbContext, ImageFileService fileService,
            ImagesPaymentPlanPartnerCountChecker checker)
        {
            _dbContext = dbContext;
            _fileService = fileService;
            _checker = checker;
        }

        public async Task<Image[]> Handle(UploadImagesDto dto)
        {
            var errors = _checker.CheckCount(dto.UserId, dto.Contents.Count(), dto.PlaceId);
            if (errors.Length > 0)
                throw new OperationException(errors.Aggregate((s, s1) => s + "\n" + s1));

            var files = await _fileService.SaveAsync(dto.Contents);
            var images = new List<Image>();
            foreach (var file in files)
            {
                var image = new Image
                {
                    File = file.Name,
                    Height = file.Heigth,
                    Width = file.Width
                };
                images.Add(image);
                _dbContext.Add(image);
                _dbContext.Add(new ImagePlace
                {
                    Image = image,
                    PlaceId = dto.PlaceId
                });
            }

            await _dbContext.SaveChangesAsync();
            return images.ToArray();
        }
    }
}