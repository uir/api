﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Service.Core.FileService;
using UIR.Service.Core.FileService.Model;
using UIR.Service.Core.Payment.CountCheckers;

namespace UIR.Partner.Api.Core.Modules.Place.Commands
{
    public class UploadImagesMultipleCommand : IAsyncHandler<UploadImagesMultipleDto>
    {
        private readonly DbContext _dbContext;
        private readonly ImageFromUrlFileService _fileService;
        private readonly ImagesPaymentPlanPartnerCountChecker _checker;

        public UploadImagesMultipleCommand(DbContext dbContext, ImageFromUrlFileService fileService,
            ImagesPaymentPlanPartnerCountChecker checker)
        {
            _dbContext = dbContext;
            _fileService = fileService;
            _checker = checker;
        }

        public async Task Handle(UploadImagesMultipleDto input)
        {
            var errors = _checker.CheckCount(input.UserId, input.Files.Length, input.PlaceId);
            if (errors.Length > 0)
                throw new OperationException(errors.Aggregate((s, s1) => s + "\n" + s1));

            Uri[] urls;
            try
            {
                urls = input.Files
                    .Select(x => new Uri(x))
                    .ToArray();
            }
            catch
            {
                throw new OperationException("Ошибка чтения ссылок");
            }

            if (urls.Length == 0)
                throw new OperationException("Не найденно ссылок");


            ImageFileModel[] files;
            try
            {
                files = await _fileService.MultipleUploadAsync(urls);
            }
            catch
            {
                throw new OperationException("Ошибка сохраения файлов");
            }

            foreach (var file in files)
            {
                var image = new Image
                {
                    File = file.Name,
                    Height = file.Heigth,
                    Width = file.Width
                };
                _dbContext.Add(image);
                _dbContext.Add(new ImagePlace
                {
                    Image = image,
                    PlaceId = input.PlaceId
                });
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}