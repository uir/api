﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Service.Core.FileService;
using UIR.Service.Core.Payment.CountCheckers;

namespace UIR.Partner.Api.Core.Modules.Place.Commands
{
    public class UploadVideosCommand :IAsyncHandler<UploadVideosDto>
    {
        private readonly DbContext _dbContext;
        private readonly VideoFileService _fileService;
        private readonly VideosPaymentPlanPartnerCountChecker _checker;

        public UploadVideosCommand(DbContext dbContext, VideoFileService fileService,
            VideosPaymentPlanPartnerCountChecker checker)
        {
            _dbContext = dbContext;
            _fileService = fileService;
            _checker = checker;
        }

        public async Task Handle(UploadVideosDto dto)
        {
            var errors = _checker.CheckCount(dto.UserId, dto.Contents.Count(), dto.PlaceId);
            if (errors.Length > 0)
                throw new OperationException(errors.Aggregate((s, s1) => s + "\n" + s1));

            var files = await _fileService.SaveAsync(dto.Contents);
            foreach (var file in files)
            {
                var video = new Video
                {
                    File = file.Name,
                    Name = DateTime.Now.ToString("G"),
                };
                _dbContext.Add(video);
                _dbContext.Add(new VideoPlace
                {
                    Video = video,
                    PlaceId = dto.PlaceId
                });
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}