﻿using System.Linq;
using Force.Ddd;
using UIR.Domain.Entities;

namespace UIR.Partner.Api.Core.Modules.Place.Filters
{
    public class DeleteImagesSpec: IQueryableFilter<Image>
    {
        public long[] Ids { get; set; }

        public DeleteImagesSpec(long[] ids)
        {
            Ids = ids;
        }

        public IQueryable<Image> Apply(IQueryable<Image> query) => query
            .Where(x => Ids.Contains(x.Id));
    }
}