﻿using System.Linq;
using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Place.Filters
{
    public class DeleteVideosSpec: IQueryableFilter<Domain.Entities.Video>
    {
        public long[] Ids { get; set; }

        public DeleteVideosSpec(long[] ids)
        {
            Ids = ids;
        }

        public IQueryable<Domain.Entities.Video> Apply(IQueryable<Domain.Entities.Video> query) => query
            .Where(x => Ids.Contains(x.Id));
    }
}