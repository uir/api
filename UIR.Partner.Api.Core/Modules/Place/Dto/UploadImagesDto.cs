﻿using System.Collections.Generic;
using System.Net.Http;
using UIR.Service.Core.FileService.Model;

namespace UIR.Partner.Api.Core.Modules.Place.Dto
{
    public class UploadImagesDto
    {
        public long PlaceId { get; set; }
        public IEnumerable<FileContent> Contents { get; set; }
        public long UserId { get; set; }

        public UploadImagesDto(long placeId, long userId, IEnumerable<FileContent> contents)
        {
            PlaceId = placeId;
            Contents = contents;
            UserId = userId;
        }
    }
}