﻿using System;
using System.ComponentModel.DataAnnotations;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Place.Dto.Request
{
    public class CreateFilialSaleDto
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Description { get; set; }
        
        public double Amount { get; set; }
        
        public bool IsActive { get; set; }
        
        [Required]
        public DiscountType DiscountType { get; set; }
        
        public DateTimeOffset? DiscountEndDate { get; set; }
        
        public bool IsExclusive { get; set; }
    }
}