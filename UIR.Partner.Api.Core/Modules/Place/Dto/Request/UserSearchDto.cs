﻿namespace UIR.Partner.Api.Core.Modules.Place.Dto.Request
{
    public class UserSearchDto
    {
        public UserSearchDto(string search, long placeId)
        {
            Search = search;
            PlaceId = placeId;
        }

        public string Search { get; }
        public long PlaceId { get; }
    }
}