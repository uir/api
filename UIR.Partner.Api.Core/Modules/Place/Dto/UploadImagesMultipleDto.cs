﻿namespace UIR.Partner.Api.Core.Modules.Place.Dto
{
    public class UploadImagesMultipleDto
    {
        public long PlaceId { get; }
        public string[] Files { get; }
        public long UserId { get; }

        public UploadImagesMultipleDto(long placeId, string[] files, long userId)
        {
            PlaceId = placeId;
            Files = files;
            UserId = userId;
        }
    }
}