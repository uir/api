﻿using System.Collections.Generic;
using System.Net.Http;
using UIR.Service.Core.FileService.Model;

namespace UIR.Partner.Api.Core.Modules.Place.Dto
{
    public class UploadVideosDto
    {
        public long PlaceId { get; }
        public IEnumerable<FileContent> Contents { get; }
        public long UserId { get; }

        public UploadVideosDto(long placeId, long userId, IEnumerable<FileContent> contents)
        {
            Contents = contents;
            UserId = userId;
            PlaceId = placeId;
        }
    }
}