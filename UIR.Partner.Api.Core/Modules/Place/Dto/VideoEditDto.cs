﻿using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Place.Dto
{
    public class VideoEditDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Discription { get; set; }
    }
}
