﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Auth.Core.Filters;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Service.Core.FileService;
using UIR.Service.Core.FileService.Model;

namespace UIR.Partner.Api.Core.Modules.Place.Controllers
{
    [AuthorizePermission(typeof(PlacePermissionSpec), typeof(Domain.Entities.Place))]
    public class PlaceVideoController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly VideoFileService _fileService;
        private readonly IAsyncHandler<UploadVideosDto> _uploadVideosCommand;

        public PlaceVideoController(
            DbContext dbContext,
            VideoFileService fileService,
            IAsyncHandler<UploadVideosDto> uploadVideosCommand)
        {
            _dbContext = dbContext;
            _fileService = fileService;
            _uploadVideosCommand = uploadVideosCommand;
        }

        /// <summary>
        /// Сохраняет видео
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/videos/upload"), HttpPost, Authorize(Roles = "Partner")]
        public async Task<IActionResult> VideosUpload(long id)
        {
            var files = Request.Form?.Files;
            if (files == null || !files.Any())
                return Ok();
            var fileContents = new List<FileContent>();
            using (var ms = new MemoryStream())
            {
                foreach (var file in files)
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    await file.CopyToAsync(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileContents.Add(new FileContent(ms.ToArray(), file.FileName));
                }
            }

            await _uploadVideosCommand.Handle(new UploadVideosDto(id, User.GetId(), fileContents));
            return Ok();
        }

        /// <summary>
        /// Удаляет видео
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/videos/{videoId}"), HttpDelete, Authorize(Roles = "Partner")]
        public async Task<IActionResult> DeleteVideos(long id, long videoId)
        {
            var video = _dbContext.GetById<Video>(videoId);
            _dbContext.DeleteById<Video>(videoId);
            await _fileService.Remove(video.File);
            return Ok();
        }
        
        [Route("~/api/places/{id}/videos/{videoId}"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult VideoEdit(long id, long videoId, [FromBody] VideoEditDto videoDto)
        {
            _dbContext.Edit<Video, VideoEditDto>(videoDto, videoId);
            return Ok();
        }
    }
}