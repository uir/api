﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Auth.Core.Filters;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Place.Commands;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Service.Core.FileService;
using UIR.Service.Core.FileService.Model;

namespace UIR.Partner.Api.Core.Modules.Place.Controllers
{
    [AuthorizePermission(typeof(PlacePermissionSpec), typeof(Domain.Entities.Place))]
    public class PlaceImageController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly ImageFileService _imageFileService;
        private readonly IAsyncHandler<UploadImagesMultipleDto> _uploadImagesMultipleCommand;
        private readonly UploadImagesCommand _uploadImages;
        private readonly IOptions<AppSettings.AppSettings> _settings;

        public PlaceImageController(
            DbContext dbContext,
            ImageFileService imageFileService,
            IAsyncHandler<UploadImagesMultipleDto> uploadImagesMultipleCommand,
            UploadImagesCommand uploadImages,
            IOptions<AppSettings.AppSettings> settings)
        {
            _dbContext = dbContext;
            _imageFileService = imageFileService;
            _uploadImagesMultipleCommand = uploadImagesMultipleCommand;
            _uploadImages = uploadImages;
            _settings = settings;
        }

        /// <summary>
        /// Сохраняет изображения партнера
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/images/upload"), HttpPost, Authorize(Roles = "Partner")]
        public async Task<IActionResult> ImagesUpload(long id)
        {
            var files = Request.Form?.Files;
            if (files == null || !files.Any())
                return Ok();
            var fileContents = new List<FileContent>();
            using (var ms = new MemoryStream())
            {
                foreach (var file in files)
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    await file.CopyToAsync(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileContents.Add(new FileContent(ms.ToArray(), file.FileName));
                }
            }

            var images = await _uploadImages.Handle(new UploadImagesDto(id, User.GetId(), fileContents));
            return Ok(images.Select(x => new
            {
                x.Id,
                File = x.File.ToPathImg()
            }));
        }

        /// <summary>
        /// Сохраняет изображения партнера
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/images/multiple_upload"), HttpPost, Authorize(Roles = "Partner")]
        public async Task<IActionResult> ImagesUpload(long id, [FromBody] string[] images)
        {
            await _uploadImagesMultipleCommand.Handle(new UploadImagesMultipleDto(id, images, User.GetId()));
            return Ok();
        }

        /// <summary>
        /// Удаляет изображения партнера
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/images/{imageId}"), HttpDelete, Authorize(Roles = "Partner")]
        public async Task<IActionResult> DeleteImage(long id, long imageId)
        {
            var img = _dbContext.GetById<Image>(imageId);
            _dbContext.DeleteById<Image>(imageId);
            await _imageFileService.Remove(img.File);
            return Ok();
        }
    }
}