﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Auth.Core.Filters;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Partner.Api.Core.Modules.Place.Dto.Request;
using UIR.Partner.Api.Core.Modules.Place.Filters;

namespace UIR.Partner.Api.Core.Modules.Place.Controllers
{
//    [AuthorizePermission(typeof(PlacePermissionSpec), typeof(Domain.Entities.Place))]
    public class PlaceSalesController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IQuery<UserSearchDto, PlaceUserDto[]> _partnerUsersSearchQuery;

        public PlaceSalesController(
            DbContext dbContext,
            IQuery<UserSearchDto, PlaceUserDto[]> partnerUsersSearchQuery)
        {
            _dbContext = dbContext;
            _partnerUsersSearchQuery = partnerUsersSearchQuery;
        }

        /// <summary>
        /// Возвращает скидки филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult GetSales(long id)
        {
            var sales = _dbContext
                .Set<Domain.Entities.Sale>()
                .Where(SaleSpec.ByPlaceId(id))
                .ProjectTo<SaleDto>()
                .ToArray();

            return Ok(sales);
        }

        /// <summary>
        /// Возвращает пользователей
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales/users")]
        [HttpGet]
        [Authorize(Roles = "Partner")]
        public IActionResult GetUsers(long id, string search) => _partnerUsersSearchQuery
            .Ask(new UserSearchDto(search, id))
            .PipeTo(Ok);

        /// <summary>
        /// Принимает скидку у пользователя
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales/{saleId}/accept"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult AcceptSale(long id, long saleId, [FromBody] SaleAcceptDto data)
        {
            if (data?.UserId == null || data.UserId == 0)
                return BadRequest(new OperationError
                {
                    Message = "UserId faild"
                });
            _dbContext
                .Where<UserSale>(x => x.Sale.Id == saleId && x.UserId == data.UserId)
                .ToArray()
                .Each(x => { x.IsActive = false; });
            _dbContext.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Создает скидку
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult CreateSale(long id, [FromBody] CreateFilialSaleDto dto)
        {
            var sale = Mapper.Map<Domain.Entities.Sale>(dto);
            _dbContext.Add(sale);
            _dbContext.Add(new SalePlace
            {
                PlaceId = id,
                Sale = sale
            });
            _dbContext.SaveChanges();
            return Ok(sale.Id);
        }

        /// <summary>
        /// Удаляет скидку
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales/{saleId}"), HttpDelete, Authorize(Roles = "Partner")]
        public IActionResult DeleteSale(long id, long saleId)
        {
            _dbContext.DeleteById<Domain.Entities.Sale>(saleId);
            return Ok();
        }

        /// <summary>
        /// Редактирует скидку
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales/{saleId}"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult EditSale(long id, long saleId, [FromBody] EditSaleDto dto)
        {
            var oldSale = _dbContext.Set<Domain.Entities.Sale>().ById(saleId);
            if (oldSale == null)
                throw new OperationException("Данной скидки не существует.");
            Mapper.Map(dto, oldSale);
            _dbContext.SaveChanges();
            return Ok();
        }
    }
}