﻿using System.Linq;
using AutoMapper;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Partner.Api.Core.Modules.Place.Dto.Request;

namespace UIR.Partner.Api.Core.Modules.Place
{
    public class PlaceProfile : Profile
    {
        public PlaceProfile()
        {
            CreateMap<Image, ImageDto>();

            CreateMap<Image, ImageDto>()
                .ForMember(dest => dest.File, opts => opts.MapFrom(src => @"img/" + src.File));
            

            CreateMap<Domain.Entities.Sale, SaleDto>();

            CreateMap<Period, WorkPeriodDto>();

            CreateMap<Video, VideoDto>();

            CreateMap<EditPlaceDto, Domain.Entities.Place>();

            CreateMap<Domain.Entities.UserSetUp.User, PlaceUserDto>()
                .ForMember(dest => dest.Sales,
                    opts => opts.MapFrom(src =>
                        src.Sales.Where(sale => sale.IsActive).Select(sale => sale.Sale.Map<SaleDto>())))
                .ForMember(dest => dest.Phone, opts => opts.MapFrom(src => src.PhoneNumber));
            
            CreateMap<VideoEditDto, Domain.Entities.Video>()
                .ForMember(dest => dest.File, opts => opts.Ignore());
            
            CreateMap<EditSaleDto, Domain.Entities.Sale>();
            
            CreateMap<CreateFilialSaleDto, Domain.Entities.Sale>();
        }
    }
}