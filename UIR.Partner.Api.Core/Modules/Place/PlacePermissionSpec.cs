﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using UIR.Auth.Core.Permission;

namespace UIR.Partner.Api.Core.Modules.Place
{
    public class PlacePermissionSpec : PermissionSpecBase<Domain.Entities.Place>
    {
        public override IQueryable<Domain.Entities.Place> Apply(IQueryable<Domain.Entities.Place> query) => query
            .Where(x => x.Id == EntityId && x.Organization.OwnerId == UserId);

        public PlacePermissionSpec(DbContext dbContext, long userId, long entityId) : base(dbContext, userId, entityId)
        {
        }
    }
}