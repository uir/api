﻿using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Partner.Api.Core.Modules.Place.Commands;
using UIR.Partner.Api.Core.Modules.Place.Dto;
using UIR.Partner.Api.Core.Modules.Place.Dto.Request;
using UIR.Partner.Api.Core.Modules.Place.Filters;
using UIR.Partner.Api.Core.Modules.Place.Queries;

namespace UIR.Partner.Api.Core.Modules.Place
{
    public class PlaceModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<UploadImagesCommand>();
            services.AddScoped<IAsyncHandler<UploadImagesMultipleDto>, UploadImagesMultipleCommand>();
            services.AddScoped<IAsyncHandler<UploadVideosDto>, UploadVideosCommand>();
            services.AddScoped<IQuery<UserSearchDto, PlaceUserDto[]>, PartnerUsersSearchQuery>();
        }
    }
}