﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Service.Core.TokenService;

namespace UIR.Partner.Api.Core.Modules.User.Commands.ChangePassword
{
    public class ChangePasswordConfirmDto
    {
        [Required]
        public string Password { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Code { get; set; }
    }

    internal class ChangePasswordConfirmCommand : IAsyncHandler<ChangePasswordConfirmDto>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly DbContext _dbContext;
        private readonly EmailTokenService _emailTokenService;

        public ChangePasswordConfirmCommand(UserManager<Domain.Entities.UserSetUp.User> userManager,
            DbContext dbContext, EmailTokenService emailTokenService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _emailTokenService = emailTokenService;
        }

        public async Task Handle(ChangePasswordConfirmDto input)
        {
            var user = await _userManager.FindByNameAsync(input.Email);
            await _emailTokenService.ValidateToken(user, input.Code);
            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, input.Password);
            await _dbContext.SaveChangesAsync();
        }
    }
}