﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Service.Core.TokenService;

namespace UIR.Partner.Api.Core.Modules.User.Commands.ChangePassword
{
    public class ChangePasswordDto
    {
        [Required]
        public string Email { get; set; }
    }

    internal class ChangePasswordRequestCommand : IAsyncHandler<ChangePasswordDto>
    {
        private readonly DbContext _dbContext;
        private readonly EmailTokenService _tokenService;

        public ChangePasswordRequestCommand(DbContext dbContext, EmailTokenService tokenService)
        {
            _dbContext = dbContext;
            _tokenService = tokenService;
        }

        public async Task Handle(ChangePasswordDto input)
        {
            var user = _dbContext.FirstOrDefault<Domain.Entities.UserSetUp.User>(x => x.UserName == input.Email);
            if (user == null)
                throw new OperationException("Пользователь не найден");

            await _tokenService.SendChangePasswordToken(user);
        }
    }
}