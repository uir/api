using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.User.Dto;
using UIR.Service.Core.TokenService;

namespace UIR.Partner.Api.Core.Modules.User.Commands.PartnerUserReg
{
    public class PartnerRegisterByReferralCommand
    {
        private readonly DbContext _dbContext;
        private readonly IUserPasswordStore<Domain.Entities.UserSetUp.User> _passwordStore;
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly EmailTokenService _tokenService;

        public PartnerRegisterByReferralCommand(DbContext dbContext, IUserStore<Domain.Entities.UserSetUp.User> passwordStore,
            UserManager<Domain.Entities.UserSetUp.User> userManager, EmailTokenService tokenService)
        {
            _dbContext = dbContext;
            _passwordStore = passwordStore as IUserPasswordStore<Domain.Entities.UserSetUp.User>;
            _userManager = userManager;
            _tokenService = tokenService;
        }

        public async Task Handle(string referral, PartnerRegDto data)
        {
            var user = _dbContext.FirstOrDefault<PartnerUser>(x => x.UserName == referral);
            if (user == null)
                throw new OperationException("Пользователь не найден", HttpStatusCode.NotFound);

            if (user.EmailConfirmed)
                throw new OperationException("Пользователь уже зарегестрирован.");

            if (_dbContext.Set<PartnerUser>().Any(x => x.UserName == data.Email))
                throw new OperationException("Email занят другим пользователем");

            Mapper.Map(data, user);

            try
            {
                await _tokenService.SendToken(user);
            }
            catch (SmtpFailedRecipientException e)
            {
                throw new OperationException(e.Message);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}