using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Force.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Partner.Api.Core.Modules.User.Dto.Request;
using UIR.Service.Core.TokenService;

namespace UIR.Partner.Api.Core.Modules.User.Commands.PartnerUserReg
{
    public class PartnerRegisterByReferralConfirmCommand
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly EmailTokenService _tokenService;
        private readonly DbContext _dbContext;
        private IUserPasswordStore<Domain.Entities.UserSetUp.User> _passwordStore;

        public PartnerRegisterByReferralConfirmCommand(IUserStore<Domain.Entities.UserSetUp.User> passwordStore,
            UserManager<Domain.Entities.UserSetUp.User> userManager, EmailTokenService tokenService,
            DbContext dbContext)
        {
            _passwordStore = passwordStore as IUserPasswordStore<Domain.Entities.UserSetUp.User>;
            _userManager = userManager;
            _tokenService = tokenService;
            _dbContext = dbContext;
        }

        public async Task Handle(string referral, RegisterByReferralConfirmDto data)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == referral);

            if (user == null)
                throw new OperationException("Пользователь не найден", HttpStatusCode.NotFound);

            if (user.EmailConfirmed)
                throw new OperationException("Пользователь уже зарегестрирован");

            await _tokenService.ValidateToken(user, data.Code);

            await _passwordStore.SetPasswordHashAsync(user,
                _userManager.PasswordHasher.HashPassword(user, data.Password), CancellationToken.None);

            user.EmailConfirmed = true;
            user.UserName = user.Email;
            await _userManager.UpdateAsync(user);
        }
    }
}