﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Exception;
using UIR.Partner.Api.Core.Modules.User.Dto.Request;
using UIR.Service.Core.TokenService;

namespace UIR.Partner.Api.Core.Modules.User.Commands.PartnerUserReg
{
    public class PartnerRegisterConfirmCommand : IAsyncHandler<PartnerConfirmDto>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly EmailTokenService _tokenService;
        private readonly DbContext _dbContext;

        public PartnerRegisterConfirmCommand(UserManager<Domain.Entities.UserSetUp.User> userManager,
            EmailTokenService tokenService, DbContext dbContext)
        {
            _userManager = userManager;
            _tokenService = tokenService;
            _dbContext = dbContext;
        }
        
        public async Task Handle(PartnerConfirmDto data)
        {
            var user = await _userManager.FindByNameAsync(data.Email);
            
            if (user == null)
                throw new OperationException("user not found");
            
            await _tokenService.ValidateToken(user, data.Code);

            user.EmailConfirmed = true;
            await _userManager.AddToRoleAsync(user, RoleType.Partner.ToString());
            await _dbContext.SaveChangesAsync();
        }
    }
}