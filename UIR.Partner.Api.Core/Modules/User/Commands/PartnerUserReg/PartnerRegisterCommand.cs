﻿using System.Net.Mail;
using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.User.Dto;
using UIR.Service.Core.TokenService;

namespace UIR.Partner.Api.Core.Modules.User.Commands.PartnerUserReg
{
    public class PartnerRegisterCommand : IAsyncHandler<PartnerRegDto, long>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly DbContext _dbContext;
        private readonly EmailTokenService _tokenService;

        public PartnerRegisterCommand(
            UserManager<Domain.Entities.UserSetUp.User> userManager, 
            DbContext dbContext,
            EmailTokenService tokenService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _tokenService = tokenService;
        }

        public async Task<long> Handle(PartnerRegDto data)
        {
            var user = _dbContext.FirstOrDefault<PartnerUser>(x => x.UserName == data.Email);
            if (user != null)
            {
                if (user.EmailConfirmed || user.Email != data.Email)
                    throw new OperationException("Пользователь уже зарегистрирован");
            }
            else
            {
                user = new PartnerUser
                {
                    Email = data.Email,
                    UserName = data.Email,
                    Name = data.Name,
                    PhoneNumber = data.Phone
                };
                await _userManager.CreateAsync(user, data.Password);
                await _dbContext.SaveChangesAsync();
            }

            try
            {
                await _tokenService.SendToken(user);
            }
            catch (SmtpFailedRecipientException e)
            {
                throw new OperationException(e.Message);
            }

            return user.Id;
        }
    }
}