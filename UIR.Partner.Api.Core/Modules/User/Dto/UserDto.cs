﻿namespace UIR.Partner.Api.Core.Modules.User.Dto
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class UserInfoDto
    {
        public string Name { get; set; }
    }
}