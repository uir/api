﻿namespace UIR.Partner.Api.Core.Modules.User.Dto
{
    public class PartnerRegDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}