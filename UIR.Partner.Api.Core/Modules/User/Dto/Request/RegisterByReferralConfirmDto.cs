using System.ComponentModel.DataAnnotations;

namespace UIR.Partner.Api.Core.Modules.User.Dto.Request
{
    public class RegisterByReferralConfirmDto
    {
        [Required]
        public string Code { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}