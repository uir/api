﻿using System.ComponentModel.DataAnnotations;

namespace UIR.Partner.Api.Core.Modules.User.Dto.Request
{
    public class PartnerConfirmDto
    {
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Code { get; set; }
    }
}