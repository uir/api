﻿using AutoMapper;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Partner.Api.Core.Modules.User.Dto;

namespace UIR.Partner.Api.Core.Modules.User
{
    public class UserProfile
    {
        public class CategoryProfile : Profile
        {
            public CategoryProfile()
            {
                CreateMap<Domain.Entities.UserSetUp.User, UserInfoDto>();
                
                CreateMap<PartnerRegDto, PartnerUser>()
                    .ForMember(d => d.UserName, o => o.Ignore())
                    .ForMember(d => d.PhoneNumber, o => o.ResolveUsing(d => d.Phone));

                CreateMap<PartnerRegDto, Domain.Entities.Place>();
            }
        }
    }
}