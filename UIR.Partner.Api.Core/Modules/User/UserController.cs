using System.Linq;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.PaySystem.Specs;

namespace UIR.Partner.Api.Core.Modules.User
{
    public class UserController : Controller
    {
        private readonly DbContext _dbContext;

        public UserController(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("~/api/user/checkpaid/{phone}")]
        public IActionResult Check([FromRoute] string phone) => _dbContext
            .Set<Domain.Entities.UserSetUp.User>()
            .Where(UserPaySpec.IsPay(phone))
            .Any()
            .PipeTo(x => Ok(x));
    }
}