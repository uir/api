﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Shared.Core.PaySystem.Filters;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.User.Commands.ChangePassword;
using UIR.Partner.Api.Core.Modules.User.Commands.PartnerUserReg;
using UIR.Partner.Api.Core.Modules.User.Dto;
using UIR.Partner.Api.Core.Modules.User.Dto.Request;
using UIR.Service.Core.Payment.CountCheckers;

namespace UIR.Partner.Api.Core.Modules.User
{
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly PartnerRegisterByReferralCommand _registerByReferralCommand;
        private readonly PartnerRegisterByReferralConfirmCommand _registerByReferralConfirmCommand;
        private readonly ImagesPaymentPlanPartnerCountChecker _imagesChecker;
        private readonly VideosPaymentPlanPartnerCountChecker _videosChecker;
        private readonly IAsyncHandler<ChangePasswordDto> _changePasswordCommand;
        private readonly IAsyncHandler<ChangePasswordConfirmDto> _changePasswordConfirmCommand;
        private readonly IAsyncHandler<PartnerRegDto, long> _partnerRegCommand;
        private readonly IAsyncHandler<PartnerConfirmDto> _partnerConfirmCommand;

        public AuthController(
            DbContext dbContext,
            PartnerRegisterByReferralCommand registerByReferralCommand,
            PartnerRegisterByReferralConfirmCommand registerByReferralConfirmCommand,
            ImagesPaymentPlanPartnerCountChecker imagesChecker,
            VideosPaymentPlanPartnerCountChecker videosChecker,
            IAsyncHandler<ChangePasswordDto> changePasswordCommand,
            IAsyncHandler<ChangePasswordConfirmDto> changePasswordConfirmCommand,
            IAsyncHandler<PartnerRegDto, long> partnerRegCommand,
            IAsyncHandler<PartnerConfirmDto> partnerConfirmCommand)
        {
            _dbContext = dbContext;
            _registerByReferralCommand = registerByReferralCommand;
            _registerByReferralConfirmCommand = registerByReferralConfirmCommand;
            _imagesChecker = imagesChecker;
            _videosChecker = videosChecker;
            _changePasswordCommand = changePasswordCommand;
            _changePasswordConfirmCommand = changePasswordConfirmCommand;
            _partnerRegCommand = partnerRegCommand;
            _partnerConfirmCommand = partnerConfirmCommand;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDto dto)
        {
            await _changePasswordCommand.Handle(dto);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("changepassword/confirm")]
        public async Task<IActionResult> Confirm([FromBody] ChangePasswordConfirmDto dto)
        {
            await _changePasswordConfirmCommand.Handle(dto);
            return Ok();
        }


        [AllowAnonymous, HttpPost, Route("confirm/partner")]
        public async Task<IActionResult> ConfirmPartner([FromBody] PartnerConfirmDto dto)
        {
            await _partnerConfirmCommand.Handle(dto);
            return Ok();
        }

        [AllowAnonymous, HttpPost, Route("reg/partner")]
        public async Task<IActionResult> PartnerRegistered([FromBody] PartnerRegDto dto)
        {
            await _partnerRegCommand.Handle(dto);
            return Ok();
        }

        [AllowAnonymous, HttpPost, Route("reg/partner/{referral}")]
        public async Task<IActionResult> PartnerRegisteredByRef([FromRoute] string referral,
            [FromBody] PartnerRegDto dto)
        {
            await _registerByReferralCommand.Handle(referral, dto);
            return Ok();
        }

        [AllowAnonymous, HttpPost, Route("reg/partner/{referral}/confirm")]
        public async Task<IActionResult> ConfirmPartnerByRef([FromRoute] string referral,
            [FromBody] RegisterByReferralConfirmDto dto)
        {
            await _registerByReferralConfirmCommand.Handle(referral, dto);
            return Ok();
        }

        [Authorize, HttpGet, Route("~/api/user/allowcontent")]
        public IActionResult GetAllowContnet()
        {
            var paymentPlanValue = _dbContext
                .FirstBySpec(new LastSuccessPaymentFilter(User.GetId()))
                ?.PaymentPlan
                ?.Value;
            var imagesMax = _imagesChecker.AllowNotPayCount;
            var videosMax = _videosChecker.AllowNotPayCount;
            if (paymentPlanValue != null)
            {
                imagesMax = _imagesChecker.AllowCount[paymentPlanValue.Value];
                videosMax = _videosChecker.AllowCount[paymentPlanValue.Value];
            }

            return Ok(new
            {
                videosMaxCount = videosMax,
                imagesMaxCount = imagesMax
            });
        }
    }
}