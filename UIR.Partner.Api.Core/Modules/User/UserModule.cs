﻿using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Partner.Api.Core.Modules.User.Commands.ChangePassword;
using UIR.Partner.Api.Core.Modules.User.Commands.PartnerUserReg;
using UIR.Partner.Api.Core.Modules.User.Dto;
using UIR.Partner.Api.Core.Modules.User.Dto.Request;

namespace UIR.Partner.Api.Core.Modules.User
{
    public class UserModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IAsyncHandler<PartnerRegDto, long>, PartnerRegisterCommand>();
            services.AddScoped<IAsyncHandler<PartnerConfirmDto>, PartnerRegisterConfirmCommand>();
            services.AddScoped<IAsyncHandler<ChangePasswordDto>, ChangePasswordRequestCommand>();
            services.AddScoped<IAsyncHandler<ChangePasswordConfirmDto>, ChangePasswordConfirmCommand>();
            services.AddScoped<PartnerRegisterByReferralCommand>();
            services.AddScoped<PartnerRegisterByReferralConfirmCommand>();
        }
    }
}