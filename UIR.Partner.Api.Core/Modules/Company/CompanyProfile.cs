﻿using System.Linq;
using AutoMapper;
using UIR.Domain.Entities;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;

namespace UIR.Partner.Api.Core.Modules.Company
{
    public class CompanyProfile : Profile
    {
        public CompanyProfile()
        {
            CreateMap<EditCompanyDto, Organization>();
            CreateMap<EditCompanyDto, Domain.Entities.Company>();
            CreateMap<EditCompanyDto, IndividBusiness>();
            CreateMap<EditCompanyDto, SelfEmployed>();

            CreateMap<CategoryOrganization, CategoryTagDto>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.CategoryId))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Category.Name));

            CreateMap<Domain.Entities.Company, CompanyDto>();
            CreateMap<IndividBusiness, IndividBusinessDto>();
            CreateMap<SelfEmployed, SelfEmployedDto>();

            CreateMap<Organization, CompanyListDto>()
                .ForMember(d => d.Logo, o => o.MapFrom(OrganizationFormatter.Logo));
            CreateMap<Organization, CompanyNavbarInfoDto>();

            CreateMap<PlaceCreateDto, Domain.Entities.Place>()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location
                {
                    Latitude = src.Lat,
                    Longitude = src.Lng
                }));

            CreateMap<EditPlaceDto, Domain.Entities.Place>()
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => new Location
                {
                    Latitude = src.Lat,
                    Longitude = src.Lng
                }));

            CreateMap<Domain.Entities.Sale, PlaceDashboardSaleDto>();

            CreateMap<Image, PlaceDashboardImageDto>();

            CreateMap<Domain.Entities.Video, PlaceDashboardVideoDto>();

            CreateMap<Domain.Entities.Place, PlaceDashboardDto>()
                .ForMember(dest => dest.RoomType, opts => opts.MapFrom(src => src.RoomType))
                .ForMember(dest => dest.Images, opts => opts.MapFrom(src => src.Images.Select(x => x.Image)))
                .ForMember(dest => dest.Videos, opts => opts.MapFrom(src => src.Videos.Select(x => x.Video)))
                .ForMember(dest => dest.Sales, opts => opts.MapFrom(src => src.Sales.Select(x => x.Sale)))
                .ForMember(dest => dest.Lat, opts => opts.MapFrom(src => src.Location.Latitude))
                .ForMember(dest => dest.Lng, opts => opts.MapFrom(src => src.Location.Longitude));

            CreateMap<Domain.Entities.Place, PlaceListDto>();

            CreateMap<Domain.Entities.Place, CompanyPlaceDto>()
                .ForMember(dest => dest.Lat, opts => opts.MapFrom(src => src.Location.Latitude))
                .ForMember(dest => dest.Lng, opts => opts.MapFrom(src => src.Location.Longitude));
        }
    }
}