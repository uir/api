﻿using System.Linq;
using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Company.Filters
{
    public class PlaceListSpec: IQueryableFilter<Domain.Entities.Place>
    {
        public long CompanyId { get; set; }

        public PlaceListSpec(long companyId)
        {
            CompanyId = companyId;
        }

        public IQueryable<Domain.Entities.Place> Apply(IQueryable<Domain.Entities.Place> query) => query
            .Where(x => x.Organization.Id == CompanyId);
    }
}