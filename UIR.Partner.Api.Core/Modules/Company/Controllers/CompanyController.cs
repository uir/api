﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Auth.Core.Filters;
using UIR.Domain.Entities;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Commands;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;

namespace UIR.Partner.Api.Core.Modules.Company.Controllers
{
    public class CompanyController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly SetOrganizationCategories _setOrganizationCategories;
        private readonly IHandler<EditCompanyDto> _editCompanyCommand;
        private AppSettings.AppSettings _settings;


        public CompanyController(
            DbContext dbContext,
            SetOrganizationCategories setOrganizationCategories,
            IOptions<AppSettings.AppSettings> settings,
            IHandler<EditCompanyDto> editCompanyCommand
        )
        {
            _dbContext = dbContext;
            _setOrganizationCategories = setOrganizationCategories;
            _editCompanyCommand = editCompanyCommand;
            _settings = settings.Value;
        }

        /// <summary>
        /// Возвращает компании
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult Companies()
        {
            var userId = User.GetId();
            var res = _dbContext
                .FilterToArray<Organization, CompanyListDto>(x => x.OwnerId == userId)
                .Each(x => x.Logo = x.Logo.ToPathImg(Request.GetUri(), _settings.CloudHost));
            return Ok(res);
        }

        /// <summary>
        /// Изменение категорий
        /// </summary>
        /// <returns></returns>
        [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company))]
        [Route("~/api/companies/{id}/categories"), HttpPatch, Authorize(Roles = "Partner")]
        public IActionResult SetCategories([FromRoute] long id, [FromBody] long[] categories)
        {
            _setOrganizationCategories.Handle(id, categories);
            return Ok();
        }

        /// <summary>
        /// Удаление компании
        /// </summary>
        /// <returns></returns>
        [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company))]
        [Route("~/api/companies/{id}"), HttpDelete, Authorize(Roles = "Partner")]
        public IActionResult DeleteCompany(long id)
        {
            _dbContext.DeleteById<Organization>(id);
            return Ok();
        }

        /// <summary>
        /// Редактирование компании
        /// </summary>
        /// <returns></returns>
        [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company))]
        [Route("~/api/companies/{id}"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult EditCompany(long id, [FromBody] EditCompanyDto dto)
        {
            _editCompanyCommand.Handle(dto.SetId(id));
            return Ok();
        }

        /// <summary>
        /// Получение информации для Navbar
        /// </summary>
        /// <returns></returns>
        [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company))]
        [Route("~/api/companies/{id}/navbar"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult NavbarInfoCompany(long id)
        {
            Mapper.Map<CompanyNavbarInfoDto>(_dbContext.Set<Organization>().ById(id));
            return Ok();
        }

        /// <summary>
        /// Получение информации по реф ссылке
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("~/api/companies/ref/{referral}/info"), HttpGet]
        public async Task<IActionResult> GetInfoByRef(string referral)
        {
            var info = await _dbContext
                .Set<Organization>()
                .Where(x => x.Owner.UserName == referral)
                .Select(x => new {x.Name, x.Address})
                .FirstOrDefaultAsync();
            return Ok(info);
        }


        /// <summary>
        /// Получение основной информации
        /// </summary>
        /// <returns></returns>
        [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company))]
        [Route("~/api/companies/{id}"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult MainInfoCompany(long id)
        {
            object GetOrganization<T, TProj>()
                where T : Organization
                where TProj : OrganizationDto
            {
                var org = _dbContext.FirstById<T, TProj>(id);
                org.Logo = _dbContext
                    .Where<Image>(x => x.IsLogo && x.OrganizationId == id)
                    .FirstOrDefault()
                    ?.File
                    ?.ToPathImg(Request.GetUri(), _settings.CloudHost);
                return org;
            }

            var orgbase = _dbContext.GetById<Organization>(id);

            switch (orgbase.ActiveType)
            {
                case ActiveType.Company:
                    return Ok(GetOrganization<Domain.Entities.Company, CompanyDto>());
                case ActiveType.IndividBusiness:
                    return Ok(GetOrganization<IndividBusiness, IndividBusinessDto>());
                case ActiveType.SelfEmployed:
                    return Ok(GetOrganization<SelfEmployed, SelfEmployedDto>());
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}