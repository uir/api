﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Auth.Core.Filters;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;
using UIR.Partner.Api.Core.Modules.Company.Filters;

namespace UIR.Partner.Api.Core.Modules.Company.Controllers
{
    [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company), "companyId")]
    public class CompanyTagController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IHandler<UntieTagDto> _untieTagCommand;
        private readonly IHandler<BindTagDto> _bindTagCommand;

        public CompanyTagController(
            DbContext dbContext,
            IHandler<UntieTagDto> untieTagCommand,
            IHandler<BindTagDto> bindTagCommand)
        {
            _dbContext = dbContext;
            _untieTagCommand = untieTagCommand;
            _bindTagCommand = bindTagCommand;
        }

        /// <summary>
        /// Возвращает теги
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/tags"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult GetTags(long companyId) => _dbContext
            .Where<OrganizationTag>(x => x.OrganizationId == companyId)
            .Include(x => x.Tag)
            .Select(x => x.Tag)
            .ProjectTo<TagDto>()
            .PipeTo(Ok);

        /// <summary>
        /// Отвязывает тег
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/tags/{tagId}"), HttpDelete, Authorize(Roles = "Partner")]
        public IActionResult UntieTag(long companyId, long tagId)
        {
            _untieTagCommand.Handle(new UntieTagDto
            {
                CompanyId = companyId,
                TagId = tagId
            });
            return Ok();
        }

        /// <summary>
        /// Привязывает тег
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/tags"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult BindTag(long companyId, [FromBody] BindTagDto dto)
        {
            _bindTagCommand.Handle(dto);
            return Ok();
        }
    }
}