﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Auth.Core.Filters;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;
using UIR.Partner.Api.Core.Modules.Company.Filters;

namespace UIR.Partner.Api.Core.Modules.Company.Controllers
{
    [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company))]
    public class CompanyPlaceController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IHandler<PlaceCopyDto> _copyCommand;
        private readonly AppSettings.AppSettings _settings;
        private readonly IHandler<PlaceCreateDto, long> _placeCreateCommand;

        public CompanyPlaceController(
            DbContext dbContext,
            IHandler<PlaceCopyDto> copyCommand,
            IOptions<AppSettings.AppSettings> settings,
            IHandler<PlaceCreateDto, long> placeCreateCommand)
        {
            _dbContext = dbContext;
            _copyCommand = copyCommand;
            _settings = settings.Value;
            _placeCreateCommand = placeCreateCommand;
        }

        /// <summary>
        /// Возвращает филиалы
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/dashboard/places"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult Dashboard(long id) => _dbContext
            .Where<Domain.Entities.Place>(x => x.OrganizationId == id)
            .ProjectTo<PlaceDashboardDto>()
            .ToArray()
            .Each(x => x.Images.Each(i => i.File = i.File.ToPathImg(Request.GetUri(), _settings.CloudHost)))
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает филиалы
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/places"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult Places(long id) => _dbContext
            .FilterToArray<Domain.Entities.Place, PlaceListDto>(new PlaceListSpec(id))
            .PipeTo(Ok);

        /// <summary>
        /// Создание филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/places"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult CreatePlace(long id, [FromBody] PlaceCreateDto dto)
        {
            var placeId = _placeCreateCommand.Handle(dto);
            return Ok(placeId);
        }

        /// <summary>
        /// Создание филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/places/{copyId}/copy"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult CopyPlace(long id, long copyId, [FromBody] PlaceCreateDto dto)
        {
            _copyCommand.Handle(new PlaceCopyDto(dto, copyId));
            return Ok();
        }

        /// <summary>
        /// Получение филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/places/{placeId}"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult Place(long id, long placeId) => _dbContext
            .FirstById<Domain.Entities.Place, PlaceDashboardDto>(placeId)
            .PipeTo(x =>
                {
                    x.Images.Each(i => i.File = i.File.ToPathImg(Request.GetUri(), _settings.CloudHost));
                    return x;
                }
            )
            .PipeTo(Ok);

        /// <summary>
        /// Удаление филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/places/{placeId}"), HttpDelete, Authorize(Roles = "Partner")]
        public IActionResult DeletePlace(long id, long placeId)
        {
            //todo сделать удаление
//            var place = _dbContext.Set<Domain.Entities.Place>().ById(placeId);
//            _dbContext.DeleteByIds<Image>(place.Images.Select(x => x.Id).ToArray());
//            _dbContext.DeleteByIds<Domain.Entities.Video>(place.Videos.Select(x => x.Id).ToArray());
//            _dbContext.DeleteByIds<Domain.Entities.Sale>(place.Sales.Select(x => x.Id).ToArray());
//            place.Periods.Clear();
//            place.UsersAddedBookmarks.Clear();
//            _dbContext.DeleteById<Domain.Entities.Place>(place.Id);
//            _dbContext.SaveChanges();
            return Ok();
        }


        /// <summary>
        /// Редактирование филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{id}/places/{placeId}"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult EditPlace(long id, long placeId, [FromBody] EditPlaceDto dto)
        {
            _dbContext.Edit<Domain.Entities.Place, EditPlaceDto>(dto, placeId);
            return Ok();
        }
    }
}