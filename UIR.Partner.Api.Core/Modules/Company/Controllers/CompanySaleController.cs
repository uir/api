﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Auth.Core.Filters;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;

namespace UIR.Partner.Api.Core.Modules.Company.Controllers
{
    [AuthorizePermission(typeof(OrganizationPermissionSpec), typeof(Domain.Entities.Company), "companyId")]
    public class CompanySaleController : Controller
    {
        private readonly DbContext _dbContext;

        public CompanySaleController(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Возвращает скидки
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/sales"), HttpGet, Authorize(Roles = "Partner")]
        public IActionResult GetSales(long companyId) => _dbContext
            .Set<Domain.Entities.Sale>()
            .Where(SaleSpec.ByOrganizationId(companyId))
            .ProjectTo<SaleDto>()
            .PipeTo(Ok);

        /// <summary>
        /// Создает скидку
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/sales"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult CreateSale(long companyId, [FromBody] CreateCompanySaleDto dto)
        {
            var company = _dbContext.Set<Organization>().Include(x => x.Sales).ById(companyId);
            var sale = Mapper.Map<Domain.Entities.Sale>(dto);
            sale.Organization = company;
            _dbContext.Add(sale);
            _dbContext.SaveChanges();
            return Ok(sale.Id);
        }

        /// <summary>
        /// Удаляет скидку
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/sales/{saleId}"), HttpDelete, Authorize(Roles = "Partner")]
        public IActionResult DeleteSale(long companyId, long saleId)
        {
            var sale = _dbContext.Set<Domain.Entities.Sale>().ById(saleId);
            var sales = _dbContext.Set<Organization>().ById(companyId).Sales;

            if (!sales.Contains(sale)) return BadRequest();

            sales.Remove(sale);
            _dbContext.DeleteById<Domain.Entities.Sale>(saleId);
            return Ok();
        }


        /// <summary>
        /// Редактирует скидку
        /// </summary>
        /// <returns></returns>
        [Route("~/api/companies/{companyId}/sales/{saleId}"), HttpPost, Authorize(Roles = "Partner")]
        public IActionResult EditSale(long companyId, [FromBody] EditSaleDto dto)
        {
            dto.Id.ThrowIfDefault();
            var oldSale = _dbContext.Set<Domain.Entities.Sale>().ById(dto.Id);
            Mapper.Map(dto, oldSale);
            _dbContext.SaveChanges();
            return Ok();
        }
    }
}