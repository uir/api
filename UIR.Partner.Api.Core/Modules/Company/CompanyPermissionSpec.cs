﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using UIR.Auth.Core.Permission;
using UIR.Domain.Entities.OrganizationSetUp;

namespace UIR.Partner.Api.Core.Modules.Company
{
    public class OrganizationPermissionSpec : PermissionSpecBase<Organization>
    {
        public override IQueryable<Organization> Apply(IQueryable<Organization> query) => query
            .Where(x => x.Id == EntityId && x.Owner.Id == UserId);

        public OrganizationPermissionSpec(DbContext dbContext, long userId, long entityId) : base(dbContext, userId, entityId)
        {
        }
    }
}