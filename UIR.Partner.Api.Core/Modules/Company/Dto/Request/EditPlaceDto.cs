﻿using Force.Ddd;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class EditPlaceDto : HasIdBase<long>
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Vicinity { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public PriceLevel PriceLevel { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Description { get; set; }
        public RoomType? RoomType { get; set; }
        public string Room { get; set; }
        public bool IsActive { get; set; }
    }
}