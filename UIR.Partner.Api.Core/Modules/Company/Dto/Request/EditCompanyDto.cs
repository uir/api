﻿using AutoMapper;
using Force.Ddd;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class EditCompanyDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Fio { get; set; }
        public string Address { get; set; }
        public ActiveType ActiveType { get; set; }
        public string Inn { get; set; }
        public string Ogrn { get; set; }
        public string Kpp { get; set; }
        public string NumberPassport { get; set; }
        public string SeriesPassport { get; set; }
        public string ResidenceAddress { get; set; }
        public string Birthday { get; set; }
        public string PassportDepartment { get; set; }
        public string DatePassport { get; set; }

        [IgnoreMap]
        public long[] CategoriesIds { get; set; }
    }
}