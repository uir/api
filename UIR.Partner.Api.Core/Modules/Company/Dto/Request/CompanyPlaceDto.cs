﻿using Force.Ddd;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class CompanyPlaceDto: HasIdBase<long>
    {
        public long CompanyId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Vicinity { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public PriceLevel PriceLevel { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        
    }
}