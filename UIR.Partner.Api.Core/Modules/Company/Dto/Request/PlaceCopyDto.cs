namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class PlaceCopyDto
    {
        public PlaceCopyDto(PlaceCreateDto dto, long copyId)
        {
            CreateDto = dto;
            CopyId = copyId;
        }

        public PlaceCreateDto CreateDto { get; set; }
        public long CopyId { get; set; }
    }
}