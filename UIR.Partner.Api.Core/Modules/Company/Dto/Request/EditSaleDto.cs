﻿using System;
using System.ComponentModel.DataAnnotations;
using Force.Ddd;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class EditSaleDto : HasIdBase<long>
    {
        [Required]
        public override long Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Description { get; set; }
        
        public double Amount { get; set; }
        
        public bool IsActive { get; set; }
        
        [Required]
        public DiscountType DiscountType { get; set; }
        
        public DateTimeOffset? DiscountEndDate { get; set; }
        
        public bool IsExclusive { get; set; }
    }
}