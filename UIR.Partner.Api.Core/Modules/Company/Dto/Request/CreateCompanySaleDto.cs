﻿using System.ComponentModel.DataAnnotations;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class CreateCompanySaleDto
    {
        [Required]
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        [Range(0, 100)]
        public double Amount { get; set; }
        
        public bool IsActive { get; set; }
    }
}