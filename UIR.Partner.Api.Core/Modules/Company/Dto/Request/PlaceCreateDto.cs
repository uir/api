﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Request
{
    public class PlaceCreateDto
    {
        [Range(1, long.MaxValue)]
        public long OrganizationId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Phone { get; set; }
        public string Vicinity { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        
        [Required]
        public string Address { get; set; }
        
        [Range(double.MinValue, double.MaxValue)]
        public double Lat { get; set; }
        
        [Range(double.MinValue, double.MaxValue)]
        public double Lng { get; set; }
        public RoomType? RoomType { get; set; }
        public string Room { get; set; }
        public bool IsActive { get; set; }
    }
}