﻿using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Responce
{
    public class CompanyListDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Logo { get; set; }
    }
}