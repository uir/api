﻿﻿using Force.Ddd;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Responce
{
    public class CategoryTagDto : HasIdBase<long>
    {
        public string Name { get; set; }
    }
}