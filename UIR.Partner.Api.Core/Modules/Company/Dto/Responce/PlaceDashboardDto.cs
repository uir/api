﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Responce
{
    public class PlaceDashboardDto : HasIdBase<long>
    {
        public long OrganizationId { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Phone { get; set; }
        public string Vicinity { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Description { get; set; }
        public RoomType? RoomType { get; set; }
        public string Room { get; set; }
        public bool IsActive { get; set; }


        public List<PlaceDashboardImageDto> Images { get; set; }
        public List<PlaceDashboardSaleDto> Sales { get; set; }
        public List<PlaceDashboardVideoDto> Videos { get; set; }
    }

    public class PlaceDashboardSaleDto
    {
        public string Name { get; set; }
        public double Amount { get; set; }
        public DiscountType DiscountType { get; set; }
    }

    public class PlaceDashboardImageDto
    {
        public string File { get; set; }
    }

    public class PlaceDashboardVideoDto
    {
        public string Name { get; set; }
    }
}