﻿namespace UIR.Partner.Api.Core.Modules.Company.Dto.Responce
{
    public class BindTagDto
    {
        public long TagId { get; set; }
        public string Value { get; set; }
        public long CompanyId { get; set; }
    }
}