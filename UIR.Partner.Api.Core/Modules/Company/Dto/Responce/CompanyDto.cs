﻿using System.Collections.Generic;
using AutoMapper;
using UIR.Domain.Enums;

namespace UIR.Partner.Api.Core.Modules.Company.Dto.Responce
{
    public class OrganizationDto 
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Fio { get; set; }
        public ActiveType ActiveType { get; set; }
        
        [IgnoreMap]
        public string Logo { get; set; }

        public ICollection<CategoryTagDto> Categories { get; set; }
    }
        
    public class IndividBusinessDto : OrganizationDto
    {
        public string Inn { get; set; }
        public string Ogrn { get; set; }       
    }
    
    public class CompanyDto : IndividBusinessDto
    {
        public string Kpp { get; set; }
    }

    public class SelfEmployedDto : OrganizationDto
    {
        public string NumberPassport { get; set; }
        public string SeriesPassport { get; set; }
        public string Birthday { get; set; }
        public string ResidenceAddress { get; set; }
        public string DatePassport { get; set; }
        public string PassportDepartment { get; set; }
    }
}