﻿namespace UIR.Partner.Api.Core.Modules.Company.Dto.Responce
{
    public class CompanyNavbarInfoDto
    {
        public string Name { get; set; }
    }
}