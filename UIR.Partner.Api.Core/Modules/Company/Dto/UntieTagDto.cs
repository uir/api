﻿namespace UIR.Partner.Api.Core.Modules.Company.Dto
{
    public class UntieTagDto
    {
        public long TagId { get; set; }
        public long CompanyId { get; set;}
    }
}