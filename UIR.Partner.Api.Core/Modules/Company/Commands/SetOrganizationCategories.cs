using System.Linq;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;

namespace UIR.Partner.Api.Core.Modules.Company.Commands
{
    public class SetOrganizationCategories
    {
        private readonly DbContext _dbContext;

        public SetOrganizationCategories(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Handle(long orgId, long[] categoriesIds)
        {
            var exist = _dbContext
                .Set<CategoryOrganization>()
                .Where(x => x.OrganizationId == orgId);
            _dbContext.RemoveRange(exist);
            _dbContext.SaveChanges();
            
            _dbContext
                .Set<CategoryOrganization>()
                .AddRange(categoriesIds.Select(x => new CategoryOrganization
                {
                    CategoryId = x,
                    OrganizationId = orgId
                }));
            _dbContext.SaveChanges();
        }
    }
}