﻿using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;

namespace UIR.Partner.Api.Core.Modules.Company.Commands
{
    public class BindTagCommand : IHandler<BindTagDto>
    {
        private readonly DbContext _dbContext;

        public BindTagCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Handle(BindTagDto dto)
        {
            var tags = _dbContext
                .Where<OrganizationTag>(x => x.OrganizationId == dto.CompanyId)
                .ToArray();

            var tagId = dto.TagId;

            if (tagId == 0)
            {
                if (string.IsNullOrEmpty(dto.Value))
                    throw new OperationException("Знаение не может быть пустым.");

                var res = _dbContext
                    .Set<Tag>()
                    .Where(x => x.Name == dto.Value)
                    .Select(x => x.Id)
                    .FirstOrDefault();

                tagId = res == 0
                    ? _dbContext
                        .Set<Tag>()
                        .Add(new Tag
                        {
                            Name = dto.Value
                        })
                        .Entity
                        .Id
                    : res;
            }

            if (tags.Any(x => x.TagId == tagId))
                return;

            _dbContext
                .Set<OrganizationTag>()
                .Add(new OrganizationTag
                {
                    TagId = tagId,
                    OrganizationId = dto.CompanyId
                });
            _dbContext.SaveChanges();
        }
    }
}