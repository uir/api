using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;

namespace UIR.Partner.Api.Core.Modules.Company.Commands
{
    public class PlaceCopyCommand: IHandler<PlaceCopyDto>
    {
        private readonly IHandler<PlaceCreateDto, long> _createCommand;
        private readonly DbContext _dbContext;

        public PlaceCopyCommand(IHandler<PlaceCreateDto, long> createCommand, DbContext dbContext)
        {
            _createCommand = createCommand;
            _dbContext = dbContext;
        }

        public void Handle(PlaceCopyDto dto)
        {
            var newPlaceId = _createCommand.Handle(dto.CreateDto);
            
            var copiedImages = _dbContext
                .Set<ImagePlace>()
                .Where(x => x.PlaceId == dto.CopyId)
                .Select(x => x.ImageId)
                .ToArray()
                .Select(x => new ImagePlace
                {
                    ImageId = x,
                    PlaceId = newPlaceId
                });
            
            var copiedVideos = _dbContext
                .Set<VideoPlace>()
                .Where(x => x.PlaceId == dto.CopyId)
                .Select(x => x.VideoId)
                .ToArray()
                .Select(x => new VideoPlace
                {
                    VideoId = x,
                    PlaceId = newPlaceId
                });
            
            var copiedSales = _dbContext
                .Set<SalePlace>()
                .Where(x => x.PlaceId == dto.CopyId)
                .Select(x => x.SaleId)
                .ToArray()
                .Select(x => new SalePlace
                {
                    SaleId = x,
                    PlaceId = newPlaceId
                });
            
            _dbContext.AddRange(copiedImages);
            _dbContext.AddRange(copiedVideos);
            _dbContext.AddRange(copiedSales);
            _dbContext.SaveChanges();
        }
    }
}