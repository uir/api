﻿using System;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Extensions.Core.Extensions;

namespace UIR.Partner.Api.Core.Modules.Company.Commands
{
    public class PlaceCreateCommand : IHandler<PlaceCreateDto, long>
    {
        private readonly DbContext _dbContext;

        public PlaceCreateCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public long Handle(PlaceCreateDto dto)
        {
            var newPlace = dto.Map<Domain.Entities.Place>();
            newPlace.CreateDate = DateTimeOffset.UtcNow;

            var entry = _dbContext.Add(newPlace);
            _dbContext.SaveChanges();

            return entry.Entity.Id;
        }
    }
}