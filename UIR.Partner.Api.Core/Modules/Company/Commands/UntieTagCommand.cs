﻿using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto;

namespace UIR.Partner.Api.Core.Modules.Company.Commands
{
    public class UntieTagCommand : IHandler<UntieTagDto>
    {
        private readonly DbContext _dbContext;

        public UntieTagCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Handle(UntieTagDto dto)
        {
            var tag = _dbContext.Set<OrganizationTag>()
                .FirstOrDefault(x => x.TagId == dto.TagId && x.OrganizationId == dto.CompanyId);
            if (tag == null)
                throw new OperationException("Тег не принадлежит партнеру");

            _dbContext.Set<OrganizationTag>().Remove(tag);

            if (!_dbContext.Set<OrganizationTag>().Any(x => x.TagId == dto.TagId))
                _dbContext.RemoveById<Domain.Entities.Tag>(dto.TagId);

            _dbContext.SaveChanges();
        }
    }
}