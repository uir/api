﻿using System.Linq;
using AutoMapper;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Entities.OrganizationSetUp;
using UIR.Extensions.Core.Extensions;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;

namespace UIR.Partner.Api.Core.Modules.Company.Commands
{
    public class EditCompanyCommand : IHandler<EditCompanyDto>
    {
        private readonly DbContext _dbContext;

        public EditCompanyCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Handle(EditCompanyDto dto)
        {
            dto.Id.ThrowIfDefault();

            var org = _dbContext
                .GetById<Organization>(dto.Id);

            Mapper.Map(dto, org);

            var categories = _dbContext.Set<CategoryOrganization>().Where(x => x.OrganizationId == dto.Id).ToArray();
            
            _dbContext.Set<CategoryOrganization>().RemoveRange(categories);
            _dbContext.SaveChanges();
            
            _dbContext.Set<CategoryOrganization>().AddRange(dto.CategoriesIds.Select(x => new CategoryOrganization
            {
                CategoryId = x,
                OrganizationId = dto.Id
            }));
            _dbContext.SaveChanges();
        }
    }
}