﻿using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Partner.Api.Core.Modules.Company.Commands;
using UIR.Partner.Api.Core.Modules.Company.Dto;
using UIR.Partner.Api.Core.Modules.Company.Dto.Request;
using UIR.Partner.Api.Core.Modules.Company.Dto.Responce;
using UIR.Partner.Api.Core.Modules.Company.Filters;

namespace UIR.Partner.Api.Core.Modules.Company
{
    public class CompanyModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IHandler<EditCompanyDto>, EditCompanyCommand>();
            services.AddScoped<IHandler<UntieTagDto>, UntieTagCommand>();
            services.AddScoped<IHandler<BindTagDto>, BindTagCommand>();
            services.AddScoped<IHandler<PlaceCreateDto, long>, PlaceCreateCommand>();
            services.AddScoped<IHandler<PlaceCopyDto>, PlaceCopyCommand>();
            services.AddScoped<SetOrganizationCategories>();
        }
    }
}