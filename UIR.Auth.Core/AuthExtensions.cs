﻿using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using OpenIddict.Server;
using UIR.Auth.Core.Services;
using UIR.DAL.Core;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Auth.Core
{
    public static class AuthExtensions
    {
        public static void AddUirAuth(this IServiceCollection services, AuthSettings settings)
        {
            services.AddIdentity<User, Role>(config =>
                {
                    config.SignIn.RequireConfirmedPhoneNumber = settings.RequireConfirmedPhoneNumber;
                    config.SignIn.RequireConfirmedEmail = settings.RequireConfirmedEmail;
                    config.Password = new PasswordOptions
                    {
                        RequiredLength = 6,
                        RequireUppercase = false,
                        RequireNonAlphanumeric = false,
                        RequireLowercase = false
                    };
                })
                .AddEntityFrameworkStores<UirDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            });

            services.AddOpenIddict()
                .AddCore(options =>
                {
                    options.UseEntityFrameworkCore()
                        .UseDbContext<UirDbContext>();
                })
                .AddServer(options =>
                {
                    options.UseMvc();
                    options.EnableTokenEndpoint("/token");
                    options.AllowPasswordFlow().AllowRefreshTokenFlow();
                    options.AcceptAnonymousClients();
                    options.DisableHttpsRequirement();
                    options.AddEventHandler<OpenIddictServerEvents.MatchEndpoint>(notification =>
                    {
                        var request = notification.Context.HttpContext.Request;
                        if (request.Path == "/api/admin/organizations/login")
                        {
                            notification.Context.MatchTokenEndpoint();
                        }

                        return Task.FromResult(OpenIddictServerEventState.Handled);
                    });
                })
                .AddValidation();

            services.AddSingleton<IAuthenticationSchemeProvider, CustomAuthenticationSchemeProvider>();
            services.AddScoped<IAuthorizeService, AuthorizeService>();
        }
    }
}