﻿using System;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;

namespace UIR.Auth.Core.Filters
{
    public class TestAttribute : AuthorizeAttribute
    {
        public override bool IsDefaultAttribute()
        {
            return base.IsDefaultAttribute();
        }

        public override bool Match(object obj)
        {
            return base.Match(obj);
        }
    }
    public class SpecAuthorizeAttribute : IAuthorizationFilter
    {
        private readonly DbContext _dbContext;
        private readonly Type _spec;
        private readonly Type _ent;
        private readonly string _idSegmentName;

        public SpecAuthorizeAttribute(DbContext dbContext, Type spec, Type ent, string idSegmentName = "id")
        {
            _dbContext = dbContext;
            _spec = spec;
            _ent = ent;
            _idSegmentName = idSegmentName;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                SetError(context, (int)HttpStatusCode.Forbidden, "Пользователь не авторизован.");
                return;
            }

            var userId = context.HttpContext.User.GetId();
            var entId = GetEntitiId(context);
            var specRes = IsAuthorizedBySpec(userId, entId);
            if (!specRes)
                SetError(context, (int) HttpStatusCode.Forbidden, "Недостаточно прав для выполнения данной операции.");
        }

        private void SetError(AuthorizationFilterContext context, int code, string error)
        {
            context.HttpContext.Response.StatusCode = 401;
            context.Result = new JsonResult(new OperationError
            {
                Message = error
            });   
        }

        private long GetEntitiId(AuthorizationFilterContext actionContext)
        {
            if (string.IsNullOrEmpty(_idSegmentName))
                throw new ArgumentException("Id segment name can`t be null or empty");
            if (actionContext.RouteData?.Values != null &&
                actionContext.RouteData.Values.ContainsKey(_idSegmentName))
            {
                var entIdStr = actionContext.RouteData.Values[_idSegmentName].ToString();
                if (!long.TryParse(entIdStr, out var entId))
                    throw new OperationException("Id segment value is not number");

                return entId;
            }

            return 0;
        }

        private bool IsAuthorizedBySpec(long userId, long entId)
        {
            var spec = Activator.CreateInstance(_spec, _dbContext, userId, entId);
            var isHavePermissionMethod = _spec.GetMethod("IsHavePermission");
            
            if (isHavePermissionMethod == null)
                throw new Exception("This method use reflection. Check interfece and implementation.");

            return (bool) isHavePermissionMethod.Invoke(spec, new object[0]);
        }
    }
}