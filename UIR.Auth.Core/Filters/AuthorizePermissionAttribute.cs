﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace UIR.Auth.Core.Filters
{
    public class AuthorizePermissionAttribute : TypeFilterAttribute
    {
        public AuthorizePermissionAttribute(Type specType, Type entityType, string idSegmentName = "id") : base(
            typeof(SpecAuthorizeAttribute))
        {
            Arguments = new object[] {specType, entityType, idSegmentName};
        }
    }
}