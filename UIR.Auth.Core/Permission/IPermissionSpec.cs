﻿using System.Linq;
using Force.Ddd;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Extensions;

namespace UIR.Auth.Core.Permission
{
    public interface IPermissionSpec<TEt> : IQueryableFilter<TEt> where TEt : class, IHasId
    {
        long UserId { get; }
        long EntityId { get; }

        bool IsHavePermission();
    }

    public abstract class PermissionSpecBase<TEt> : IPermissionSpec<TEt> where TEt : class, IHasId
    {
        private readonly DbContext _dbContext;
        public long UserId { get; }
        public long EntityId { get; }

        protected PermissionSpecBase(DbContext dbContext, long userId, long entityId)
        {
            _dbContext = dbContext;
            UserId = userId;
            EntityId = entityId;
        }

        public bool IsHavePermission() => _dbContext.Where(this).Any();

        public abstract IQueryable<TEt> Apply(IQueryable<TEt> query);
    }
}