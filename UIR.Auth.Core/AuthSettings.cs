﻿namespace UIR.Auth.Core
{
    public class AuthSettings
    {
        public bool RequireConfirmedPhoneNumber { get; set; }
        public bool RequireConfirmedEmail { get; set; }
    }
}