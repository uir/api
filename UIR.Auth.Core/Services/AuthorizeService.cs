using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using OpenIddict.Abstractions;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;

namespace UIR.Auth.Core.Services
{
    public interface IAuthorizeService
    {
        Task<AuthenticationTicket> CreateTicketAsync(OpenIdConnectRequest oidcRequest, User user,
            AuthenticationProperties properties = null);
    }

    public class AuthorizeService : IAuthorizeService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IOptions<IdentityOptions> _identityOptions;

        public AuthorizeService(SignInManager<User> signInManager,
            UserManager<User> userManager, IOptions<IdentityOptions> identityOptions)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _identityOptions = identityOptions;
        }

        public async Task<AuthenticationTicket> CreateTicketAsync(
            OpenIdConnectRequest oidcRequest, User user,
            AuthenticationProperties properties = null)
        {
            var principal = await _signInManager.CreateUserPrincipalAsync(user);
            var ticket = new AuthenticationTicket(principal, properties,
                OpenIdConnectServerDefaults.AuthenticationScheme);

            if (!oidcRequest.IsRefreshTokenGrantType())
            {
                ticket.SetScopes(new[]
                {
                    OpenIdConnectConstants.Scopes.OpenId,
                    OpenIdConnectConstants.Scopes.Email,
                    OpenIdConnectConstants.Scopes.Profile,
                    OpenIdConnectConstants.Scopes.OfflineAccess,
                    OpenIddictConstants.Scopes.Roles
                }.Intersect(oidcRequest.GetScopes()));
            }

            ticket.SetResources("resource_server");
            var roles = await _userManager.GetRolesAsync(user);
            var identity = GetClaimsIdentity(user, roles);

            principal.AddIdentities(new[] {identity});

            foreach (var claim in ticket.Principal.Claims)
            {
                if (claim.Type == _identityOptions.Value.ClaimsIdentity.SecurityStampClaimType)
                {
                    continue;
                }

                var destinations = new List<string>
                {
                    OpenIdConnectConstants.Destinations.AccessToken
                };
                if ((claim.Type == OpenIdConnectConstants.Claims.Name &&
                     ticket.HasScope(OpenIdConnectConstants.Scopes.Profile)) ||
                    (claim.Type == OpenIdConnectConstants.Claims.Email &&
                     ticket.HasScope(OpenIdConnectConstants.Scopes.Email)) ||
                    (claim.Type == OpenIdConnectConstants.Claims.Role &&
                     ticket.HasScope(OpenIddictConstants.Claims.Roles)))
                {
                    destinations.Add(OpenIdConnectConstants.Destinations.IdentityToken);
                }

                claim.SetDestinations(destinations);
            }

            return ticket;
        }

        private static ClaimsIdentity GetClaimsIdentity(User user, IList<string> roles)
        {
            var identity = new ClaimsIdentity(
                OpenIdConnectServerDefaults.AuthenticationScheme,
                OpenIdConnectConstants.Claims.Name,
                OpenIdConnectConstants.Claims.Role);

            identity.AddClaim(
                OpenIdConnectConstants.Claims.Subject,
                user.Id.ToString(),
                OpenIdConnectConstants.Destinations.AccessToken);

            if (!string.IsNullOrEmpty(user.Name))
            {
                identity.AddClaim(
                    OpenIdConnectConstants.Claims.Name,
                    user.Name,
                    OpenIdConnectConstants.Destinations.AccessToken);
            }

            if (!string.IsNullOrEmpty(user.Email))
            {
                identity.AddClaim(
                    OpenIdConnectConstants.Claims.Email,
                    user.Email,
                    OpenIdConnectConstants.Destinations.AccessToken);
            }

            if (!string.IsNullOrEmpty(user.PhoneNumber))
            {
                identity.AddClaim(
                    OpenIdConnectConstants.Claims.PhoneNumber,
                    user.PhoneNumber,
                    OpenIdConnectConstants.Destinations.AccessToken);
            }

            if (roles.Count > 0)
            {
                identity.AddClaim(
                    OpenIdConnectConstants.Claims.Role, string.Join(',', roles),
                    OpenIdConnectConstants.Destinations.AccessToken);
            }

            if (user is ManagerUser userManager)
            {
                identity.AddClaim(
                    OpenIdConnectConstants.Scopes.Profile,
                    userManager.ManagerType.GetHashCode().ToString(),
                    OpenIdConnectConstants.Destinations.AccessToken);
            }

            return identity;
        }
    }
}