﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenIddict.Server;
using OpenIddict.Mvc.Internal;
using UIR.Auth.Core.Services;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Exception;

namespace UIR.Auth.Core
{
    public class AuthorizationController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IAuthorizeService _authorizeService;

        public AuthorizationController(UserManager<User> userManager, IAuthorizeService authorizeService)
        {
            _userManager = userManager;
            _authorizeService = authorizeService;
        }

        [HttpPost("~/token"), Produces("application/json"), EnableCors("CorsPolicy")]
        public async Task<IActionResult> Exchange([ModelBinder(typeof(OpenIddictMvcBinder))]
            OpenIdConnectRequest request)
        {
            if (request.IsPasswordGrantType())
            {
                var user = await _userManager.Users.Where(x => x.UserName == request.Username).FirstOrDefaultAsync();
                if (user == null)
                {
                    throw new OperationException("Неправильный логин или пароль.", HttpStatusCode.Forbidden);
                }

                if (!await _userManager.CheckPasswordAsync(user, request.Password))
                {
                    throw new OperationException("Неправильный логин или пароль.", HttpStatusCode.Forbidden);
                }

                var ticket = await _authorizeService.CreateTicketAsync(request, user);
                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }

            if (request.IsRefreshTokenGrantType())
            {
                var info = await HttpContext.AuthenticateAsync(OpenIddictServerDefaults.AuthenticationScheme);
                var user = await _userManager.GetUserAsync(info.Principal);
                if (user == null)
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = "The refresh token is no longer valid."
                    });
                }

                var ticket = await _authorizeService.CreateTicketAsync(request, user, info.Properties);
                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }

            return BadRequest(new OpenIdConnectResponse
            {
                Error = OpenIdConnectConstants.Errors.UnsupportedGrantType,
                ErrorDescription = "The specified grant type is not supported."
            });
        }

        [Authorize, HttpGet("~/api/user/info")]
        public IActionResult GetUserInfo() => Json(new
        {
            Id = User.GetClaim(OpenIdConnectConstants.Claims.Subject),
            Name = User.Identity.Name,
            Roles = User.GetClaim(OpenIdConnectConstants.Claims.Role),
            Email = User.GetClaim(OpenIdConnectConstants.Claims.Email),
            PhoneNumber = User.GetClaim(OpenIdConnectConstants.Claims.PhoneNumber),
            Scope = User.GetClaim(OpenIdConnectConstants.Scopes.Profile)
        });
    }
}