namespace UIR.Public.Api.Modules.Tag.Dto
{
    public class CreateTagDto
    {
        public string Name { get; set; }
    }
}