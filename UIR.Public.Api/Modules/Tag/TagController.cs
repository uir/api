using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Ddd.Pagination;
using Force.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Bl.Shared.Core.Place.Filters;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Controller;
using UIR.Extensions.Core.Extensions;
using UIR.Extensions.Paging;

namespace UIR.Public.Api.Modules.Tag
{
    [Route("api/tag")]
    public class TagController : CrudController
    {
        private readonly IQuery<PlacesFilter, PlacesResponseDto> _placeQuery;

        public TagController(
            DbContext dbContext,
            IQuery<PlacesFilter, PlacesResponseDto> placeQuery)
            : base(dbContext)
        {
            _placeQuery = placeQuery;
        }

        /// <summary>
        /// Возвращает партнеров по тегам
        /// </summary>
        /// <returns></returns>
        [Route("places"), HttpGet]
        public IActionResult GetPartners(string ids, [FromQuery] PlacesQueryOptions options)
        {
            var tagsIds = ids
                .Split(',')
                .Select(long.Parse)
                .ToArray();
            var placesIds = DbContext
                .Set<OrganizationTag>()
                .Where(x => tagsIds.Contains(x.TagId))
                .SelectMany(x => x.Organization.Places.Select(p => p.Id))
                .ToArray();

            return _placeQuery
                .Ask(new PlacesFilter(placesIds, User.GetId(), options))
                .PipeTo(Ok);
        }

        /// <summary>
        /// Возвращает теги
        /// </summary>
        /// <returns></returns>
        [Route("~/api/tags"), HttpGet]
        public IActionResult GetTags(string ids)
        {
            var idsArr = ids.Split(',').Select(long.Parse).ToArray();
            return DbContext
                .Where<Domain.Entities.Tag>(x => idsArr.Contains(x.Id))
                .ProjectTo<TagDto>()
                .PipeTo(Ok);
        }

        /// <summary>
        /// Возвращает теги
        /// </summary>
        /// <returns></returns>
        [Route("tags"), HttpGet]
        public IActionResult GetTags(string name, int page = 1, int take = 10) => DbContext
            .Filter<Domain.Entities.Tag, TagDto>(x => x.Name.Contains(name))
            .OrderBy(x => x.Name)
            .ToPagedEnumerable(new UirPaging(page, take))
            .PipeTo(Ok);
    }
}