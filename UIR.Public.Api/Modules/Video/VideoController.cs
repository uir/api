﻿using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Video.Dto;

namespace UIR.Public.Api.Modules.Video
{
    [Route("api/video")]
    public class VideoController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _options;

        public VideoController(DbContext dbContext, IOptions<AppSettings.AppSettings> options)
        {
            _dbContext = dbContext;
            _options = options;
        }

        [Route("{id}/info"), HttpGet]
        public IActionResult VideoGet(long id)
        {
            var video = _dbContext.FirstById<Domain.Entities.Video, VideoDto>(id);
            video.File = video.File.ToPathVideo(Request.GetUri(), _options.Value.CloudHost);
            return Ok(video);
        }
    }
}