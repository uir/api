﻿using AutoMapper;
using UIR.Public.Api.Modules.Video.Dto;

namespace UIR.Public.Api.Modules.Video
{
    public class VideoProfile : Profile
    {
        public VideoProfile()
        {
            CreateMap<Domain.Entities.Video, VideoDto>();
        }
    }
}