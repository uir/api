﻿using Force.Ddd;

namespace UIR.Public.Api.Modules.Video.Dto
{
    public class VideoDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Discription { get; set; }
        public string File { get; set; }
    }
}