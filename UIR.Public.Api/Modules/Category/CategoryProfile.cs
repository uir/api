﻿using System.Linq;
using AutoMapper;
using UIR.Domain.Entities;
using UIR.Public.Api.Modules.Category.Dto;

namespace UIR.Public.Api.Modules.Category
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Template, TemplateDto>()
                .ForMember(dest => dest.Categories, o => o.MapFrom(s => s.Categories.Select(c => c.Category)));

            CreateMap<Domain.Entities.Category, CategoryDto>();
            CreateMap<Domain.Entities.Category, PlacesListDto>()
                .ForMember(dest => dest.Places,
                    o => o.MapFrom(src => src.Companies.Select(x => x.Organization).SelectMany(c => c.Places)))
                .ForMember(dest => dest.CategoryName, o => o.MapFrom(src => src.Name));
        }
    }
}