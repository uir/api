﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Category.Dto;
using UIR.Public.Api.Modules.Category.Filters;
using UIR.Public.Api.Modules.Category.Queries;

namespace UIR.Public.Api.Modules.Category
{
    [Route("api/category")]
    public class CategoryController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IMemoryCache _memoryCache;
        private readonly IQuery<TemplateSpec, TemplateDto[]> _templateQuery;
        private readonly IQuery<CategoryListSpec, CategoryDto[]> _allCategories;
        private readonly IQuery<GetPlacesByCategoryDto, PlacesListDto> _placesByCategory;

        public CategoryController(
            DbContext dbContext,
            IMemoryCache memoryCache,
            IQuery<TemplateSpec, TemplateDto[]> templateQuery,
            IQuery<CategoryListSpec, CategoryDto[]> allCategories,
            IQuery<GetPlacesByCategoryDto, PlacesListDto> placesByCategory)
        {
            _dbContext = dbContext;
            _memoryCache = memoryCache;
            _templateQuery = templateQuery;
            _allCategories = allCategories;
            _placesByCategory = placesByCategory;
        }

        /// <summary>
        /// Возвращает список категорий
        /// </summary>
        /// <returns></returns>
        [Route("all")]
        [HttpGet]
        public IActionResult GetAllCategries(long? templateId = null) => _allCategories
            .Ask(new CategoryListSpec(templateId))
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает список категорий
        /// </summary>
        /// <returns></returns>
        [Route("~/api/template/{templateId}/categories"), HttpGet]
        public IActionResult GetCategories(long templateId) => _dbContext
            .Where<Template>(t => t.Id == templateId)
            .Include(x => x.Categories)
            .ThenInclude(x => x.Category)
            .Select(t => new
            {
                TemplateIcon = t.Icon,
                TemplateColor = t.Color,
                TemplateName = t.Name,
                Categories = t.Categories.Select(c => new {Id = c.CategoryId, c.Category.Name})
            })
            .FirstOrDefault()
            .Either(x => x != null, Ok, x => (IActionResult) NotFound());

        /// <summary>
        /// Возвращает список мест
        /// </summary>
        /// <returns></returns>
        [Route("~/api/category/{categoryId}/places"), HttpGet]
        public IActionResult GetPartnerByCategory(long categoryId, [FromQuery] PlacesQueryOptions options) => _placesByCategory
            .Ask(new GetPlacesByCategoryDto(
                HttpContext.Request.GetUri(),
                HttpContext.User.GetId(),
                categoryId,
                options))
            .Either(x => x != null, Ok, x => (IActionResult) NotFound());

        /// <summary>
        /// Возвращает список маркеров
        /// </summary>
        /// <returns></returns>
        [Route("~/api/category/{categoryId}/markers"), HttpGet]
        public IActionResult GetMarkers(long categoryId) => _dbContext
            .Set<CategoryOrganization>()
            .Where(x => x.CategoryId == categoryId)
            .SelectMany(x => x.Organization.Places)
            .Select(x => new
            {
                x.Id,
                x.Location.Latitude,
                x.Location.Longitude
            })
            .ToArray()
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает список рубрик
        /// </summary>
        /// <returns></returns>
        [Route("templates")]
        [HttpGet]
        public IActionResult GetTemplates() => _templateQuery
            .Ask(new TemplateSpec())
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает список рубрик
        /// </summary>
        /// <returns></returns>
        [Route("~/api/templates/categories")]
        [HttpGet]
        public IActionResult GetTemplatesWithCategories()
        {
            const string key = "templates/categories";
            IEnumerable<TemplateDto> templatesCache;
            if (!_memoryCache.TryGetValue(key, out templatesCache))
            {
                templatesCache = _dbContext
                    .Set<Template>()
                    .ProjectTo<TemplateDto>()
                    .ToArray();
                _memoryCache.Set(key, templatesCache);
            }
            return Ok(templatesCache);
        }
    }
}