﻿using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Bl.Shared.Core.Base.Queries;
using UIR.Domain.Entities;
using UIR.Public.Api.Modules.Category.Dto;
using UIR.Public.Api.Modules.Category.Filters;
using UIR.Public.Api.Modules.Category.Queries;

namespace UIR.Public.Api.Modules.Category
{
    public class CategoryModule 
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IQuery<TemplateSpec, TemplateDto[]>, BaseQuery<Template, TemplateDto>>();
            services.AddScoped<IQuery<CategoryListSpec, CategoryDto[]>, BaseQuery<Domain.Entities.Category, CategoryDto>>();
            services.AddScoped<IQuery<GetPlacesByCategoryDto, PlacesListDto>, GetPlacesByCategory>();
        }
    }
}