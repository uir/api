﻿using System;
using System.Collections.Generic;
using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Bl.Shared.Core.Place.Filters;
using UIR.Extensions.Core.Exception;
using UIR.Public.Api.Modules.Category.Dto;

namespace UIR.Public.Api.Modules.Category.Queries
{
    public class GetPlacesByCategoryDto
    {
        public GetPlacesByCategoryDto(Uri uri, long userId, long categoryId, PlacesQueryOptions options)
        {
            Uri = uri;
            UserId = userId;
            CategoryId = categoryId;
            Options = options;
        }

        public PlacesQueryOptions Options { get; set; }
        public Uri Uri { get; set; }
        public long UserId { get; set; }
        public long CategoryId { get; set; }
    }

    public class GetPlacesByCategory : IQuery<GetPlacesByCategoryDto, PlacesListDto>
    {
        private readonly DbContext _dbContext;
        private readonly IQuery<PlacesFilter, PlacesResponseDto> _placeQuery;

        public GetPlacesByCategory(DbContext dbContext, IQuery<PlacesFilter, PlacesResponseDto> placeQuery)
        {
            _dbContext = dbContext;
            _placeQuery = placeQuery;
        }

        public PlacesListDto Ask(GetPlacesByCategoryDto dto)
        {
            var data = _dbContext
                .Set<Domain.Entities.Category>()
                .Where(x => x.Id == dto.CategoryId)
                .Select(x => new
                {
                    CateroryName = x.Name,
                    PlacesIds = x
                        .Companies
                        .Select(o => o.Organization)
                        .SelectMany(o => o.Places)
                        .Select(p => p.Id)
                })
                .FirstOrDefault();

            if (data == null)
                throw new OperationException("Категория не найдена");

            var places = _placeQuery
                .Ask(new PlacesFilter(data.PlacesIds.ToArray(), dto.UserId, dto.Options));
            return new PlacesListDto(data.CateroryName, places);
        }
    }
}