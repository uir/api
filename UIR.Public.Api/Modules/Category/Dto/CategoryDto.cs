﻿namespace UIR.Public.Api.Modules.Category.Dto
{
    public class CategoryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}