﻿﻿using Force.Ddd;

namespace UIR.Public.Api.Modules.Category.Dto
{
    public class CategoryTagDto : HasIdBase<long>
    {
        public string Name { get; set; }
    }
}