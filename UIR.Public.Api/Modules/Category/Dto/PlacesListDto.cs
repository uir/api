﻿using UIR.Bl.Shared.Core.Place.Dto;

namespace UIR.Public.Api.Modules.Category.Dto
{
    public class PlacesListDto : PlacesResponseDto
    {
        public string CategoryName { get; }

        public PlacesListDto(string categoryName, PlacesResponseDto placesResponse)
            : base(placesResponse.PlacesCount, placesResponse.Places, placesResponse.Markers)
        {
            CategoryName = categoryName;
        }
    }
}