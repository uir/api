﻿using System.Collections.Generic;

namespace UIR.Public.Api.Modules.Category.Dto
{
    public class TemplateDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }

        public IEnumerable<CategoryDto> Categories { get; set; }
    }
}