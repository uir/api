﻿using System.Linq;
using Force.Ddd;
using UIR.Domain.Entities;

namespace UIR.Public.Api.Modules.Category.Filters
{
    public class TemplateSpec : IQueryableFilter<Template>, IQueryableOrder<Template>
    {
        public IQueryable<Template> Apply(IQueryable<Template> query) => query;

        public IOrderedQueryable<Template> OrderBy(IQueryable<Template> queryable) =>
            queryable.OrderBy(x => x.Name);
    }
}