﻿using System.Linq;
using Force.Ddd;
using Force.Extensions;

namespace UIR.Public.Api.Modules.Category.Filters
{
    public class CategoryListSpec : IQueryableFilter<Domain.Entities.Category>, IQueryableOrder<Domain.Entities.Category>
    {
        public CategoryListSpec(long? templateId)
        {
            TemplateId = templateId;
        }

        public long? TemplateId { get; set; }

        public IQueryable<Domain.Entities.Category> Apply(IQueryable<Domain.Entities.Category> query) =>
            query.WhereIf(TemplateId.HasValue, x => x.Templates.Any(y => y.TemplateId == TemplateId.Value));

        public IOrderedQueryable<Domain.Entities.Category> OrderBy(IQueryable<Domain.Entities.Category> queryable) =>
            queryable.OrderBy(x => x.Name);
    }
}