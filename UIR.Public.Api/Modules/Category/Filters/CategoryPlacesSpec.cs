﻿using System.Linq;
using Force.Ddd;
using Microsoft.EntityFrameworkCore;

namespace UIR.Public.Api.Modules.Category.Filters
{
    public class CategoryPlacesSpec : IQueryableFilter<Domain.Entities.Place>, IQueryableOrder<Domain.Entities.Place>
    {
        public CategoryPlacesSpec(long categoryId)
        {
            CategoryId = categoryId;
        }

        public long CategoryId { get; set; }

        public IQueryable<Domain.Entities.Place> Apply(IQueryable<Domain.Entities.Place> query) =>
            query.Where(x => x.Organization.Categories.Any(c => c.CategoryId == CategoryId))
                .Include(x => x.Organization)
                .ThenInclude(x => x.Categories);

        public IOrderedQueryable<Domain.Entities.Place> OrderBy(IQueryable<Domain.Entities.Place> queryable) =>
            queryable.OrderByDescending(x => x.Rating);
    }
}