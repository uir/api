﻿using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Public.Api.Modules.Place.Commands;
using UIR.Public.Api.Modules.Place.Dto.Request;

namespace UIR.Public.Api.Modules.Place
{
    public class PlaceModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IHandler<CreateOverviewDto, long>, CreateOverviewCommand>();
        }
    }
}