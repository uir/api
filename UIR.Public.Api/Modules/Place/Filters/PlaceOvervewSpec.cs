﻿using System.Linq;
using Force.Ddd;
using UIR.Domain.Entities;

namespace UIR.Public.Api.Modules.Place.Filters
{
    public class PlaceOverviewSpec: IQueryableFilter<Overview>
    {
        public long PlaceId { get; set; }
        public long OverviewId { get; set; }

        public PlaceOverviewSpec(long placeId, long overviewId)
        {
            PlaceId = placeId;
            OverviewId = overviewId;
        }


        public IQueryable<Overview> Apply(IQueryable<Overview> query) => query
            .Where(x => x.PlaceId == PlaceId && x.IsConfirmed && x.Id ==  OverviewId);
    }
}