﻿using System.Linq;
using Force.Ddd;
using Microsoft.EntityFrameworkCore;

namespace UIR.Public.Api.Modules.Place.Filters
{
    public class PlaceUsersSearchSpec: IQueryableFilter<Domain.Entities.UserSetUp.User>
    {
        public string Search { get; }
        public long[] SalesIds { get; }

        public PlaceUsersSearchSpec(string search, long[] salesIds)
        {
            Search = search;
            SalesIds = salesIds;
        }

        public IQueryable<Domain.Entities.UserSetUp.User> Apply(IQueryable<Domain.Entities.UserSetUp.User> query) => query
            .Where(x => x.Email.Contains(Search) || x.Name.Contains(Search) || x.PhoneNumber.Contains(Search))
            .Where(x => x.Sales.Any(s => SalesIds.Contains(s.Sale.Id) && s.IsActive))
            .Include(x => x.Sales);
    }
}