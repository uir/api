﻿using System.Linq;
using Force.Ddd;
using UIR.Domain.Entities;

namespace UIR.Public.Api.Modules.Place.Filters
{
    public class PlaceOvervewsListSpec: IQueryableFilter<Overview>
    {
        public long PlaceId { get; set; }


        public PlaceOvervewsListSpec(long placeId)
        {
            PlaceId = placeId;
        }

        public IQueryable<Domain.Entities.Overview> Apply(IQueryable<Overview> query) => query
            .Where(x => x.PlaceId == PlaceId && x.IsConfirmed );
    }
}