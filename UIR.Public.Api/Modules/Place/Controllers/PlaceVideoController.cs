﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Video.Dto;

namespace UIR.Public.Api.Modules.Place.Controllers
{
    public class PlaceVideoController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _options;

        public PlaceVideoController(DbContext dbContext, IOptions<AppSettings.AppSettings> options)
        {
            _dbContext = dbContext;
            _options = options;
        }

        /// <summary>
        /// Возвращает видео
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/videos"), HttpGet]
        public IActionResult GetVideos(long id) => _dbContext
            .Where<VideoPlace>(x => x.PlaceId == id)
            .Select(x => x.Video)
            .ProjectTo<VideoDto>()
            .ToArray()
            .Each(x => x.File = x.File.ToString().ToPathVideo(Request.GetUri(), _options.Value.CloudHost))
            .ToArray()
            .PipeTo(Ok);
    }
}