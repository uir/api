﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;

namespace UIR.Public.Api.Modules.Place.Controllers
{
    public class PlaceSalesController : Controller
    {
        private readonly DbContext _dbContext;

        public PlaceSalesController(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Возвращает все скидки филиала
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/sales"), HttpGet]
        public IActionResult GetAllSales(long id)
        {
            var companyId = _dbContext
                .Set<Domain.Entities.Place>()
                .ById(id)
                .OrganizationId
                .ThrowIfDefault();
            return _dbContext
                .Set<Sale>()
                .Where(SaleSpec.All(id, companyId) & SaleSpec.Active)
                .ProjectTo<SaleDto>()
                .OrderByDescending(x => x.IsExclusive)
                .ToArray()
                .PipeTo(Ok);
        }
    }
}