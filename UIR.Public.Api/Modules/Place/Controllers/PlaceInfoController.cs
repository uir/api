﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Place.Dto;

namespace UIR.Public.Api.Modules.Place.Controllers
{
    public class PlaceInfoController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _options;

        public PlaceInfoController(DbContext dbContext, IOptions<AppSettings.AppSettings> options)
        {
            _dbContext = dbContext;
            _options = options;
        }

        /// <summary>
        /// Возвращает данные о месте
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{placeId}/meta"), HttpGet]
        public IActionResult GetMeta(long placeId) => _dbContext
            .Set<Domain.Entities.Place>()
            .Where(PlaceSpec.ById(placeId) & PlaceSpec.Active)
            .Include(x => x.Overviews)
            .Include(x => x.Images)
            .Include(x => x.Videos)
            .Include(x => x.Sales)
            .Include(x => x.Organization)
            .ThenInclude(x => x.Sales)
            .Select(x => new
            {
                x.Name,
                HasImages = x.Images.Any(),
                HasVideos = x.Videos.Any(),
                HasOverviews = x.Overviews.Any(),
                HasSales = x.Sales.Any() || x.Organization.Sales.Any()
            })
            .FirstOrDefault()
            .Either(x => x != null, Ok, x => (IActionResult) NotFound());

        /// <summary>
        /// Возвращает информацию о месте
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{placeId}/info"), HttpGet]
        public IActionResult GetFullInfo(long placeId) => _dbContext
            .Set<Domain.Entities.Place>()
            .Where(PlaceSpec.ById(placeId) & PlaceSpec.Active)
            .ProjectTo<PlaceFullInfo>()
            .ToArray()
            .Each(x => x.Logo = x.Logo.ToPathImg(Request.GetUri(), _options.Value.CloudHost))
            .FirstOrDefault()
            .Either(x => x != null, Ok, x => (IActionResult) NotFound());
    }
}