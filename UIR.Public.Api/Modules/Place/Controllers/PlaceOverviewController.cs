﻿using System;
using AutoMapper;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Controller;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Place.Dto;
using UIR.Public.Api.Modules.Place.Dto.Request;
using UIR.Public.Api.Modules.Place.Filters;

namespace UIR.Public.Api.Modules.Place.Controllers
{
    public class PlaceOverviewController : CrudController
    {
        private readonly IHandler<CreateOverviewDto, long> _createCreateCommand;

        public PlaceOverviewController(DbContext dbContext,
          IHandler<CreateOverviewDto, long> createCreateCommand) : base(dbContext)
        {
            _createCreateCommand = createCreateCommand;
        }
        
        /// <summary>
        /// Возвращает обзоры по компании
        /// </summary>
        /// <returns></returns>
        [Route("api/places/{id}/overviews"), HttpGet]
        public IActionResult Overviews(long id) => 
            DbContext.FilterToArray<Overview, OverviewListDto>(new PlaceOvervewsListSpec(id)).PipeTo(Ok);
        
        /// <summary>
        /// Возвращает обзор
        /// </summary>
        /// <returns></returns>
        [Route("api/places/{id}/overviews/{overviewId}"), HttpGet]
        public IActionResult Get(long id, long overviewId) =>             
            DbContext.FilterToArray<Overview, OverviewDto>(new PlaceOverviewSpec(id, overviewId)).PipeTo(Ok);

        
        /// <summary>
        /// Создание обзора
        /// </summary>
        /// <returns></returns>
        [HttpPost, Authorize(Roles = "User")]
        [Route("api/places/{id}/overviews")]
        public IActionResult Create([FromBody] CreateOverviewDto dto) => _createCreateCommand.Handle(dto).PipeTo(x => Ok(x));
        
        /// <summary>
        /// Редактирование обзора
        /// </summary>
        /// <returns></returns>
        [HttpPut, Authorize(Roles = "User")]
        [Route("api/places/{id}/overviews/{overviewId}")]
        public IActionResult Edit(EditOverviewDto dto)
        {
            var review = DbContext.GetById<Overview>(dto.Id);
            if(review.User.Id != dto.UserId)
                throw new OperationException("Обзор не принадлежит пользователю.");
            if(review.Place.Id != dto.PlaceId)
                throw new OperationException("Обзор не принадлежит компании.");
            var newObj = Mapper.Map(dto, review);
            newObj.CreateDate = DateTime.Now;
            newObj.IsConfirmed = true;
            DbContext.SaveChanges();
            return Ok(newObj.Id);
        }

        /// <summary>
        /// Проверка прав
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize(Roles = "User")]
        [Route("api/overviews/{overviewId}/permission/{userId}")]
        public IActionResult Permission(long overviewId, long userId)
        {
            var overview = DbContext.GetById<Overview>(overviewId);
            return Ok(overview.UserId == userId);
        }
    }
}