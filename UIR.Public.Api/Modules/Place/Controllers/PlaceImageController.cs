﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Place.Dto;

namespace UIR.Public.Api.Modules.Place.Controllers
{
    public class PlaceImageController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _options;

        public PlaceImageController(DbContext dbContext, IOptions<AppSettings.AppSettings> options)
        {
            _dbContext = dbContext;
            _options = options;
        }

        /// <summary>
        /// Возвращает изображения партнера
        /// </summary>
        /// <returns></returns>
        [Route("~/api/places/{id}/images"), HttpGet]
        public IActionResult GetImages(long id)
        {
            var res = _dbContext
                .Where<ImagePlace>(x => x.PlaceId == id)
                .Select(x => x.Image)
                .ProjectTo<ImageDto>()
                .ToArray()
                .Each(x => x.File = x.File.ToPath(Request.GetUri(), _options.Value.CloudHost))
                .ToArray();
            return Ok(res);
        }
    }
}