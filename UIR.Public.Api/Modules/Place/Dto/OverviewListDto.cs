﻿using UIR.Domain.Enums;

namespace UIR.Public.Api.Modules.Place.Dto
{
    public class OverviewListDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string CreateDate { get; set; }
        public Rating Rating { get; set; }
        public string UserName { get; set; }
        public long UserId { get; set; }
        public string Plus { get; set; }
        public string Minus { get; set; }
    }
}