﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Enums;

namespace UIR.Public.Api.Modules.Place.Dto
{
    public class PlaceFullInfo : HasIdBase<long>
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Phone { get; set; }
        public string Vicinity { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public PriceLevel PriceLevel { get; set; }
        public string Logo { get; set; }
        public List<string> Categories { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public IEnumerable<TagDto> Tags { get; set; }
    }
}