﻿namespace UIR.Public.Api.Modules.Place.Dto
{
    public class OverviewDto : OverviewListDto
    {
        public string Description { get; set; }
        public string PlaceId { get; set; }
    }
}