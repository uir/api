using UIR.Domain.Enums;

namespace UIR.Public.Api.Modules.Place.Dto.Request
{
    public class CreateOverviewDto
    {
        public Rating Rating { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public long UserId { get; set; }
        public long PlaceId { get; set; }
        public string Plus { get; set; }
        public string Minus { get; set; }
    }
}