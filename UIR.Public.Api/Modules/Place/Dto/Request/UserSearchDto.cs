﻿namespace UIR.Public.Api.Modules.Place.Dto.Request
{
    public class UserSearchDto
    {
        public UserSearchDto(string search, long placeId)
        {
            Search = search;
            PlaceId = placeId;
        }

        public string Search { get; set; }
        public long PlaceId { get; set; }
    }
}