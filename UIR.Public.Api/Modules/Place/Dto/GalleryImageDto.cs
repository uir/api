﻿namespace UIR.Public.Api.Modules.Place.Dto
{
    public class GalleryDto
    {
        public GalleryImageDto Preview_xxs { get; set; }
        public GalleryImageDto Preview_xs { get; set; }
        public GalleryImageDto preview_s { get; set; }
        public GalleryImageDto Preview_m { get; set; }
        public GalleryImageDto Preview_l { get; set; }
        public GalleryImageDto Preview_xl { get; set; }
        public GalleryImageDto Raw { get; set; }
    }

    public class GalleryImageDto
    {
        public string Path { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}