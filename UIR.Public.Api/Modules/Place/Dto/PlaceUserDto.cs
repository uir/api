﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Bl.Shared.Core.Base.Dto;

namespace UIR.Public.Api.Modules.Place.Dto
{
    public class PlaceUserDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public IEnumerable<SaleDto> Sales { get; set; }
    }
}