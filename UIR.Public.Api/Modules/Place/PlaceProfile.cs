﻿using System.Linq;
using AutoMapper;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Place.Dto;
using UIR.Public.Api.Modules.Place.Dto.Request;

namespace UIR.Public.Api.Modules.Place
{
    public class PlaceProfile : Profile
    {
        public PlaceProfile()
        {
            CreateMap<Image, ImageDto>();

            CreateMap<Image, ImageDto>()
                .ForMember(dest => dest.File, opts => opts.MapFrom(src => @"img/" + src.File));

            CreateMap<Image, GalleryImageDto>()
                .ForMember(dest => dest.Path, opts => opts.MapFrom(src => @"img/" + src.File));

            CreateMap<Sale, SaleDto>();

            CreateMap<Period, WorkPeriodDto>();

            CreateMap<Domain.Entities.Video, VideoDto>();

            CreateMap<OrganizationTag, TagDto>()
                .ForMember(dest => dest.Name, o => o.MapFrom(tag => tag.Tag.Name))
                .ForMember(dest => dest.Id, o => o.MapFrom(tag => tag.TagId));

            CreateMap<Domain.Entities.Place, PlaceFullInfo>()
                .ForMember(dest => dest.Tags, o => o.MapFrom(x => x.Organization.Tags))
                .ForMember(dest => dest.Lat, o => o.MapFrom(x => x.Location.Latitude))
                .ForMember(dest => dest.Lng, o => o.MapFrom(x => x.Location.Longitude))
                .ForMember(dest => dest.Categories, o => o.MapFrom(x => x.Organization.Categories.Select(c => c.Category.Name)))
                .ForMember(dest => dest.Logo, o => o.MapFrom(PlaceFormatter.Logo));

            CreateMap<Domain.Entities.UserSetUp.User, PlaceUserDto>()
                .ForMember(dest => dest.Sales,
                    opts => opts.MapFrom(src => src
                        .Sales
                        .Where(sale => sale.IsActive)
                        .Select(sale => sale.Sale)))
                .ForMember(dest => dest.Phone, opts => opts.MapFrom(src => src.PhoneNumber));

            CreateMap<Overview, OverviewDto>()
                .ForMember(dest => dest.CreateDate,
                    opts => opts.MapFrom(src =>
                        src.CreateDate.Day + "." + src.CreateDate.Month + "." + src.CreateDate.Year));
            CreateMap<Overview, OverviewListDto>()
                .ForMember(dest => dest.CreateDate,
                    opts => opts.MapFrom(src =>
                        src.CreateDate.Day + "." + src.CreateDate.Month + "." + src.CreateDate.Year));
            CreateMap<Overview, CreateOverviewDto>();
            CreateMap<Overview[], OverviewListDto[]>();
            CreateMap<CreateOverviewDto, Overview>();
            CreateMap<EditOverviewDto, Overview>();
            CreateMap<OverviewDto, Overview>();
        }
    }
}