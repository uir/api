using System;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Place.Dto.Request;

namespace UIR.Public.Api.Modules.Place.Commands
{
    public class CreateOverviewCommand : IHandler<CreateOverviewDto, long>
    {
        private readonly DbContext _dbContext;

        public CreateOverviewCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public long Handle(CreateOverviewDto input)
        {
            var review = input.Map<Domain.Entities.Overview>();
            review.CreateDate = DateTime.Now;
            review.Place = _dbContext.GetById<Domain.Entities.Place>(input.PlaceId);
            review.User = _dbContext.GetById<Domain.Entities.UserSetUp.User>(input.UserId);
            review.IsConfirmed = true;
            _dbContext.Add(review);
            _dbContext.SaveChanges();
            return review.Id;
        }
    }
}