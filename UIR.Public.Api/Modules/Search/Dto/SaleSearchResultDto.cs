using UIR.Domain.Enums;

namespace UIR.Public.Api.Modules.Search.Dto
{
    public class SaleSearchResultDto : SearchResultDto
    {
        public double Amount { get; set; }
        public string OrganizationName { get; set; }
        public long PlaceId { get; set; }
        public bool IsExclusive { get; set; }
        public DiscountType DiscountType { get; set; }
    }
}