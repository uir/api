using UIR.Domain.Interfaces;

namespace UIR.Public.Api.Modules.Search.Dto
{
    public class PlaceSearchResultDto : SearchResultDto, IHasLogo
    {
        public string Logo { get; set; }
        public string Address { get; set; }
    }
}