﻿using UIR.Domain.Entities.Base;

namespace UIR.Public.Api.Modules.Search.Dto
{
    public class SearchResultDto : IHasName
    {
        public string Name { get; set; }
        public long Id { get; set;}
    }
}