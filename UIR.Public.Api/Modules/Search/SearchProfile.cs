using AutoMapper;
using UIR.Domain.Entities;
using UIR.Domain.Entities.Base;
using UIR.Public.Api.Modules.Search.Dto;

namespace UIR.Public.Api.Modules.Search
{
    public class SearchProfile: Profile
    {
        public SearchProfile()
        {
            CreateMap<Domain.Entities.Category, SearchResultDto>();
            
            CreateMap<Domain.Entities.Tag, SearchResultDto>();
            
            CreateMap<Domain.Entities.Place, PlaceSearchResultDto>()
                .ForMember(d => d.Logo, o => o.MapFrom(PlaceFormatter.Logo));
            
            CreateMap<Sale, SaleSearchResultDto>()
                .ForMember(d => d.PlaceId, o => o.MapFrom(SaleFormatter.PlaceId))
                .ForMember(d => d.OrganizationName, o => o.MapFrom(SaleFormatter.OrganizationName));
        }
    }
}