﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using Force.Ddd;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Bl.Shared.Core;
using UIR.Domain.Entities;
using UIR.Domain.Entities.Base;
using UIR.Domain.Entities.Base.Specs;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Search.Dto;

namespace UIR.Public.Api.Modules.Search
{
    public class SearchRequest
    {
        public string Text { get; set; }
    }

    public class SearchController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _options;

        public SearchController(DbContext dbContext, IOptions<AppSettings.AppSettings> options)
        {
            _dbContext = dbContext;
            _options = options;
        }

        /// <summary>
        /// Возвращает результат поиска
        /// </summary>
        /// <returns></returns>
        [Route("~/api/search"), HttpGet]
        public IActionResult Search(SearchRequest request)
        {
            if (string.IsNullOrEmpty(request.Text))
                return Ok();
            var tags = GetSearchResult<Domain.Entities.Tag, SearchResultDto>(request);
            var places =
                GetSearchResult<Domain.Entities.Place, PlaceSearchResultDto>(request, PlaceSpec.Search(request.Text) & PlaceSpec.Active)
                    .SetPath(Request.GetUri(), _options);
            var categories = GetSearchResult<Domain.Entities.Category, SearchResultDto>(request);
            var sales = GetSearchResult<Sale, SaleSearchResultDto>(request, SaleSpec.Search(request.Text),
                s => s.OrganizationName);
            return Ok(new
            {
                tags,
                places,
                categories,
                sales
            });
        }

        private TDto[] GetSearchResult<T, TDto>(SearchRequest request, ForceSpec<T> searchSpec = null,
            Func<TDto, string> orderBy = null)
            where T : HasIdBase<long>, IHasName where TDto : class, IHasName => _dbContext
            .Set<T>()
            .Where(searchSpec ?? DomainSpec.SearchByName<T>(request.Text))
            .ProjectTo<TDto>()
            .Take(10)
            .ToArray()
            .OrderBy(orderBy ?? (x => x.Name))
            .ToArray();
    }
}