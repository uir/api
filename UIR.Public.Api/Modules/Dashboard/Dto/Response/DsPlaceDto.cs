namespace UIR.Public.Api.Modules.Dashboard.Dto.Response
{
    public class DsPlaceDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string[] Categories { get; set; }
    }
}