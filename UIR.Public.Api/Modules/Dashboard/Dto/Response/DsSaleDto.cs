using UIR.Domain.Enums;

namespace UIR.Public.Api.Modules.Dashboard.Dto.Response
{
    public class DsSaleDto
    {
        public string Name { get; set; }
        public string OrganizationName { get; set; }
        public double Amount { get; set; }
        public long PlaceId { get; set; }
        public bool IsExclusive { get; set; }
        public DiscountType Type { get; set; }
    }
}