using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.Dashboard.Dto.Response;

namespace UIR.Public.Api.Modules.Dashboard
{
    public class DashboardController : Controller
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _settings;

        public DashboardController(
            DbContext dbContext,
            IOptions<AppSettings.AppSettings> settings
        )
        {
            _dbContext = dbContext;
            _settings = settings;
        }

        /// <summary>
        /// Возвращает закладки пользователя
        /// </summary>
        /// <returns></returns>
        [Route("~/api/dashboard/places/bookmarks"), HttpGet]
        public IActionResult GetBookmarks() => _dbContext
            .Where<Bookmarks>(x => x.UserId == User.GetId())
            .Take(10)
            .Select(x => x.Place)
            .Where(PlaceSpec.Active)
            .ProjectTo<DsPlaceDto>()
            .OrderBy(x => x.Name)
            .ToArray()
            .Each(x => x.Logo = x.Logo.ToPathImg(Request.GetUri(), _settings.Value.CloudHost))
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает новые места
        /// </summary>
        /// <returns></returns>
        [Route("~/api/dashboard/places"), HttpGet]
        public IActionResult GetNewPlaces() => _dbContext
            .Set<Domain.Entities.Place>()
            .Where(PlaceSpec.Active)
            .OrderByDescending(x => x.CreateDate)
            .ThenBy(x => x.Name)
            .Take(10)
            .ProjectTo<DsPlaceDto>()
            .ToArray()
            .Each(x => x.Logo = x.Logo.ToPathImg(Request.GetUri(), _settings.Value.CloudHost))
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает новые скидки
        /// </summary>
        /// <returns></returns>
        [Route("~/api/dashboard/sales"), HttpGet]
        public IActionResult GetNewSales() => _dbContext
            .Set<Sale>()
            .Where(SaleSpec.Active)
            .OrderByDescending(x => x.Id)
            .Take(10)
            .ProjectTo<DsSaleDto>()
            .ToArray()
            .PipeTo(Ok);

        /// <summary>
        /// Возвращает недавние скидки
        /// </summary>
        /// <returns></returns>
        [Route("~/api/dashboard/sales/recent"), HttpGet]
        public IActionResult GetRecentPlaces() => _dbContext
            .Where<UserSale>(x => x.UserId == User.GetId())
            .OrderByDescending(x => x.CreateDateTime)
            .Take(10)
            .Select(x => x.Sale)
            .ProjectTo<DsSaleDto>()
            .ToArray()
            .PipeTo(Ok);
    }
}