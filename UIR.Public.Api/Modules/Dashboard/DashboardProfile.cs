using System.Linq;
using AutoMapper;
using UIR.Domain.Entities;
using UIR.Public.Api.Modules.Dashboard.Dto.Response;

namespace UIR.Public.Api.Modules.Dashboard
{
    public class DashboardProfile : Profile
    {
        public DashboardProfile()
        {
            CreateMap<Domain.Entities.Place, DsPlaceDto>()
                .ForMember(dest => dest.Categories,
                    o => o.MapFrom(x => x.Organization.Categories.Select(c => c.Category.Name)))
                .ForMember(dest => dest.Logo, o => o.MapFrom(PlaceFormatter.Logo))
                .ForMember(dest => dest.Name, o => o.MapFrom(PlaceFormatter.Name));

            CreateMap<Sale, DsSaleDto>()
                .ForMember(dest => dest.Type, o => o.MapFrom(s => s.DiscountType))
                .ForMember(dest => dest.PlaceId, o => o.MapFrom(SaleFormatter.PlaceId))
                .ForMember(dest => dest.OrganizationName, o => o.MapFrom(SaleFormatter.OrganizationName));
        }
    }
}