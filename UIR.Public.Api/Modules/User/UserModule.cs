﻿﻿using Force.Cqrs;
using Microsoft.Extensions.DependencyInjection;
using UIR.Public.Api.Modules.User.Commands;
using UIR.Public.Api.Modules.User.Commands.ChangePassword;
using UIR.Public.Api.Modules.User.Commands.UserReg;
using UIR.Public.Api.Modules.User.Dto;
using UIR.Public.Api.Modules.User.Dto.Request;
using UIR.Public.Api.Modules.User.Queries;

namespace UIR.Public.Api.Modules.User
{
    public class UserModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IAsyncHandler<AuthDto, long>, RegisterCommand>();
            services.AddScoped<IAsyncHandler<ConfirmDto>, ConfirmCommnad>();
            services.AddScoped<IAsyncHandler<ChangePasswordRequestDto>, ChangePasswordRequestCommand>();
            services.AddScoped<IAsyncHandler<ChangePasswordConfirmDto>, ChangePasswordConfirmCommand>();
            services.AddScoped<IHandler<ReferalCreateCommandDto, string>, CreateRefCommand>();
            services.AddScoped<IQuery<string, ReferalDto>, GetReferalQuery>();
            services.AddScoped<IQuery<StatisticQueryDto, UserRefLineDto[]>, StatisticQuery>();
            services.AddScoped<IQuery<ChartQueryDto, UserRefChartDto>, ChartQuery>();
            services.AddScoped<IQuery<TariffQueryDto, UserInfoTariffDto>, TariffsQuery>();
            services.AddScoped<IQuery<InfoQueryDto, UserInfoDto>, UserInfoQuery>();
            services.AddScoped<IHandler<UserInfoDto, UserInfoDto>, EditUserInfoCommand>();
        }
    }
}