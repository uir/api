﻿﻿using AutoMapper;
 using UIR.Domain.Entities;
 using UIR.Domain.Entities.UserSetUp;
 using UIR.Domain.PaySystem.Entities;
 using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Modules.User
{
    public class UserProfile
    {
        public class CategoryProfile : Profile
        {
            public CategoryProfile()
            {
                CreateMap<Referal, ReferalDto>();

                CreateMap<Domain.Entities.UserSetUp.User, UserInfoDto>()
                    .ForMember(dest => dest.TariffName ,opts => opts.Ignore())
                    .ForMember(dest => dest.TariffId,opts => opts.Ignore())
                    .ForMember(dest => dest.EndDay ,opts => opts.Ignore());
                    
                CreateMap<PublicUser, UserInfoDto>()
                    .ForMember(dest => dest.PayPalEmail ,opts => opts.MapFrom(d => d.MassPayAccount))
                    .ForMember(dest => dest.TariffName ,opts => opts.Ignore())
                    .ForMember(dest => dest.TariffId,opts => opts.Ignore())
                    .ForMember(dest => dest.EndDay ,opts => opts.Ignore());

                CreateMap<UserInfoDto, PublicUser>()
                    .ForMember(dest => dest.MassPayAccount ,opts => opts.MapFrom(d => d.PayPalEmail));

                CreateMap<Sale, RecentSaleDto>();

                CreateMap<AuthDto, Domain.Entities.UserSetUp.User>()
                    .ForMember(dest => dest.PhoneNumber, opts => opts.MapFrom(src => src.Phone))
                    .ForMember(dest => dest.ParentId, opts => opts.Ignore())
                    .ForMember(dest => dest.UserName, opts => opts.MapFrom(src => src.Phone));
                
                
                CreateMap<AuthDto, PublicUser>()
                    .ForMember(dest => dest.PhoneNumber, opts => opts.MapFrom(src => src.Phone))
                    .ForMember(dest => dest.ParentId, opts => opts.Ignore())
                    .ForMember(dest => dest.UserName, opts => opts.MapFrom(src => src.Phone));
                
                CreateMap<PaymentPlan, UserTariffDto>()
                    .ForMember(dest => dest.IsActive, opts => opts.MapFrom(src => false))
                    .ForMember(dest => dest.Lines, opts =>  opts.Ignore())
                    .ForMember(dest => dest.Price, opts =>  opts.MapFrom(src => src.Value))
                    .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name));

            }
        }
    }
}