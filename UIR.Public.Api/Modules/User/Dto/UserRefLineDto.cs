﻿namespace UIR.Public.Api.Modules.User.Dto
{
    public class UserRefLineDto
    {
        public int All { get; set; }
        public int Pay { get; set; }
        public int Profit { get; set; }
    }
}