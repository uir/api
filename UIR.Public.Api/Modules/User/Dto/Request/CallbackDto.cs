﻿namespace UIR.Public.Api.Modules.User.Dto.Request
{
    public class CallbackDto
    {
        public string Email { get; set; }
        public string Message { get; set; }
    }
}