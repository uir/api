using System.ComponentModel.DataAnnotations;

namespace UIR.Public.Api.Modules.User.Dto.Request
{
    public class FeedbackDto
    {
        [Required] public string Text { get; set; }
        [Required] public string Url { get; set; }
        [Required] public string Platform { get; set; }
        [Required] public string ScreenWidth { get; set; }
        [Required] public string ScreenHeight { get; set; }
        [Required] public string UserAgent { get; set; }
    }
}