﻿namespace UIR.Public.Api.Modules.User.Dto.Request
{
    public class ChangePasswordConfirmDto: ConfirmDto
    {
        public string Password { get; set; }
    }
}