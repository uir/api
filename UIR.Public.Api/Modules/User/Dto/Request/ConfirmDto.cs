﻿using System.ComponentModel.DataAnnotations;

namespace UIR.Public.Api.Modules.User.Dto.Request
{
    public class ConfirmDto
    {
        [Required]
        public string Phone { get; set; }
        
        [Required]
        public string Code { get; set; }
    }
}