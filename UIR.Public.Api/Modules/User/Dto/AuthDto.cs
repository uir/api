﻿using System.ComponentModel.DataAnnotations;

namespace UIR.Public.Api.Modules.User.Dto
{
    public class AuthDto
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Phone { get; set; }
        
        public long? ParentId { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}