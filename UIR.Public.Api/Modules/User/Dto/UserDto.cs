﻿﻿namespace UIR.Public.Api.Modules.User.Dto
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class UserInfoDto : UserDto
    {
        public long Id { get; set; }
        public long? TariffId { get; set; }
        public string TariffName { get; set; }
        public long? EndDay { get; set; }
        public string NumberPassport { get; set; }
        public string SeriesPassport { get; set; }
        public string Birthday { get; set; }
        public string ResidenceAddress { get; set; }
        public string DatePassport { get; set; }
        public string PassportDepartment { get; set; }
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public string PayPalEmail { get; set; }
    }
}