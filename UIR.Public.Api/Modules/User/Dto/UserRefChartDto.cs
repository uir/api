﻿namespace UIR.Public.Api.Modules.User.Dto
{
    public class UserRefChartDto
    {
        public UserChartData[] ByMonth { get; set; }
        public UserChartData[] ByWeek { get; set; }
    }

    public class UserChartData
    {
        public string Key { get; set; }
        public int Count { get; set; }
    }
}