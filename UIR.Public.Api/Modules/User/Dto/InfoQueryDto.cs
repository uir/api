﻿namespace UIR.Public.Api.Modules.User.Dto
{
    public class InfoQueryDto
    {
        public InfoQueryDto(long userId)
        {
            UserId = userId;
        }

        public long UserId { get; set; }
    }
}