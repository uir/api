﻿using System;
using UIR.Domain.Enums;

namespace UIR.Public.Api.Modules.User.Dto
{
    public class ReferalCreateDto
    {
        public string Name { get; set; }
        public ReferalType Type { get; set; }
    }

    public class ReferalCreateCommandDto: ReferalCreateDto
    {
        public long UserId { get; set; }
        public Uri Uri { get; set; }
    }

    public class ReferalDto
    {
        public string Name { get; set; }
        public ReferalType Type { get; set; }
        public long UserId { get; set; }
    }
}