﻿﻿using System.Collections.Generic;
 using UIR.Domain.PaySystem.Enums;

namespace UIR.Public.Api.Modules.User.Dto
{
    public class UserTariffDto
    {
        public string Name { get; set; }
        public PaymentPlanValue Price { get; set; }
        public int Lines { get; set; }
        public bool IsActive { get; set; }
        public long Id { get; set; }
    }


    public class UserInfoTariffDto
    {
        public List<UserTariffDto> Tariffs { get; set; }
        public bool InfosFill { get; set; }
    }
    
}