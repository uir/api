﻿namespace UIR.Public.Api.Modules.User.Dto
{
    public class TariffQueryDto
    {
        public TariffQueryDto(long userId)
        {
            UserId = userId;
        }

        public long UserId { get; set; }
    }
}