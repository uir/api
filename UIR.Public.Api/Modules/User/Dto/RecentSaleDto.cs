using UIR.Bl.Shared.Core.Base.Dto;

namespace UIR.Public.Api.Modules.User.Dto
{
    public class RecentSaleDto: SaleDto
    {
        public long PartnerId { get; set; }
        public string PartnerName { get; set; }
    }
}