﻿namespace UIR.Public.Api.Modules.User.Dto
{
    public class ChartQueryDto
    {
        public ChartQueryDto(long userId)
        {
            UserId = userId;
        }

        public long UserId { get; set; }
    }
}