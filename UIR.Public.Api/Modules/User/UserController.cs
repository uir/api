﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Ddd.Pagination;
using Force.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Bl.Shared.Core.Place.Filters;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Controller;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto;
using UIR.Public.Api.Modules.User.Dto.Request;
using UIR.Public.Api.Modules.User.Filters;
using UIR.Service.Core;

namespace UIR.Public.Api.Modules.User
{
    [Route("api/user")]
    [Authorize]
    public class UserController : CrudController
    {
        private readonly DbContext _dbContext;
        private readonly EmailService _emailService;
        private readonly IQuery<PlacesFilter, PlacesResponseDto> _getBookmarksQuery;
        private readonly IOptions<AppSettings.AppSettings> _options;
        private readonly IQuery<string, ReferalDto> _getRef;
        private readonly IQuery<StatisticQueryDto, UserRefLineDto[]> _statisticQuery;
        private readonly IQuery<TariffQueryDto, UserInfoTariffDto> _tariffQuery;
        private readonly IQuery<InfoQueryDto, UserInfoDto> _infoQuery;
        private readonly IQuery<ChartQueryDto, UserRefChartDto> _chartQuery;
        private readonly IHandler<ReferalCreateCommandDto, string> _createRefCommand;
        private readonly IHandler<UserInfoDto, UserInfoDto> _editUserInfoCommand;

        public UserController(
            DbContext dbContext,
            EmailService emailService,
            IQuery<PlacesFilter, PlacesResponseDto> getBookmarksQuery,
            IOptions<AppSettings.AppSettings> options,
            IQuery<string, ReferalDto> getRef,
            IQuery<StatisticQueryDto, UserRefLineDto[]> statisticQuery,
            IQuery<ChartQueryDto, UserRefChartDto> chartQuery,
            IHandler<ReferalCreateCommandDto, string> createRefCommand,
            IQuery<TariffQueryDto, UserInfoTariffDto> tariffQuery,
            IQuery<InfoQueryDto, UserInfoDto> infoQuery,
            IHandler<UserInfoDto, UserInfoDto> editUserInfoCommand
        ) : base(dbContext)
        {
            _dbContext = dbContext;
            _emailService = emailService;
            _getBookmarksQuery = getBookmarksQuery;
            _options = options;
            _getRef = getRef;
            _statisticQuery = statisticQuery;
            _chartQuery = chartQuery;
            _createRefCommand = createRefCommand;
            _tariffQuery = tariffQuery;
            _infoQuery = infoQuery;
            _editUserInfoCommand = editUserInfoCommand;
        }

        /// <summary>
        /// Создает реферальную ссылку
        /// </summary>
        /// <returns></returns>
        [Route("ref")]
        [HttpPost]
        public IActionResult CreateRef([FromBody] ReferalCreateDto dto) => _createRefCommand
            .Handle(new ReferalCreateCommandDto
            {
                Name = dto.Name,
                Type = dto.Type,
                UserId = User.GetId()
            })
            .PipeTo(Json);

        /// <summary>
        /// Получает реферльную ссылку
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("ref")]
        [HttpGet]
        public IActionResult GetRef(string referal) => _getRef
            .Ask(referal)
            .PipeTo(Ok);

        /// <summary>
        /// Создает закладку
        /// </summary>
        /// <returns></returns>
        [Route("bookmark")]
        [HttpPost]
        public IActionResult AddBookmark([FromBody] BookmarkDto dto)
        {
            var userId = User.GetId();

            var newBookmark = new Bookmarks
            {
                PlaceId = dto.PartnerId,
                UserId = userId
            };

            if (!DbContext.Set<Bookmarks>().Any(x => x.PlaceId == newBookmark.PlaceId && x.UserId == userId))
            {
                DbContext
                    .Set<Bookmarks>()
                    .Add(newBookmark);
                DbContext.SaveChanges();
            }

            return Ok();
        }

        /// <summary>
        /// Удаляет закладку
        /// </summary>
        /// <returns></returns>
        [Route("bookmark")]
        [HttpDelete]
        public IActionResult RemoveBookmark(long placeId)
        {
            var userId = User.GetId();
            var place = DbContext
                .Set<Bookmarks>()
                .FirstOrDefault(x => x.PlaceId == placeId && x.UserId == userId);
            if (place == null)
                throw new OperationException("Организации нет в закладках");
            DbContext
                .Set<Bookmarks>()
                .Remove(place);
            DbContext.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Возвращает все закладки пользователя
        /// </summary>
        /// <returns></returns>
        [Route("bookmark/all")]
        [HttpGet]
        public IActionResult AllBookmarks()
        {
            var userId = User.GetId();
            return (IActionResult) _dbContext
                       .Where<Domain.Entities.UserSetUp.User>(x => x.Id == userId)
                       .Include(x => x.Bookmarks)
                       .FirstOrDefault()
                       ?.Bookmarks
                       .Select(x => x.PlaceId)
                       .ToArray()
                       .PipeTo(Ok)
                   ?? BadRequest();
        }

        /// <summary>
        /// Возвращает все закладки пользователя
        /// </summary>
        /// <returns></returns>
        [Route("~/api/user/places/bookmarks")]
        [HttpGet]
        public IActionResult GetBookmarks([FromQuery] PlacesQueryOptions options)
        {
            var userId = User.GetId();
            var placesIds = DbContext
                .Where<Bookmarks>(x => x.UserId == userId)
                .Select(x => x.PlaceId)
                .ToArray();

            return _getBookmarksQuery
                .Ask(new PlacesFilter(placesIds, userId, options))
                .PipeTo(Ok);
        }

        /// <summary>
        /// Возвращает скидки пользователя
        /// </summary>
        /// <returns></returns>
        [Route("sales"), HttpGet]
        public IActionResult GetUserSales(int page = 1, int count = 10)
        {
            var userId = User.GetId();
            var spec = new UserSalesSpec(userId, page, count);
            return DbContext
                .Where(spec)
                .ProjectTo<RecentSaleDto>()
                .OrderBy(x => x.Name)
                .ToPagedEnumerable(spec)
                .PipeTo(Ok);
        }

        /// <summary>
        /// Взять скидку
        /// </summary>
        /// <returns></returns>
        [Route("sale/{saleId}"), HttpGet]
        public IActionResult TakeSale(long saleId)
        {
            var user = DbContext
                .Where<Domain.Entities.UserSetUp.User>(x => x.Id == User.GetId())
                .Include(x => x.Sales)
                .FirstOrDefault();

            var oldSale = DbContext
                .FirstBySpec(new TakeSaleSpec(saleId, user.Id));

            if (oldSale != null)
                throw new Exception($"Скидка {oldSale.Sale.Name} уже у вас.");

            var sale = DbContext.GetById<Sale>(saleId)
                .ThrowIfNull("sale not found");

            user.Sales.Add(new UserSale
            {
                IsActive = true,
                Sale = sale,
                User = user
            });

            DbContext.SaveChanges();
            return Ok();
        }

        /// <summary>
        /// Статистика приглашений
        /// </summary>
        /// <returns></returns>
        [Route("ref/statistic"), HttpGet]
        public IActionResult GetStatistic() => _statisticQuery
            .Ask(new StatisticQueryDto(User.GetId()))
            .PipeTo(Ok);

        /// <summary>
        /// График по приглашениям
        /// </summary>
        /// <returns></returns>
        [Route("ref/chart"), HttpGet]
        public IActionResult GetChartData() => _chartQuery
            .Ask(new ChartQueryDto(User.GetId()))
            .PipeTo(Ok);

        /// <summary>
        /// Отправить запрос на ответ
        /// </summary>
        /// <returns></returns>
        [Route("callback"), HttpPost, AllowAnonymous]
        public async Task<IActionResult> Callback([FromBody] CallbackDto callbackDto)
        {
            await _emailService.Send(
                "info@uir.one",
                $"Запрос на обратную связь от {callbackDto.Email}",
                $"От {callbackDto.Email}\n" +
                $"Сообщение: {callbackDto.Message}");
            return Ok();
        }

        /// <summary>
        /// Сообщить об ошибке
        /// </summary>
        /// <returns></returns>
        [Route("feedback"), HttpPost, AllowAnonymous]
        public async Task<IActionResult> Feedback([FromBody] FeedbackDto feedbackDto)
        {
            string text = $@"
                URL: {feedbackDto.Url};
                Platform: {feedbackDto.Platform};
                Screen Height: {feedbackDto.ScreenHeight};
                Scree nWidth: {feedbackDto.ScreenWidth};
                User gent: {feedbackDto.UserAgent};
                User comment: {feedbackDto.Text};\n";
            await _emailService.Send("info@uir.one", $"Отчет об ошибке {feedbackDto.Url}", text);
            return Ok();
        }

        /// <summary>
        /// Получить тарифы
        /// </summary>
        /// <returns></returns>
        [Route("tariffs"), HttpGet]
        public IActionResult GetTarrifs() => _tariffQuery
            .Ask(new TariffQueryDto(User.GetId()))
            .PipeTo(Ok);

        /// <summary>
        /// Получить основную информацию о пользователе
        /// </summary>
        /// <returns></returns>
        [Route("lk/info"), HttpGet]
        public IActionResult GetInfo() => _infoQuery
            .Ask(new InfoQueryDto(User.GetId()))
            .PipeTo(Ok);

        /// <summary>
        /// Редактировать основную информацию
        /// </summary>
        /// <returns></returns>
        [Route("lk/info"), HttpPut]
        public IActionResult PutInfo([FromBody] UserInfoDto info) =>
            Ok(_editUserInfoCommand.Handle(info));
    }
}