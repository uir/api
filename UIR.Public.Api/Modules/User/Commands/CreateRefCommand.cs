﻿using System;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Modules.User.Commands
{
    public class CreateRefCommand :IHandler<ReferalCreateCommandDto, string>
    {
        private readonly DbContext _dbContext;

        public CreateRefCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public string Handle(ReferalCreateCommandDto data)
        {
            if (data.Type == ReferalType.General)
            {
                var oldGeneralRef = _dbContext
                    .FirstOrDefault<Referal>(x => x.UserId == data.UserId && x.Type == ReferalType.General);
                if (oldGeneralRef != null)
                    return oldGeneralRef.Id;
            }

            var newref = _dbContext.Add(new Referal
            {
                Id = Guid.NewGuid().ToString("N"),
                Name = data.Name,
                Type = data.Type,
                CreateDateTime = DateTime.Now,
                UserId = data.UserId
            });

            _dbContext.SaveChanges();

            return newref.Entity.Id;
        }
    }
}