﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto.Request;
using UIR.Service.Core.TokenService;

namespace UIR.Public.Api.Modules.User.Commands.ChangePassword
{
    public class ChangePasswordRequestCommand : IAsyncHandler<ChangePasswordRequestDto>
    {
        private readonly DbContext _dbContext;
        private readonly ITokenService _tokenService;

        public ChangePasswordRequestCommand(DbContext dbContext, SmsTokenService tokenService)
        {
            _dbContext = dbContext;
            _tokenService = tokenService;
        }

        public async Task Handle(ChangePasswordRequestDto input)
        {
            var user = _dbContext.FirstOrDefault<Domain.Entities.UserSetUp.User>(x => x.UserName == input.Phone);
            if (user == null)
                throw new OperationException("Пользователь не найден");

            await _tokenService.SendToken(user);
        }
    }
}