﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Public.Api.Modules.User.Dto.Request;
using UIR.Service.Core.TokenService;

namespace UIR.Public.Api.Modules.User.Commands.ChangePassword
{
    public class ChangePasswordConfirmCommand : IAsyncHandler<ChangePasswordConfirmDto>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly DbContext _dbContext;
        private readonly SmsTokenService _smsTokenService;

        public ChangePasswordConfirmCommand(UserManager<Domain.Entities.UserSetUp.User> userManager,
            DbContext dbContext, SmsTokenService smsTokenService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _smsTokenService = smsTokenService;
        }

        public async Task Handle(ChangePasswordConfirmDto input)
        {
            var user = await _userManager.FindByNameAsync(input.Phone);
            await _smsTokenService.ValidateToken(user, input.Code);
            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, input.Password);
            await _dbContext.SaveChangesAsync();
        }
    }
}