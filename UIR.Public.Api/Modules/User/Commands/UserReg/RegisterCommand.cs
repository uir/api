﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Exception;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto;
using UIR.Service.Core.TokenService;

namespace UIR.Public.Api.Modules.User.Commands.UserReg
{
    public class RegisterCommand : IAsyncHandler<AuthDto, long>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly DbContext _dbContext;
        private readonly ITokenService _tokenService;

        public RegisterCommand(UserManager<Domain.Entities.UserSetUp.User> userManager, DbContext dbContext,
            SmsTokenService tokenService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _tokenService = tokenService;
        }

        public async Task<long> Handle(AuthDto data)
        {
            var user = _dbContext
                .FirstOrDefault<Domain.Entities.UserSetUp.User>(x => x.UserName == data.Phone);
            if (user != null)
            {
                if (user.PhoneNumberConfirmed || user.PhoneNumber != data.Phone)
                    throw new OperationException("Пользователь уже зарегистрирован");
                user.Name = data.Name;
                user.Email = data.Email;
                await _dbContext.SaveChangesAsync();
                await _tokenService.SendToken(user);
                return user.Id;
            }
            user = data.Map<PublicUser>();
            if (data.ParentId != 0)
            {
                var parent = _dbContext.GetById<Domain.Entities.UserSetUp.User>(data.ParentId);
                if (parent == null)
                    throw new OperationException("По реферальной ссылке пользователь не найден");
                user.ParentId = parent.Id;
            }

            var result = await _userManager.CreateAsync(user, data.Password);
            await _dbContext.SaveChangesAsync();
            await _tokenService.SendToken(user);
            return user.Id;
        }
    }
}