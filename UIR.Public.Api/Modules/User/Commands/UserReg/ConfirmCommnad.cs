﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Enums;
using UIR.Extensions.Core.Exception;
using UIR.Public.Api.Modules.User.Dto.Request;
using UIR.Service.Core.TokenService;

namespace UIR.Public.Api.Modules.User.Commands.UserReg
{
    public class ConfirmCommnad : IAsyncHandler<ConfirmDto>
    {
        private readonly UserManager<Domain.Entities.UserSetUp.User> _userManager;
        private readonly ITokenService _tokenService;
        private readonly DbContext _dbContext;

        public ConfirmCommnad(UserManager<Domain.Entities.UserSetUp.User> userManager, SmsTokenService tokenService,
            DbContext dbContext)
        {
            _userManager = userManager;
            _tokenService = tokenService;
            _dbContext = dbContext;
        }

        public async Task Handle(ConfirmDto input)
        {
            var user = await _userManager.FindByNameAsync(input.Phone);
            if (user == null)
                throw new OperationException("user not found");
            
            await _tokenService.ValidateToken(user, input.Code);
            user.PhoneNumberConfirmed = true;
            await _userManager.AddToRoleAsync(user, RoleType.User.ToString());
            await _userManager.UpdateAsync(user);
        }
    }
}