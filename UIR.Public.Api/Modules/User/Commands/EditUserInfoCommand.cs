﻿using System;
using System.Linq;
using AutoMapper;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Domain.Entities.UserSetUp;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto;


namespace UIR.Public.Api.Modules.User.Commands
{
    public class EditUserInfoCommand : IHandler<UserInfoDto, UserInfoDto>
    {
        private readonly DbContext _dbContext;

        public EditUserInfoCommand(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public UserInfoDto Handle(UserInfoDto info)
        {
            var user = _dbContext.GetById<PublicUser>(info.Id);
            Mapper.Map(info, user);
            _dbContext.SaveChanges();
            return info;
        }
    }
}