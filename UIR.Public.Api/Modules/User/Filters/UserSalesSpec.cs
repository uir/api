using System.Linq;
using Force.Ddd;
using Force.Ddd.Pagination;
using Microsoft.EntityFrameworkCore;

namespace UIR.Public.Api.Modules.User.Filters
{
    public class UserSalesSpec : IQueryableFilter<Domain.Entities.Sale>, IPaging
    {
        public long UserId { get; }
        public int Page { get; }
        public int Take { get; }

        public UserSalesSpec(long userId, int page = 1, int take = 10)
        {
            UserId = userId;
            Page = page;
            Take = take;
        }

        public IQueryable<Domain.Entities.Sale> Apply(IQueryable<Domain.Entities.Sale> query) => query
            .Where(x => x.Users.Any(y => y.Id == UserId))
            .Include(x => x.Users);
    }
}