﻿using System.Linq;
using Force.Ddd;
using UIR.Bl.Shared.Core.PaySystem.Filters;

namespace UIR.Public.Api.Modules.User.Filters
{
    public class PaymentsLineFilter : LastPaymentFilterBase
    {
        private readonly long[] _userIds;
        protected override IQueryableOrder<Domain.PaySystem.Entities.Payment> Order => new LastPaymentOrder();

        protected override Spec<Domain.PaySystem.Entities.Payment> DateSpec =>
            Domain.PaySystem.Entities.Payment.LastPaid;

        public PaymentsLineFilter(long[] userIds)
        {
            _userIds = userIds;
        }

        protected override IQueryable<Domain.PaySystem.Entities.Payment> Filter(
            IQueryable<Domain.PaySystem.Entities.Payment> queryable) => queryable
            .Where(x => _userIds.Contains(x.UserId));
    }
}