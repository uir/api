﻿using System.Linq;
using Force.Ddd;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;

namespace UIR.Public.Api.Modules.User.Filters
{
    public class TakeSaleSpec: IQueryableFilter<UserSale>
    {
        public long SaleId { get; }
        public long UserId { get; }

        public TakeSaleSpec(long saleId, long userId)
        {
            SaleId = saleId;
            UserId = userId;
        }

        public IQueryable<UserSale> Apply(IQueryable<UserSale> query) => query
            .Where(x => x.UserId == UserId && x.SaleId == SaleId && x.IsActive)
            .Include(x => x.Sale);
    }
}