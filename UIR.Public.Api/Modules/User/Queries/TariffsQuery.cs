﻿﻿using System;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.EntityFrameworkCore;
 using UIR.Bl.Shared.Core.PaySystem.Filters;
 using UIR.Domain.Entities;
 using UIR.Domain.Entities.UserSetUp;
 using UIR.Domain.PaySystem.Entities;
using UIR.Domain.PaySystem.Enums;
 using UIR.Extensions.Core.Extensions;
 using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Modules.User.Queries
{
    public class TariffsQuery : IQuery<TariffQueryDto, UserInfoTariffDto>
    {
        private readonly DbContext _dbContext;

        public TariffsQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public UserInfoTariffDto Ask(TariffQueryDto spec)
        {
            var tarrifs = _dbContext.Set<PaymentPlan>()
                .Where(x => x.PaymentPlanType == PaymentPlanType.Public)
                .ProjectTo<UserTariffDto>()
                .ToList();
            
            tarrifs.ForEach(x =>
                {
                    x.Lines = x.Price == PaymentPlanValue.HeigthValue
                        ? 5
                        : (x.Price == PaymentPlanValue.MiddleValue ? 3 : 0);
                });

            var lastPaidPayment = _dbContext
                .Where(new LastSuccessPaymentFilter(spec.UserId))
                .FirstOrDefault();
           
            if (lastPaidPayment != null)
               tarrifs.First(x => x.Price == lastPaidPayment.PaymentPlan.Value).IsActive = true;

            else
               tarrifs.First(x => x.Price == PaymentPlanValue.Free).IsActive = true;

            var user = _dbContext.GetById<PublicUser>(spec.UserId);
            
            return new UserInfoTariffDto()
            {
                Tariffs = tarrifs,
                InfosFill = user.InfosFill()
            };
        }
    }
}