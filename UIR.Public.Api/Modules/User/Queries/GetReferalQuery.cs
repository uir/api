﻿using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Modules.User.Queries
{
    public class GetReferalQuery: IQuery<string, ReferalDto>
    {
        private readonly DbContext _dbContext;

        public GetReferalQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ReferalDto Ask(string spec)
        {
            var referal = _dbContext.GetById<Referal>(spec);
            return referal.Map<ReferalDto>();
        }
    }
}