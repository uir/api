﻿﻿using System;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.EntityFrameworkCore;
 using UIR.Bl.Shared.Core.PaySystem.Filters;
 using UIR.Domain.Entities;
 using UIR.Domain.Entities.UserSetUp;
 using UIR.Domain.PaySystem.Entities;
using UIR.Domain.PaySystem.Enums;
 using UIR.Extensions.Core.Extensions;
 using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Modules.User.Queries
{
    public class UserInfoQuery : IQuery<InfoQueryDto, UserInfoDto>
    {
        private readonly DbContext _dbContext;

        public UserInfoQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public UserInfoDto Ask(InfoQueryDto spec)
        {
            var infoDto = _dbContext
                .Set<PublicUser>()
                .Include(x => x.Payments)
                .ThenInclude(x => x.PaymentPlan)
                .Where(x => x.Id == spec.UserId)
                .ProjectTo<UserInfoDto>()
                .FirstOrDefault();

            var lastPaidPayment = _dbContext
                .Where(new LastSuccessPaymentFilter(spec.UserId))
                .FirstOrDefault();

            if (lastPaidPayment == null) return infoDto;

            if (infoDto != null)
            {
                infoDto.TariffId = lastPaidPayment.PaymentPlan.Id;
                infoDto.TariffName = lastPaidPayment.PaymentPlan.Name;
                infoDto.EndDay = lastPaidPayment.DateTime.AddMonths(1).DayOfYear - DateTime.Now.DayOfYear;
            }
           
            return infoDto;
        }
    }
}