﻿using System;
using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.Entities;
using UIR.Public.Api.Extensions;
using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Modules.User.Queries
{
    public class ChartQuery : IQuery<ChartQueryDto, UserRefChartDto>
    {
        private readonly DbContext _dbContext;

        public ChartQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public UserRefChartDto Ask(ChartQueryDto dto)
        {
            var startDateTime = DateTime.Now.AddYears(-1);
            var data = _dbContext
                .Set<Referal>()
                .Where(x => x.CreateDateTime >= startDateTime)
                .Where(x => x.UserId == dto.UserId)
                .Select(x => x.CreateDateTime)
                .OrderBy(x => x)
                .ToArray();

            var byMonth = data
                .Select(x => x.Month.ToString())
                .ToChartData();

            var byWeek = data
                .Select(x => x.ToString("dd.M"))
                .ToChartData();

            return new UserRefChartDto
            {
                ByMonth = byMonth,
                ByWeek = byWeek
            };
        }        
    }
}