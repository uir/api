﻿using System.Collections.Generic;
using System.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.PaySystem.Enums;
using UIR.Extensions.Core.Extensions;
using UIR.Public.Api.Modules.User.Dto;
using UIR.Public.Api.Modules.User.Filters;

namespace UIR.Public.Api.Modules.User.Queries
{
    //todo вынести код получения дерева пользователя
    public class StatisticQuery : IQuery<StatisticQueryDto, UserRefLineDto[]>
    {
        private readonly DbContext _dbContext;

        public StatisticQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public UserRefLineDto[] Ask(StatisticQueryDto dto)
        {
            var res = new List<UserRefLineDto>();
            var parentIds = new List<long> {dto.UserId};
            for (var i = 0; i < 5; i++)
            {
                var lineDto = _dbContext
                    .Set<Domain.Entities.UserSetUp.User>()
                    .Where(x => x.ParentId != null && parentIds.Contains(x.ParentId.Value))
                    .ToArray();

                if (lineDto.Length == 0)
                    break;

                var userIds = lineDto.Select(x => x.Id).ToArray();
                var payments = _dbContext
                    .Where(new PaymentsLineFilter(userIds))
                    .Select(x => x.PaymentPlan.Value)
                    .ToList();

                res.Add(new UserRefLineDto
                {
                    All = lineDto.Length,
                    Pay = payments.Count,
                    Profit = payments.Count == 0
                        ? 0
                        : payments.Sum(x =>
                        {
                            switch (x)
                            {
                                case PaymentPlanValue.Free: return 0;
                                case PaymentPlanValue.LowValue: return 50;
                                case PaymentPlanValue.MiddleValue:
                                case PaymentPlanValue.HeigthValue: return 150;
                                default: return 0;
                            }
                        })
                });

                parentIds = lineDto.Select(x => x.Id).ToList();
            }

            return res.ToArray();
        }
    }
}