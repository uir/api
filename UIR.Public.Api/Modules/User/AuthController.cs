﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions.Core.Controller;
using UIR.Public.Api.Modules.User.Dto;
using UIR.Public.Api.Modules.User.Dto.Request;

namespace UIR.Public.Api.Modules.User
{
    [Route("api/auth")]
    public class AuthController : CrudController
    {
        private readonly IAsyncHandler<ConfirmDto> _confirmCommand;
        private readonly IAsyncHandler<ChangePasswordRequestDto> _changePasswordCommand;
        private readonly IAsyncHandler<ChangePasswordConfirmDto> _changePasswordConfirmCommand;
        private readonly IAsyncHandler<AuthDto, long> _authHandler;

        public AuthController(
            DbContext dbContext,
            IAsyncHandler<ConfirmDto> confirmCommand,
            IAsyncHandler<ChangePasswordRequestDto> changePasswordCommand,
            IAsyncHandler<ChangePasswordConfirmDto> changePasswordConfirmCommand,
            IAsyncHandler<AuthDto, long> authHandler) : base(dbContext)
        {
            _confirmCommand = confirmCommand;
            _changePasswordCommand = changePasswordCommand;
            _changePasswordConfirmCommand = changePasswordConfirmCommand;
            _authHandler = authHandler;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequestDto dto)
        {
            await _changePasswordCommand.Handle(dto);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("changepassword/confirm")]
        public async Task<IActionResult> Confirm([FromBody] ChangePasswordConfirmDto dto)
        {
            await _changePasswordConfirmCommand.Handle(dto);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("confirm")]
        public async Task<IActionResult> Confirm([FromBody] ConfirmDto dto)
        {
            await _confirmCommand.Handle(dto);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("reg")]
        public async Task<IActionResult> Registered([FromBody] AuthDto dto)
        {
            var res = await _authHandler.Handle(dto);
            return Ok(res);
        }
    }
}