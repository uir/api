﻿using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.AppSettings.Extensions;
using UIR.Auth.Core;
using UIR.Bl.Shared.Core;
using UIR.DAL.Core;
using UIR.Extensions.Core.Extensions;
using UIR.Extensions.Core.Filters;
using UIR.Service.Core;

namespace UIR.Public.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddUirEf(Configuration);
            services.AddUirBl(Configuration);
            services.AddUirBlShared(Configuration);
            services.AddUirServices(Configuration, Environment);
            services.AddUirBaseServices(Configuration);
            services.AddUirAuth(new AuthSettings
            {
                RequireConfirmedEmail = false,
                RequireConfirmedPhoneNumber = true
            });
            services.AddMemoryCache();
            services.AddResponseCaching();
            services.AddResponseCompression();
            services.AddMvc(options => { options.Filters.Add<OperationExceptionFilter>(); });
            services.AddAutoMapper(
                typeof(Startup).Assembly,
                Assembly.Load("UIR.Bl.Shared.Core"),
                Assembly.Load("UIR.Bl.Payment"),
                Assembly.Load("UIR.Service.Core"));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            ServiceProviderFactory.ServiceProvider = services.BuildServiceProvider();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseResponseCaching();
            app.UseMvc();
            app.UseUirSwagger();
            app.UseResponseCompression();
        }
    }
}