﻿using System.Collections.Generic;
using System.Linq;
using UIR.Public.Api.Modules.User.Dto;

namespace UIR.Public.Api.Extensions
{
    public static class StringExtensions
    {
        public static UserChartData[] ToChartData(this IEnumerable<string> src) => src
            .GroupBy(x => x)
            .Select(x => new UserChartData
            {
                Key = x.Key,
                Count = x.Count()
            })
            .ToArray();
    }
}