﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.Bl.Payment.Payment;
using UIR.Public.Api.Modules.Category;
using UIR.Public.Api.Modules.Place;
using UIR.Public.Api.Modules.User;

namespace UIR.Public.Api
{
    public static class BlModule
    {
        public static void AddUirBl(this IServiceCollection services, IConfiguration configuration)
        {
            new CategoryModule().Register(services);
            new UserModule().Register(services);
            new PlaceModule().Register(services);
            new PaymentModule().Register(services, configuration);
        }
    }
}