using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using UIR.Domain.Interfaces;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Shared.Core
{
    public static class UIrHelper
    {
        public static IEnumerable<T> SetPath<T>(this IEnumerable<T> arr, Uri uri,
            IOptions<AppSettings.AppSettings> options) where T : IHasLogo => arr.Each(x =>
            x.Logo = x.Logo.ToPathImg(uri, options.Value.CloudHost));
    }
}