using AutoMapper;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.Entities;

namespace UIR.Bl.Shared.Core.Base
{
    public class BaseProfile : Profile
    {
        public BaseProfile()
        {
            CreateMap<WorkPeriodDto, Period>();

            CreateMap<Period, WorkPeriodDto>();

            CreateMap<Sale, SaleDto>();

            CreateMap<Tag, TagDto>();
        }
    }
}