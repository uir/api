using Force.Ddd;

namespace UIR.Bl.Shared.Core.Base.Dto
{
    public class ImageDto: HasIdBase<long>
    {
        public string File { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}