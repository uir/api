using UIR.Domain.Enums;

namespace UIR.Bl.Shared.Core.Base.Dto
{
    public class WorkPeriodDto
    {
        public PeriodType PeriodType { get; set; }
        public int Day { get; set; }
        public string Time { get; set; }
    }
}