using System;
using Force.Ddd;
using UIR.Domain.Enums;

namespace UIR.Bl.Shared.Core.Base.Dto
{
    public class SaleDto : HasIdBase<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public bool IsActive { get; set; }
        public bool IsExclusive { get; set; }
        public DiscountType DiscountType { get; set; }
        public DateTimeOffset? DiscountEndDate { get; set; }
    }
}