﻿using Force.Ddd;

namespace UIR.Bl.Shared.Core.Base.Dto
{
    public class TagDto : HasIdBase<long>
    {
        public string Name { get; set; }
    }
}