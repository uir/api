﻿using Force.Ddd;

namespace UIR.Bl.Shared.Core.Base.Dto
{
    public class VideoDto: HasIdBase<long>
    {
        public string Name { get; set; }
        public string Discription { get; set; }
        public string File { get; set; }
    }
}