﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Ddd;
using Force.Extensions;
using Microsoft.EntityFrameworkCore;
using UIR.Extensions;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Shared.Core.Base.Queries
{
    public class BaseQuery<TSrc, TDest> :
        IQuery<IQueryableFilter<TSrc>, bool>,
        IQuery<IQueryableFilter<TSrc>, IEnumerable<TDest>>,
        IQuery<IQueryableFilter<TSrc>, TDest[]>,
        IQuery<long, TDest>
        where TSrc : class, IHasId
    {
        private readonly DbContext _dbContext;
        private readonly IUnitOfWork _unitOfWork;

        public BaseQuery(DbContext dbContext, IUnitOfWork unitOfWork)
        {
            _dbContext = dbContext;
            _unitOfWork = unitOfWork;
        }

        public bool Ask(IQueryableFilter<TSrc> spec) => _dbContext.Where(spec).Any();

        IEnumerable<TDest> IQuery<IQueryableFilter<TSrc>, IEnumerable<TDest>>.Ask(IQueryableFilter<TSrc> spec) =>
            _dbContext.Set<TSrc>().Where(spec).ProjectTo<TDest>().MaybeOrderBy(spec).ToList();

        TDest[] IQuery<IQueryableFilter<TSrc>, TDest[]>.Ask(IQueryableFilter<TSrc> spec) =>
            _dbContext.Set<TSrc>().Where(spec).ProjectTo<TDest>().MaybeOrderBy(spec).ToArray();

        public TDest Ask(long spec) => _unitOfWork.Find<TSrc>(spec).Map<TDest>();
    }
}