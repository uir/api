﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Domain.Entities;
using UIR.Domain.PaySystem.Entities;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Bl.Shared.Core.Place
{
    public class PlaceProfile : Profile
    {
        public PlaceProfile()
        {
            CreateMap<List<Payment>, PaymentPlanValue?>()
                .ProjectUsing(
                    p => p
                        .AsQueryable()
                        .Where(Payment.LastPaid.Expression)
                        .OrderByDescending(x => x.PaidDateTime)
                        .FirstOrDefault()
                        .PaymentPlan
                        .Value);

            CreateMap<Domain.Entities.Place, PlaceDto>()
                .ForMember(dest => dest.Logo, o => o.MapFrom(PlaceFormatter.Logo))
                .ForMember(dest => dest.Name, o => o.MapFrom(PlaceFormatter.Name))
                .ForMember(dest => dest.Tags, o => o.Ignore())
                .ForMember(d => d.PlanValue, o => o.MapFrom(p => p.Organization.Owner.Payments))
                .ForMember(dest => dest.SalesCount, o => o.MapFrom(PlaceFormatter.SalesCount))
                .ForMember(dest => dest.ImagesCount, o => o.MapFrom(PlaceFormatter.ImagesCount))
                .ForMember(dest => dest.VideosCount, o => o.MapFrom(src => src.Videos.Count))
                .ForMember(dest => dest.OverviewsCount, o => o.MapFrom(src => src.Overviews.Count))
                .ForMember(dest => dest.Lat, o => o.MapFrom(src => src.Location.Latitude))
                .ForMember(dest => dest.Lng, o => o.MapFrom(src => src.Location.Longitude));

            CreateMap<Domain.Entities.Place, MarkerDto>()
                .ForMember(dest => dest.Latitude, o => o.MapFrom(src => src.Location.Latitude))
                .ForMember(dest => dest.Longitude, o => o.MapFrom(src => src.Location.Longitude));
        }
    }
}