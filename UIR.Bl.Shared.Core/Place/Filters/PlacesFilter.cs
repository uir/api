﻿using System.Linq;
using Force.Ddd;
using Force.Ddd.Pagination;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Domain.Entities;

namespace UIR.Bl.Shared.Core.Place.Filters
{
    public class PlacesFilter : IQueryableFilter<Domain.Entities.Place>, IQueryableOrder<PlaceDto>, IPaging
    {
        private readonly string _orderBy;
        
        public long UserId { get; }
        public long[] PlacesIds { get; }
        public int Page { get; }
        public int Take { get; }
        public bool WithMarkers { get; }

        public PlacesFilter(long[] placesIds, long userId, PlacesQueryOptions options)
        {
            UserId = userId;
            Page = options.Page ?? 1;
            Take = options.Take ?? 20;
            _orderBy = options.OrderBy;
            PlacesIds = placesIds;
            WithMarkers = options.WithMarkers ?? options.Page == 1;
        }

        public IQueryable<Domain.Entities.Place> Apply(IQueryable<Domain.Entities.Place> query) => query
            .Where(x => PlacesIds.Contains(x.Id))
            .Where(PlaceSpec.Active);

        public IOrderedQueryable<PlaceDto> OrderBy(IQueryable<PlaceDto> queryable)
            => !string.IsNullOrEmpty(_orderBy)
                ? queryable.AutoSort(_orderBy)
                : queryable
                    .OrderByDescending(x => x.PlanValue)
                    .ThenByDescending(x => x.Name);
    }
}