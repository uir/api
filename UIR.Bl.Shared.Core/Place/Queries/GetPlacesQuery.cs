﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Force.Ddd.Pagination;
using Force.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Bl.Shared.Core.Place.Filters;
using UIR.Domain.Entities;
using UIR.Extensions.Core.Extensions;

namespace UIR.Bl.Shared.Core.Place.Queries
{
    public class GetPlacesQuery : IQuery<PlacesFilter, PlacesResponseDto>
    {
        private readonly DbContext _dbContext;
        private readonly IOptions<AppSettings.AppSettings> _options;

        public GetPlacesQuery(DbContext dbContext, IOptions<AppSettings.AppSettings> options)
        {
            _dbContext = dbContext;
            _options = options;
        }

        public PlacesResponseDto Ask(PlacesFilter filter)
        {
            var places = _dbContext
                .Where(filter)
                .ProjectTo<PlaceDto>()
                .OrderBy(filter)
                .Paginate(filter)
                .ToArray();

            places.Each(x => x.Logo = x.Logo.ToPathImg(_options.Value.CloudHost));

            if (filter.UserId != 0)
            {
                var bookmarks = _dbContext
                    .Where<Bookmarks>(x => x.UserId == filter.UserId)
                    .Select(x => x.PlaceId)
                    .ToArray();
                places.Each(x => { x.IsBookmark = bookmarks.Contains(x.Id); });
            }

            var markers = new MarkerDto[0] { };
            if (filter.WithMarkers)
                markers = _dbContext
                    .Where(filter)
                    .ProjectTo<MarkerDto>()
                    .ToArray();

            return new PlacesResponseDto(markers.Length, places, markers);
        }
    }
}