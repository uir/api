namespace UIR.Bl.Shared.Core.Place.Dto
{
    public class MarkerDto
    {
        public long Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}