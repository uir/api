namespace UIR.Bl.Shared.Core.Place.Dto
{
    public class PlacesQueryOptions
    {
        public string OrderBy { get; set; }
        public int? Page { get; set;}
        public int? Take { get; set;}
        public bool? WithMarkers { get; set;}
    }
}