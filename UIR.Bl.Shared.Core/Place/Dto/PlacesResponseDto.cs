namespace UIR.Bl.Shared.Core.Place.Dto
{
    public class PlacesResponseDto
    {
        public PlacesResponseDto(int placesCount, PlaceDto[] places, MarkerDto[] markers)
        {
            PlacesCount = placesCount;
            Places = places;
            Markers = markers;
        }

        public int PlacesCount { get; }
        public PlaceDto[] Places { get; }
        public MarkerDto[] Markers { get; }
    }
}