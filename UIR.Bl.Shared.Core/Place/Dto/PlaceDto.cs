﻿using System.Collections.Generic;
using Force.Ddd;
using UIR.Bl.Shared.Core.Base.Dto;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Bl.Shared.Core.Place.Dto
{
    public class PlaceDto : HasIdBase<long>
    {
        public string Name { get; set; }
        
        public string Address { get; set; }

        public double Lat { get; set; }

        public double Lng { get; set; }
        
        public int SalesCount { get; set; }
        
        public int ImagesCount { get; set; }
        
        public int VideosCount { get; set; }
        
        public int OverviewsCount { get; set; }
        
        public bool IsBookmark { get; set; }
        
        public string Logo { get; set; }
       
        public long OrganizationId { get; set; }
        
        public string Website { get; set; }
        
        public string Phone { get; set; }
        
        public PaymentPlanValue? PlanValue { get; set; }

        public IEnumerable<TagDto> Tags { get; set; }
    }
}