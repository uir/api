﻿using Force.Cqrs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UIR.Bl.Shared.Core.Place.Dto;
using UIR.Bl.Shared.Core.Place.Filters;
using UIR.Bl.Shared.Core.Place.Queries;

namespace UIR.Bl.Shared.Core
{
    public static class BlSharedModule
    {
        public static void AddUirBlShared(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IQuery<PlacesFilter, PlacesResponseDto>, GetPlacesQuery>();
        }
    }
}