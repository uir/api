﻿using System.Linq;
using Force.Ddd;
using Microsoft.EntityFrameworkCore;
using UIR.Domain.PaySystem.Entities;
using UIR.Domain.PaySystem.Enums;

namespace UIR.Bl.Shared.Core.PaySystem.Filters
{
    public class LastPaymentOrder : IQueryableOrder<Payment>
    {
        public IOrderedQueryable<Payment> OrderBy(IQueryable<Payment> queryable) => queryable
            .OrderByDescending(x => x.DateTime);
    }

    public class LastSuccesPaymentOrder : IQueryableOrder<Payment>
    {
        public IOrderedQueryable<Payment> OrderBy(IQueryable<Payment> queryable) => queryable
            .OrderByDescending(x => x.PaymentPlan.Value)
            .ThenByDescending(x => x.PaidDateTime);
    }

    public abstract class LastPaymentFilterBase : IQueryableFilter<Payment>
    {
        protected abstract IQueryableOrder<Payment> Order { get; }
        protected abstract Spec<Payment> DateSpec { get; }

        public IQueryable<Payment> Apply(IQueryable<Payment> query)
        {
            query = query.Where(DateSpec.Expression).Include(x => x.PaymentPlan);
            query = Order.OrderBy(query);
            query = Filter(query);
            return query;
        }

        protected abstract IQueryable<Payment> Filter(IQueryable<Payment> queryable);
    }

    public class LastPaymentFilter : LastPaymentFilterBase
    {
        private readonly long _userId;
        private readonly PaymentStatus _paymentStatus;
        protected override IQueryableOrder<Payment> Order => new LastPaymentOrder();
        protected override Spec<Payment> DateSpec => Payment.LastCreated;

        public LastPaymentFilter(long userId, PaymentStatus paymentStatus)
        {
            _userId = userId;
            _paymentStatus = paymentStatus;
        }

        protected override IQueryable<Payment> Filter(IQueryable<Payment> queryable) => queryable
            .Where(x => x.UserId == _userId && x.PaymentStatus == _paymentStatus);
    }

    public class LastSuccessPaymentFilter : LastPaymentFilterBase
    {
        private readonly long _userId;
        protected override IQueryableOrder<Payment> Order => new LastSuccesPaymentOrder();
        protected override Spec<Payment> DateSpec => Payment.LastPaid;

        public LastSuccessPaymentFilter(long userId)
        {
            _userId = userId;
        }

        protected override IQueryable<Payment> Filter(IQueryable<Payment> queryable) => queryable
            .Where(x => x.UserId == _userId && x.PaymentStatus == PaymentStatus.Paid);
    }
}