﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace UIR.AppSettings.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddUirAppSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<AppSettings>(configuration.GetSection("AppSettings"));
            services.Configure<ClicklogSettings>(configuration.GetSection("ClicklogSettings"));
        }

        public static void AddUirSwagger(this IServiceCollection services, string apiName, int version = 1)
        {
            services.AddSwaggerGen(options => SwaggerCongig(options, apiName, version));
        }

        public static IServiceCollection AddUirBaseServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddUirAppSettings(configuration);

            var apiSettings = new ApiSettings();
            configuration.GetSection("AppSettings:ApiSettings").Bind(apiSettings);
            var apiName = apiSettings.Name;
            var version = apiSettings.Version;
            services.AddUirSwagger(apiName, version);

            var origins = configuration.GetSection("AppSettings:AllowedHosts").Get<string>().Split(';');
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("CorsPolicy"));
            });
            services.AddCors(options => options
                .AddPolicy("CorsPolicy",
                    builder => builder
                        .AllowAnyOrigin()
                        .WithOrigins(origins)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()));
            return services;
        }

        public static void UseUirSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });
        }

        private static void SwaggerCongig(SwaggerGenOptions c, string apiName, int version)
        {
            c.SwaggerDoc($"v{version}", new Info {Title = apiName, Version = $"v{version}"});
            c.AddSecurityDefinition("Bearer", new ApiKeyScheme
            {
                Description =
                    "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                Name = "Authorization",
                In = "header"
            });
        }
    }
}