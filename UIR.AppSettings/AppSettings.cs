﻿namespace UIR.AppSettings
{
    public class AppSettings
    {
        public string AllowedHosts { get; set; }
        public ApiSettings ApiSettings { get; set; }
        public string CloudHost { get; set; }
        public FileStorageSettings FileStorageSettings { get; set; }
        public EmailSettings EmailSettings { get; set; }
    }

    public class ApiSettings
    {
        public string Name { get; set; }
        public int Version { get; set; }
    }

    public class FileStorageSettings
    {
        public string ImageFolder { get; set; }
        public string VideoFolder { get; set; }
    }

    public class EmailSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class ClicklogSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
    
    public class MaidDbSettings
    {
        public string Host { get; set; }
        public int? Port { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}